package models;

import java.time.LocalDateTime;

public class AccountSubscription {
    private int id;
    private int accountId;
    private int subscriptionId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private boolean status;

    public AccountSubscription() {
    }

    public AccountSubscription(int id, int accountId, int subscriptionId, LocalDateTime startDate, LocalDateTime endDate, boolean status) {
        this.id = id;
        this.accountId = accountId;
        this.subscriptionId = subscriptionId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AccountSubscription{" + "id=" + id + ", accountId=" + accountId + ", subscriptionId=" + subscriptionId + ", startDate=" + startDate + ", endDate=" + endDate + ", status=" + status + '}';
    }
    
    
}
