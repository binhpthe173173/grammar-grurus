
package models;

/**
 *
 * @author Nam
 */
public class Error {
    private int id;
    private String content;
    private String suggest;
    private int post_id;

    public Error() {
    }

    public Error(int id, String content, String suggest, int post_id) {
        this.id = id;
        this.content = content;
        this.suggest = suggest;
        this.post_id = post_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSuggest() {
        return suggest;
    }

    public void setSuggest(String suggest) {
        this.suggest = suggest;
    }

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }
    
    
}
