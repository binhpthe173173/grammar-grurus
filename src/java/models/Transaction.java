
package models;

import java.util.Date;

public class Transaction {
    private int id;
    private int wallet_id;
    private int amount;
    private String transaction_type;
    private String description;
    private Date transaction_date;

    public Transaction() {
    }

    public Transaction(int id, int wallet_id, int amount, String transaction_type, String description, Date transaction_date) {
        this.id = id;
        this.wallet_id = wallet_id;
        this.amount = amount;
        this.transaction_type = transaction_type;
        this.description = description;
        this.transaction_date = transaction_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWallet_id() {
        return wallet_id;
    }

    public void setWallet_id(int wallet_id) {
        this.wallet_id = wallet_id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(Date transaction_date) {
        this.transaction_date = transaction_date;
    }
    
    
}
