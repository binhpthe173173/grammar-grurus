
package models;

import java.util.Date;

public class Subscription {
    private int id;
    private String name;
    private int price;
    private int duration;
    private boolean status;
    private String description;
    private Date time_created;

    public Subscription() {
    }

    public Subscription(int id, String name, int price, int duration, boolean status, String description, Date time_created) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.duration = duration;
        this.status = status;
        this.description = description;
        this.time_created = time_created;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTime_created() {
        return time_created;
    }

    public void setTime_created(Date time_created) {
        this.time_created = time_created;
    }
    
    
}
