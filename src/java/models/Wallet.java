
package models;

public class Wallet {
    private int id;
    private int account_id;
    private int balance;

    public Wallet() {
    }

    public Wallet(int id, int account_id, int balance) {
        this.id = id;
        this.account_id = account_id;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccount_id() {
        return account_id;
    }

    public void setAccount_id(int account_id) {
        this.account_id = account_id;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
    
    
}
