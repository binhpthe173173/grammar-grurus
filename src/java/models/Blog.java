/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.util.Date;

/**
 *
 * @author admin
 */


/*

CREATE TABLE Blog (
    idacount INT,
    content TEXT,
    title VARCHAR(255),
    type VARCHAR(50),
    state VARCHAR(50),
    time_up DATETIME,
    idBlog VARCHAR(50) PRIMARY KEY,
    FOREIGN KEY (idacount) REFERENCES Accounts(id_account)
);

*/
public class Blog {
 private int idacount ;
 private String content ;
 private String title ;
 private String type ;
 private String state ;
 private Date time_up;
 private String idBlog ;

    public int getIdacount() {
        return idacount;
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public String getState() {
        return state;
    }

    public Date getTime_up() {
        return time_up;
    }

    public String getIdBlog() {
        return idBlog;
    }

    public Blog(int idacount, String content, String title, String type, String state, Date time_up, String idBlog) {
        this.idacount = idacount;
        this.content = content;
        this.title = title;
        this.type = type;
        this.state = state;
        this.time_up = time_up;
        this.idBlog = idBlog;
    }

    public void setIdacount(int idacount) {
        this.idacount = idacount;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setTime_up(Date time_up) {
        this.time_up = time_up;
    }

    public void setIdBlog(String idBlog) {
        this.idBlog = idBlog;
    }

    public Blog() {
    }

    
}
