
package models;

/**
 *
 * @author Nam
 */
public class Feedback {
    private int id;
    private String content;
    private int star_rate;
    private int account_id;

    public Feedback() {
    }

    public Feedback(int id, String content, int star_rate, int account_id) {
        this.id = id;
        this.content = content;
        this.star_rate = star_rate;
        this.account_id = account_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getStar_rate() {
        return star_rate;
    }

    public void setStar_rate(int star_rate) {
        this.star_rate = star_rate;
    }

    public int getAccount_id() {
        return account_id;
    }

    public void setAccount_id(int account_id) {
        this.account_id = account_id;
    }

    @Override
    public String toString() {
        return "Feedback{" + "id=" + id + ", content=" + content + ", star_rate=" + star_rate + ", account_id=" + account_id + '}';
    }
    
    
}
