
package models;

import java.util.Date;

/**
 *
 * @author Nam
 */
public class Post {
    private int id;
    private String content;
    private String fixed_content;
    private Date time_created;
    private int account_id;
    private String title;

    public Post() {
    }

    public Post(int id, String content, String fixed_content, Date time_created, int account_id) {
        this.id = id;
        this.content = content;
        this.fixed_content = fixed_content;
        this.time_created = time_created;
        this.account_id = account_id;
    }

    public Post(int id, String content, String fixed_content, Date time_created, int account_id, String title) {
        this.id = id;
        this.content = content;
        this.fixed_content = fixed_content;
        this.time_created = time_created;
        this.account_id = account_id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFixed_content() {
        return fixed_content;
    }

    public void setFixed_content(String fixed_content) {
        this.fixed_content = fixed_content;
    }

    public Date getTime_created() {
        return time_created;
    }

    public void setTime_created(Date time_created) {
        this.time_created = time_created;
    }

    public int getAccount_id() {
        return account_id;
    }

    public void setAccount_id(int account_id) {
        this.account_id = account_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    
}
