/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

public class Qandatopic {
    private String idContent;
    private String question;
    private String content;
    private String idTopic;

    // Constructor
    public Qandatopic(String idContent, String question, String content, String idTopic) {
        this.idContent = idContent;
        this.question = question;
        this.content = content;
        this.idTopic = idTopic;
    }

    // Default constructor
    public Qandatopic() {
    }

    // Getters and Setters
    public String getIdContent() {
        return idContent;
    }

    public void setIdContent(String idContent) {
        this.idContent = idContent;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIdTopic() {
        return idTopic;
    }

    public void setIdTopic(String idTopic) {
        this.idTopic = idTopic;
    }
}
