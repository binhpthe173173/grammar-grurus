
package models;
/**
 *
 * @author Nam
 */
import java.util.Date;
public class Account {
    private int id;
    private String username;
    private String password;
    private String full_name;
    private String email;
    private String image;
    private Date time_created;
    private boolean active_status;
    private int role_id;

    public Account() {
    }

    public Account(int id, String username, String password, String full_name, String email, String image, Date time_created, boolean active_status, int role_id) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.full_name = full_name;
        this.email = email;
        this.image = image;
        this.time_created = time_created;
        this.active_status = active_status;
        this.role_id = role_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getTime_created() {
        return time_created;
    }

    public void setTime_created(Date time_created) {
        this.time_created = time_created;
    }

    public boolean isActive_status() {
        return active_status;
    }

    public void setActive_status(boolean active_status) {
        this.active_status = active_status;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    @Override
    public String toString() {
        return "Account{" + "id=" + id + ", username=" + username + ", password=" + password + ", full_name=" + full_name + ", email=" + email + ", image=" + image + ", time_created=" + time_created + ", active_status=" + active_status + ", role_id=" + role_id + '}';
    }
    
    
}
