/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author admin
 */
public class Topictype {
    private String idtopic ;
    private String type ;
    private String topic ;

    public Topictype(String idtopic, String type, String topic) {
        this.idtopic = idtopic;
        this.type = type;
        this.topic = topic;
    }

    public Topictype() {
    }

    public void setIdtopic(String idtopic) {
        this.idtopic = idtopic;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getIdtopic() {
        return idtopic;
    }

    public String getType() {
        return type;
    }

    public String getTopic() {
        return topic;
    }
    
    
    
}
