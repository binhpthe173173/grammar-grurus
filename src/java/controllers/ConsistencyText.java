/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;

import ApiAnswer.Apitaking;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.Account;

/**
 *
 * @author admin
 */
public class ConsistencyText extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ConsistencyText</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ConsistencyText at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        doPost(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
     protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String a = (String) request.getParameter("inputText");
        if(a==null){
            response.sendRedirect("checkFunction.jsp");
        }else{
            HttpSession ses = request.getSession();
            Account ac = (Account)ses.getAttribute("us");
            request.setAttribute("ac", ac);
            ses.setAttribute("inputText", a);
        Apitaking a1 =new Apitaking();
        String a3 = a1.textComplexWord(a);
        request.setAttribute("inputText", a);
        request.setAttribute("text1", a3);
        request.setAttribute("error", a1.ConsistencyWordTextError(a,10));
//        for(int i=1;i<=a1.ConsistencyWordTextError(a, 10);i++){
//            request.setAttribute("error"+i+"", );
//        }
        request.setAttribute("error1", a1.ConsistencyWordTextError(a, 0));
        request.setAttribute("error2", a1.ConsistencyWordTextError(a, 1));
        request.setAttribute("error3", a1.ConsistencyWordTextError(a, 2));
        request.setAttribute("error4", a1.ConsistencyWordTextError(a, 3));
        request.setAttribute("error5", a1.ConsistencyWordTextError(a, 4));
        request.setAttribute("error6", a1.ConsistencyWordTextError(a, 5));
        request.setAttribute("error7", a1.ConsistencyWordTextError(a, 6));
        request.setAttribute("error8", a1.ConsistencyWordTextError(a, 7));
        request.setAttribute("error9", a1.ConsistencyWordTextError(a, 8));
        request.setAttribute("error10", a1.ConsistencyWordTextError(a, 9));
        request.getRequestDispatcher("showConsistencyWord.jsp").forward(request, response);}
    }
    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
