/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;

import ApiAnswer.Apitaking;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import models.Account;

/**
 *
 * @author admin
 */
public class SubFixError extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SubFixError</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SubFixError at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String wordError = (String)request.getParameter("wordError");
        HttpSession ses = request.getSession();
        Account ac = (Account)ses.getAttribute("us");
        request.setAttribute("ac", ac);
        String text = (String)ses.getAttribute("inputText");
        if(text==null){
            response.sendRedirect("checkFunction.jsp");
        }else{
        String wordSuggest = (String) request.getParameter("wordSuggest");
        String newText = text.replace(wordError," "+ wordSuggest);
        ses.setAttribute("inputText", newText);
         Apitaking a1 =new Apitaking();
        request.setAttribute("inputText", newText);
        request.setAttribute("text1", a1.textSpelling(newText));
        request.setAttribute("suggestion", a1.getSpellingSuggestions(newText));
        request.setAttribute("error", a1.getSpellingErrors(newText));
        request.setAttribute("score", a1.getErrorScore(newText));
        request.getRequestDispatcher("showErrorSpellFix.jsp").forward(request, response);
        }
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String wordError = (String)request.getParameter("wordError");
        HttpSession ses = request.getSession();
        String text = (String)ses.getAttribute("inputText");
        if(text==null){
            response.sendRedirect("checkFunction.jsp");
        }else{
        String wordSuggest = (String) request.getParameter("wordSuggest");
        String newText = text.replace(wordError, wordSuggest);
        ses.setAttribute("inputText", newText);
         Apitaking a1 =new Apitaking();
        request.setAttribute("inputText", newText);
        request.setAttribute("text1", a1.textGrammar(newText));
        request.setAttribute("suggestion", a1.getGrammarSuggestions(newText));
        request.setAttribute("error", a1.getGrammarErrors(newText));
        request.getRequestDispatcher("showErrorGrammarFix.jsp").forward(request, response);}}
    

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
