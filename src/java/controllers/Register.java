/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.*;

/**
 *
 * @author Noisy
 */
public class Register extends HttpServlet {

    DAO dao;
    String err = "";
    boolean login_mail = false;
    String picture;

    @Override
    public void init() {
        dao = new DAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String name = request.getParameter("name");
        picture = request.getParameter("picture");
        String err = request.getParameter("err");
        if(err != null){
            request.setAttribute("err", err);
        }
        if (email != null) {
            login_mail = true;
            request.setAttribute("email", email);
            request.setAttribute("name", name);
            request.setAttribute("picture", picture);
        }
        request.getRequestDispatcher("/register.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String full_name = request.getParameter("full_name");
        String password = request.getParameter("password");
        String password_confirm = request.getParameter("password_confirm");
        String email = request.getParameter("email");
        if (!checkRegister(username, full_name, password, password_confirm, email)) {
            request.setAttribute("err", err);
            if (login_mail) {
                response.sendRedirect(request.getContextPath() + "/register?name=" + full_name + "&email=" + email + "&picture=" + picture + "&err=" + err);
            } else {
                request.getRequestDispatcher("register.jsp").forward(request, response);
            }
            return;
        }
        request.setAttribute("err", "Register success!! Please login at <a href='login'>here</a>");
        dao.addAccount(username, password, full_name, email, picture, true, 1);
        if (login_mail) {
            Account ac = dao.getAccountByMail(email);
            HttpSession ses = request.getSession();
            ses.setAttribute("us", ac);
            response.sendRedirect(request.getContextPath() + "/grammarFunction");
            return;
        }
        request.getRequestDispatcher("/register.jsp").forward(request, response);
    }

    public boolean checkRegister(String username, String full_name, String password, String password_confirm, String email) {
        if (!dao.checkUsername(username)) {
            err = "Invalid Username";
            return false;
        }
        if (!dao.checkPassword(password)) {
            err = "Password must contains uppercase, lowercase and at least a number";
            return false;
        }
        if (!password.equals(password_confirm)) {
            err = "Confirm password does not match";
            return false;
        }
        if (!dao.checkEmail(email)) {
            err = "Please input a valid email";
            return false;
        }
        if (dao.getAccountByUsername(username) != null) {
            err = "Account exist";
            return false;
        }
        if (dao.getAccountByMail(email) != null) {
            err = "Email already used";
            return false;
        }
        return true;
    }

}
