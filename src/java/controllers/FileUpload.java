package controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import models.Account;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

@WebServlet(name = "FileUpload", urlPatterns = {"/fileUpload"})
@MultipartConfig
public class FileUpload extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Part filePart = request.getPart("file");
        HttpSession ses = request.getSession();
        Account ac = (Account)ses.getAttribute("us");
        request.setAttribute("ac", ac);
        if (filePart != null && filePart.getSize() > 0) {
            try (InputStream fileContent = filePart.getInputStream(); XWPFDocument document = new XWPFDocument(fileContent)) {
                StringBuilder content = new StringBuilder();
                List<XWPFParagraph> paragraphs = document.getParagraphs();
                for (XWPFParagraph paragraph : paragraphs) {
                    content.append(paragraph.getText()).append("\n");
                }

                request.setAttribute("inputText", content.toString());
                request.getRequestDispatcher("checkFunction.jsp").forward(request, response);
            } catch (Exception e) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error processing the DOCX file");
            }
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid file uploaded");
        }
    }
}
