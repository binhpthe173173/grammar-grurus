/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;

import dal.BlogDAO;
import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.Random;
import models.Blog;
import dal.*;

/**
 *
 * @author admin
 */
public class CheckBlog extends HttpServlet {
   
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    } 

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
     BlogDAO bdao = new BlogDAO();
             int idacount = Integer.parseInt(request.getParameter("idacount"));
            String title = request.getParameter("title");
            String content = request.getParameter("content");
            String type = request.getParameter("type");
            
            String idBlog = createCodeRandom();
             while (bdao.getBlogById(idBlog) != null) {
            idBlog = createCodeRandom();

        }
            Date time_up = new Date(System.currentTimeMillis()); // Ngày giờ hiện tại

            
            Blog newBlog = new Blog(idacount, content, title, type, "choduyet", time_up, idBlog);

            
            BlogDAO blogDAO = new BlogDAO();
            boolean inserted = blogDAO.insertBlog(newBlog);
response.sendRedirect("includeBlog.jsp");
            
    }
     public  String createCodeRandom() {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            int digit = random.nextInt(10); // Số ngẫu nhiên từ 0 đến 9
            sb.append(digit);
        }
        return sb.toString();
    }
    

    

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    

}
