/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author admin
 */
public class contactus extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet contactus</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet contactus at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       String name = request.getParameter("name").trim();
         String query = request.getParameter("query").trim();
         String email = request.getParameter("email").trim();
         String message =request.getParameter("message").trim();
         autoSendEmail auto = new autoSendEmail();
         
         
         String noidung = form(name, query, name , message, email);
         auto.sendEmail("hoannhhe173201@fpt.edu.vn",noidung,name);
         String ms="The system has received your email and it will take some time for the system to respond !"; 
         request.setAttribute("ms", ms);
         request.getRequestDispatcher("contactus.jsp").forward(request, response);
    }
    
    
    
    

 
    
    
    public String  form( String name, String query, String idAcount , String message,String email ) {
    
    
    String noidung= "<!DOCTYPE html>\n" +
"<html>\n" +
"<head>\n" +
"    <meta charset=\"UTF-8\">\n" +
"    <title>Email Template</title>\n" +
"    <style>\n" +
"        .email-container {\n" +
"            font-family: Arial, sans-serif;\n" +
"            background-color: #f4f4f4;\n" +
"            padding: 20px;\n" +
"            border-radius: 10px;\n" +
"            width: 600px;\n" +
"            margin: auto;\n" +
"        }\n" +
"        .email-header {\n" +
"            background-color: #008080;\n" +
"            color: white;\n" +
"            padding: 10px;\n" +
"            border-radius: 10px 10px 0 0;\n" +
"            text-align: center;\n" +
"        }\n" +
"        .email-body {\n" +
"            background-color: white;\n" +
"            padding: 20px;\n" +
"            border-radius: 0 0 10px 10px;\n" +
"        }\n" +
"        .email-footer {\n" +
"            text-align: center;\n" +
"            margin-top: 20px;\n" +
"            color: #808080;\n" +
"        }\n" +
"    </style>\n" +
"</head>\n" +
"<body>\n" +
"    <div class=\"email-container\">\n" +
"        <div class=\"email-header\">\n" +
"            <h2>New Query from : " +name+"</h2>\n" +
"        </div>\n" +
"        <div class=\"email-body\">\n" +
"            <p><strong>Name:</strong> "+name+"</p>\n" +
           " <p><strong>Email:</strong> "+email+"</p>\n" +
"            <p><strong>Kind of Query:</strong> "+query+"</p>\n" +
"            <p><strong>Message:</strong></p>\n" +
"            <p>"+message+"</p>\n" +
"        </div>\n" +
"        <div class=\"email-footer\">\n" +
"            <p>Check and respont User please.</p>\n" +
"        </div>\n" +
"    </div>\n" +
"</body>\n" +
"</html>" ;
    
    
    return  noidung;
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
