/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import dal.DAO;
import dal.FeedDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author admin
 */
public class DeleteFeedbackUser extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DeleteFeedbackUser</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DeleteFeedbackUser at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String id = request.getParameter("id");
        String feedback = request.getParameter("feedback");
        String srating = request.getParameter("rating");
        int  rating = Integer.parseInt(srating);
        if(!action.isEmpty()&& action.equals("delete")){
        String ms = "U";
        request.setAttribute("id", id);
        request.setAttribute("ms", ms);

        request.getRequestDispatcher("deleteFeedback.jsp").forward(request, response);
        
        return;}
         if(!action.isEmpty()&& action.equals("edit")){
         int check = feedback.length();
        if(check>=250||check==0) {
        
  String ms = "Please leave your review no more than 250 characters and don't leave it blank";
HttpSession session = request.getSession();
session.setAttribute("mserro", ms);
response.sendRedirect("listFeedbacks.jsp");
return;

        }
             
         request.setAttribute("id", id);
         request.setAttribute("feedback", feedback);
         request.setAttribute("rating", rating);

                 request.getRequestDispatcher("editFeedback.jsp").forward(request, response);

         
         
         }
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String sid = request.getParameter("id");
        int id = Integer.parseInt(sid);
        FeedDAO fdao = new FeedDAO();

        fdao.deleteFeedbackById(id);
        response.sendRedirect("listFeedbacks.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
