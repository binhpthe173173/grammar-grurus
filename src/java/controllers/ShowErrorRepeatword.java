/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;

import ApiAnswer.Apitaking;
import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import models.Account;

/**
 *
 * @author admin
 */
public class ShowErrorRepeatword extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ShowErrorRepeatword</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ShowErrorRepeatword at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
       String error= (String) req.getParameter("error");HttpSession ses = req.getSession();
       String text= (String)ses.getAttribute("inputText");
       Account ac = (Account)ses.getAttribute("us");
       req.setAttribute("ac", ac);
       if(text==null){
            resp.sendRedirect("checkFunction.jsp");
        }else{
            ses.setAttribute("inputText", text);
       String  Title="";
       String[] parts = error.split("'");
                            if (parts.length > 1) {
                          Title = parts[1];}
        Apitaking a1 = new Apitaking();
        
        DAO d =new DAO();
        String trueText=a1.colorTextRepeat2(text, Title);
        List<String> errorsG = a1.getRepeatWord(text, "1");
        List<String> errorsG2 = a1.getRepeatWord(text, "2");
        List<String> errorsG3 = a1.getRepeatWord(text, "3");
        List<String> errorsG4 = a1.getRepeatWord(text, "4");
        req.setAttribute("inputText", text);
        req.setAttribute("ErrorA1", errorsG);
        req.setAttribute("ErrorA2", errorsG2);
        req.setAttribute("ErrorA3", errorsG3);
        req.setAttribute("errorwoed", error);
        req.setAttribute("ErrorA4", errorsG4);
        req.setAttribute("inputText", trueText);
         req.getRequestDispatcher("showErrorRepeatFixFollowFrequent.jsp").forward(req, resp);}
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession ses = request.getSession();
        String text= (String)ses.getAttribute("inputText");
        if(text==null){
            response.sendRedirect("checkFunction.jsp");
        }else{
            ses.setAttribute("inputText", text);
        int id = Integer.parseInt(request.getParameter("idText"));
        String trueText="";
        Apitaking a1 = new Apitaking();
        ses.getAttribute("");
        DAO d =new DAO();trueText = a1.colorTextRepeat1(text,id);
        List<String> errorsG = a1.getRepeatWord(text, "1");
        List<String> errorsG2 = a1.getRepeatWord(text, "2");
        List<String> errorsG3 = a1.getRepeatWord(text, "3");
        List<String> errorsG4 = a1.getRepeatWord(text, "4");
        ses.setAttribute("inputText", text);
        request.setAttribute("ErrorA1", errorsG);
        request.setAttribute("errorwoed", id);
        request.setAttribute("ErrorA2", errorsG2);
        request.setAttribute("ErrorA3", errorsG3);
        request.setAttribute("ErrorA4", errorsG4);
        request.setAttribute("inputText", trueText);
        request.getRequestDispatcher("showErrorRepeatFixFollowFrequent.jsp").forward(request, response);}
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
