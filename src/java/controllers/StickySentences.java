package controllers;

import ProWritingAid.ApiClient;
import ProWritingAid.ApiException;
import ProWritingAid.Configuration;
import ProWritingAid.SDK.DocTag;
import ProWritingAid.SDK.TextAnalysisRequest;
import ProWritingAid.SDK.TextAnalysisResponse;
import ProWritingAid.SDK.TextApi;
import ProWritingAid.auth.ApiKeyAuth;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import models.Account;
import org.json.JSONArray;
import org.json.JSONObject;

@WebServlet(name="StickySentences", urlPatterns={"/sticky"})
public class StickySentences extends HttpServlet {
   
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String text = request.getParameter("inputText");
        HttpSession ses = request.getSession();
        Account ac = (Account)ses.getAttribute("us");
            request.setAttribute("ac", ac);
        if (text == null || text.trim().isEmpty()) {
            request.setAttribute("errorMessage", "Please enter text for sticky check.");
            request.getRequestDispatcher("stickyCheck.jsp").forward(request, response);
            return;
        }

        List<JSONObject> errorsAndSuggestions = getStickySentencesErrorsAndSuggestions(text);

        request.setAttribute("inputText", text);
        request.setAttribute("errorsAndSuggestions", new JSONArray(errorsAndSuggestions).toString());
        request.getRequestDispatcher("stickyCheck.jsp").forward(request, response);
    }

    private List<JSONObject> getStickySentencesErrorsAndSuggestions(String text) {
        List<JSONObject> errorsAndSuggestions = new ArrayList<>();

        ApiClient defaultClient = Configuration.getDefaultApiClient();

        ApiKeyAuth licenseCode = (ApiKeyAuth) defaultClient.getAuthentication("licenseCode");
        licenseCode.setApiKey("1FBEFF86-325D-42A0-8487-EEC552BBF866");

        TextApi apiInstance = new TextApi();
        try {
            TextAnalysisRequest request = new TextAnalysisRequest();
            request.setText(text);
            request.setReports(Arrays.asList("ssentences"));
            request.setStyle(TextAnalysisRequest.StyleEnum.GENERAL);
            request.setLanguage(TextAnalysisRequest.LanguageEnum.EN);

            TextAnalysisResponse response = apiInstance.post(request).getResult();

            for (DocTag tag : response.getTags()) {
                if (tag.getCategory().equals("stickysentences")) {
                    JSONObject obj = new JSONObject();
                    obj.put("startPos", tag.getStartPos());
                    obj.put("endPos", tag.getEndPos());
                    obj.put("subcategory", tag.getSubcategory());
                    obj.put("hint", tag.getHint());
                    errorsAndSuggestions.add(obj);
                }
            }
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }

        return errorsAndSuggestions;
    }
}
