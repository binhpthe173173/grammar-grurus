/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ProWritingAid.*;
import ProWritingAid.auth.*;
import ProWritingAid.SDK.*;
import jakarta.servlet.http.HttpSession;
import java.util.*;

/**
 *
 * @author Admin
 */
public class Overview extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Overview</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Overview at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String inputText = request.getParameter("inputText");
        if(inputText==null){
            response.sendRedirect("grammarFunction");
        }else{
            HttpSession ses = request.getSession();
            ses.setAttribute("inputText", inputText);
        request.setAttribute("inputText", inputText);
        ArrayList<String> result = getOverview(inputText);
        request.setAttribute("result", result);
        request.getRequestDispatcher("overview.jsp").forward(request, response);}
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public List<AnalysisSummaryItem> getOverviewList(String text) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();

        // Configure API key authorization: licenseCode
        ApiKeyAuth licenseCode = (ApiKeyAuth) defaultClient.getAuthentication("licenseCode");
        licenseCode.setApiKey("1FBEFF86-325D-42A0-8487-EEC552BBF866");

        TextApi apiInstance = new TextApi();
        try {
            TextAnalysisRequest request = new TextAnalysisRequest();
            request.setText(text);
            request.setReports(Arrays.asList("overview"));
            request.setStyle(TextAnalysisRequest.StyleEnum.GENERAL);
            request.setLanguage(TextAnalysisRequest.LanguageEnum.EN);
            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            return result.getResult().getSummaries().get(0).getSummaryItems();
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }
        return null;
    }
    public ArrayList<String> getOverview(String text) {
        List<AnalysisSummaryItem> ls = getOverviewList(text);
        ArrayList<String> result = new ArrayList<>();
        int[] pick = {0, 1, 2, 3, 5, 6, 9, 10};
        for (int i = 0; i < pick.length; i++){
            result.add(ls.get(pick[i]).getText());
        }
        int[] pick_score = {1, 3, 4, 5};
        for (int i = 0; i < pick_score.length; i++) {
            result.add(ls.get(4).getSubItems().get(pick_score[i]).getText());
        }
        System.out.println(ls);
        System.out.println(ls.get(4).getSubItems().get(0).getText());
        return result;
    }
}
