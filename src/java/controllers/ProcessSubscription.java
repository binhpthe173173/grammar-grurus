package controllers;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import models.Account;
import models.AccountSubscription;
import models.Subscription;

public class ProcessSubscription extends HttpServlet {

    DAO dao;

    @Override
    public void init() {
        dao = new DAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int subscriptionId = Integer.parseInt(request.getParameter("subscriptionId"));
        HttpSession ses = request.getSession();
        Account ac = (Account) ses.getAttribute("us");
        int account_id = ac.getId();
        Subscription sub = dao.getSubscriptionById(subscriptionId);
        List<AccountSubscription> subs = dao.getAccountSubscriptionsByAccountId(account_id);
        boolean success = dao.processSubscription(account_id, subscriptionId);
        if (success) {
            if (subs.isEmpty()) {
                dao.addAccountSubscription(account_id, subscriptionId, LocalDateTime.now(), LocalDateTime.now().plusDays(sub.getDuration()));
                dao.updatePremium(account_id);
                ac.setRole_id(2);
                response.sendRedirect(request.getContextPath() + "/subscriptionSuccess.jsp");
            } else {
                AccountSubscription last_as = null;
                for (AccountSubscription as : subs) {
                    if (as.getAccountId() == account_id) {
                        last_as = as;
                    }
                }
                dao.addAccountSubscription(account_id, subscriptionId, last_as.getEndDate(), last_as.getEndDate().plusDays(sub.getDuration()));
                response.sendRedirect(request.getContextPath() + "/subscriptionSuccess.jsp");
            }
        } else {
            response.sendRedirect(request.getContextPath() + "/subscriptionFailure.jsp");
        }
    }
}
