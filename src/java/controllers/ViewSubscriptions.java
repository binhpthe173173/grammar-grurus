
package controllers;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import models.Account;
import models.Subscription;

public class ViewSubscriptions extends HttpServlet {
    
   
    DAO dao;

    @Override
    public void init() {
        dao = new DAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession ses = req.getSession();
        Account ac = (Account)ses.getAttribute("us");
        if(ac==null){
            resp.sendRedirect("login.jsp");
        }else{
        dao.LoadSubscription();
        req.setAttribute("dao", dao);
        
        req.getRequestDispatcher("/viewSubscription.jsp").forward(req, resp);}
    }
}
