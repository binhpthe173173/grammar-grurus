/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.util.concurrent.Delayed;
import models.Account;
import models.Account;

/**
 *
 * @author admin
 */
@WebServlet(name="changePass", urlPatterns={"/changePass"})
public class changePass extends HttpServlet {
  //HoanBuCaavCho
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    }
    
    
    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String ms="";
        DAO u= new DAO();
         Account ac ;
       ac=(Account) request.getSession().getAttribute("us");
       
      String opass =request.getParameter("oldpass");
      String pass =request.getParameter("pass");
      String repass =request.getParameter("repass");
      String getac = ac.getEmail();
      
      if (ac == null) {
        ms = "User not logged in";
        request.setAttribute("ms", ms);
        request.getRequestDispatcher("changepass.jsp").forward(request, response);
        return;}
      if (u.getAccountByMail(getac).getPassword().equals(opass))
      { 
          if (!u.checkPassword(pass)) {
            ms = "Password must contains uppercase, lowercase and at least a number";
           request.setAttribute("ms", ms);
        request.getRequestDispatcher("changepass.jsp").forward(request, response);
        return;
        } 
         
          if(!pass.equals(repass)){
        ms="Reset password is not the same";
        request.setAttribute("ms", ms);
       
        request.getRequestDispatcher("changepass.jsp").forward(request, response);
        return;
        }  
          
         
          
           u.changePassByIdAc(ac.getId(), pass);
               
                ms ="Updated password successfully" ;
                request.setAttribute("ms", ms);
                request.getRequestDispatcher("changepass.jsp").forward(request, response);
                return;
            }
      else 
      {
           ms="Old password incorrec";
        request.setAttribute("ms", ms);
       
        request.getRequestDispatcher("Changepass.jsp").forward(request, response);
        
      
      }
          request.removeAttribute("ms");
      }
       
               
       
            
    
    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
