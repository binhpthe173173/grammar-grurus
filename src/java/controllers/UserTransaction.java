/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import models.Account;
import models.Transaction;
import models.Wallet;

/**
 *
 * @author Admin
 */
public class UserTransaction extends HttpServlet {
    DAO dao;
    public void init() {
        dao = new DAO();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession ses = request.getSession();
        Account ac = (Account)ses.getAttribute("us");
        Wallet w = dao.getWalletByAccountId(ac.getId());
        ArrayList<Transaction> trans = new ArrayList<>(dao.getTransactionByWalletId(w.getId()));
        request.setAttribute("transactions", trans);
        request.getRequestDispatcher("userTransaction.jsp").forward(request, response);
    } 
}
