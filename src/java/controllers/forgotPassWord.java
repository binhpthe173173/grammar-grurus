/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;
import dal.DAO;
import models.Account;
import models.Account;
import java.util.*;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author admin
 */
@WebServlet(name="Forgot", urlPatterns={"/Forgot"})
public class forgotPassWord extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String mailF = request.getParameter("email");
         String nameact = request.getParameter("nameac");
         mailF= mailF.trim();
         nameact=nameact.trim();
        String ms=null,check;
        
        autoSendEmail e = new autoSendEmail();
       DAO u= new DAO();
       String newpassword = "Npw";
               
        if (u.getAccountByMail(mailF)==null){
        ms="Email not found";
        
        request.setAttribute("ms", ms);
        request.getRequestDispatcher("forgot.jsp").forward(request, response);
        }
        if( !u.getAccountByMail(mailF).getUsername().equalsIgnoreCase(nameact) ){
            ms="username is not compatible with email";
            request.setAttribute("ms", ms);
        request.getRequestDispatcher("forgot.jsp").forward(request, response);
        return;
            }
        else{
            
        newpassword= newpassword+ e.createCodeRandom();
        
        }
       String nameac= u.getAccountByMail(mailF).getFull_name();
        String g=e.sendEmail(mailF, " Hello "+ nameac+"! your new pass is "+newpassword,"new pass");
        if (g=="no"){
            ms="Sending new password failed,check the connection again"; 
            request.setAttribute("ms", ms);
            request.getRequestDispatcher("forgot.jsp").forward(request, response);
        }
         ms="New password sent to Email successfully";
       check ="pass";
       u.changePassByIdAc(u.getAccountByMail(mailF).getId(), newpassword);
      // u.updateUserById(u.getAccount(mailF).getIdAccount(), newpassword);
       // List<Account> lst= u.getUsers();
         request.setAttribute("check", check);
        request.setAttribute("ms", ms);
       // request.setAttribute("lst",lst);
        request.getRequestDispatcher("forgot.jsp").forward(request, response);
       
        }
    
    

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    
        request.getRequestDispatcher("list.jsp").forward(request, response);
        DAO u = new DAO();
        u.getAccountById(0);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
