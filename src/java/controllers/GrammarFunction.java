/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;

import ApiAnswer.Apitaking;
import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import models.Account;

/**
 *
 * @author admin
 */
public class GrammarFunction extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GrammarFunction</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet GrammarFunction at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession ses = request.getSession();
        Account ac = (Account)ses.getAttribute("us");
        if(ac==null){
            response.sendRedirect("login.jsp");
        }else{
        request.setAttribute("ac", ac);
        request.getRequestDispatcher("checkFunction.jsp").forward(request, response);
        }} 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String a = (String)request.getParameter("inputText");
        if(a==null){
            response.sendRedirect("checkFunction.jsp");
        }else{
        Apitaking a1 =new Apitaking();
        List<String> errorsGrammar = a1.getGrammarErrors(a);
        List<String> GrammarSuggestion = a1.getGrammarSuggestions(a);
        HttpSession ses = request.getSession();
        Account a2 =  (Account)ses.getAttribute("us"); 
        if(a2==null){
            response.sendRedirect("login.jsp");
        }else{
            ses.setAttribute("inputText", a);
        DAO d =new DAO();
        d.createPost(a, null, a2.getId());
        int idp = d.getPostByContent(a,a2.getId()).getId();
        for(String t:errorsGrammar){
            for(String y:GrammarSuggestion){
                d.createError("Grammar:"+t,"Grammar:"+ y, idp);
            }
        }
        request.setAttribute("ac", a2);
        request.setAttribute("inputText", a);
        request.setAttribute("text1", a1.textGrammar(a));
        request.setAttribute("suggestion", GrammarSuggestion);
        request.setAttribute("error", errorsGrammar);
        request.getRequestDispatcher("showErrorGrammarFix.jsp").forward(request, response);}}
    }
    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
