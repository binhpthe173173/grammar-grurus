/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import ProWritingAid.*;
import ProWritingAid.auth.*;
import ProWritingAid.SDK.*;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.*;
import models.Account;

/**
 *
 * @author admin
 */
@WebServlet(name = "Cliche", urlPatterns = {"/Cliche"})
public class Cliches extends HttpServlet {

    ApiClient defaultClient;

    TextAnalysisRequest request1;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)

            throws ServletException, IOException {

        
        String a = (String) request.getParameter("inputText");
        if(a==null){
            response.sendRedirect("checkFunction.jsp");
        }else{
            HttpSession ses = request.getSession();
            Account ac = (Account)ses.getAttribute("us");
            request.setAttribute("ac", ac);
            ses.setAttribute("inputText", a);
        List<String> lstrodunbound = new ArrayList<>();
        List<String> lstcliche = new ArrayList<>();
        defaultClient = Configuration.getDefaultApiClient();

        // Configure API key authorization: licenseCode
        ApiKeyAuth licenseCode = (ApiKeyAuth) defaultClient.getAuthentication("licenseCode");
        licenseCode.setApiKey("1FBEFF86-325D-42A0-8487-EEC552BBF866");

        TextApi apiInstance = new TextApi();
        try {
            request1 = new TextAnalysisRequest();
            request1.setText(a);
            request1.setReports(Arrays.asList("cliche"));
            request1.setStyle(TextAnalysisRequest.StyleEnum.GENERAL);
            request1.setLanguage(TextAnalysisRequest.LanguageEnum.EN);

            AsyncResponseTextAnalysisResponse result = apiInstance.post((TextAnalysisRequest) request1);
            TextAnalysisResponse response1 = result.getResult();

            System.out.println(response);

            // Extract Frequent 4-Word Phrases
            for (DocTag tag : response1.getTags()) {
                if (tag.getCategory().equals("cliche")) {

                    System.out.println("Error: " + tag.getSubcategory());
                    lstcliche.add((String) tag.getSubcategory());
                    System.out.println(tag);
                }
            }

            request.setAttribute("inputText", a);
            request.setAttribute("lc", lstcliche);

            request.getRequestDispatcher("clicheCheck.jsp").forward(request, response);
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();

        }
    }
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
