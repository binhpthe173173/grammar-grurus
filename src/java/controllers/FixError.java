/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;

import ApiAnswer.Apitaking;
import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import models.Account;

/**
 *
 * @author admin
 */
public class FixError extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FixError</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FixError at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

     // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         String a = (String) request.getParameter("inputText");
         if(a==null){
            response.sendRedirect("checkFunction.jsp");
        }else{
            HttpSession ses = request.getSession();
            Account ac = (Account)ses.getAttribute("us");
            request.setAttribute("ac", ac);
            ses.setAttribute("inputText", a);
       String a11 = (String) request.getParameter("text1");
       String score = (String)request.getParameter("score");
        Apitaking a1 =new Apitaking();
         
          Account a2 =  (Account)ses.getAttribute("us");
          if(a2==null){
            response.sendRedirect("login.jsp");
        }else{
          DAO d =new DAO();
          int idp = d.getPostByContent(a,a2.getId()).getId();
          d.updatePost(idp, a1.correctTextS(a), a2.getId());
        request.setAttribute("inputText", a);
        request.setAttribute("text1", a11);
        request.setAttribute("suggestion", a1.getSpellingSuggestions(a));
        request.setAttribute("error", a1.getSpellingErrors(a));
        request.setAttribute("fixText", a1.correctTextS(a));
        request.setAttribute("score", score);
        request.getRequestDispatcher("showErrorSpellFixText.jsp").forward(request, response);}
          }
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       String a = (String) request.getParameter("inputText");
       String a11 = (String) request.getParameter("text1");
       
        Apitaking a1 =new Apitaking();
        HttpSession ses = request.getSession();
          Account a2 =  (Account)ses.getAttribute("us");
          if(a2==null){
            response.sendRedirect("login.jsp");
        }else{
          DAO d =new DAO();
          if(d.getPostByContent(a,a2.getId())==null){
               d.createPost(a, null, a2.getId());
               int idp = d.getPostByContent(a,a2.getId()).getId();
        for(String t:a1.getGrammarErrors(a)){
            for(String y:a1.getGrammarSuggestions(a)){
                d.createError("Grammar:"+t,"Grammar:"+ y, idp);
            }
        }
          }
          int idp = d.getPostByContent(a,a2.getId()).getId();
          
          d.updatePost(idp, a1.correctTextS(a), a2.getId());
        request.setAttribute("inputText", a);
        request.setAttribute("text1", a11);
        request.setAttribute("suggestion", a1.getGrammarSuggestions(a));
        request.setAttribute("error", a1.getGrammarErrors(a));
        request.setAttribute("fixText", a1.correctTextG(a));
        request.getRequestDispatcher("showErrorGrammarFixText.jsp").forward(request, response);
    }
    }
    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
