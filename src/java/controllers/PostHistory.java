package controllers;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.*;
import models.*;

public class PostHistory extends HttpServlet {

    DAO dao;

    @Override
    public void init() {
        dao = new DAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession ses = request.getSession();
        Account ac = (Account) ses.getAttribute("us");
        if(ac==null){
            response.sendRedirect("login.jsp");
        }else{
        String post_id = request.getParameter("id");
        if (post_id == null) {
            List<Post> postHistory = dao.getPostsByAccountId(ac.getId());
            request.setAttribute("postHistory", postHistory);
            request.getRequestDispatcher("postHistory.jsp").forward(request, response);
        } else {
            int id = Integer.parseInt(post_id);
            request.setAttribute("id", id);
            Post post = dao.getPostbyId(id);
            if (post == null) {
                response.sendRedirect(request.getContextPath() + "/postHistory");
            } else {
                List<models.Error> errors = dao.getErrorsByPostId(id);
                request.setAttribute("errors", errors);
                request.setAttribute("post", post);
                request.getRequestDispatcher("postHistory.jsp").forward(request, response);
            }
        }}
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("postTitle");
        int postId = Integer.parseInt(req.getParameter("postId"));
        dao.renamePost(postId, title);
        doGet(req, resp);
    }
}
