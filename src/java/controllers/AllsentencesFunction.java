/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controllers;

import ApiAnswer.Apitaking;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ProWritingAid.*;
import ProWritingAid.auth.*;
import ProWritingAid.SDK.*;
import jakarta.servlet.http.HttpSession;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import models.Account;

/**
 *
 * @author admin
 */
public class AllsentencesFunction extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AllsentencesFunction</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AllsentencesFunction at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        doPost(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         String a = (String) request.getParameter("inputText");
         if(a==null){
            response.sendRedirect("checkFunction.jsp");
        }else{
            HttpSession ses = request.getSession();
            Account ac = (Account)ses.getAttribute("us");
            request.setAttribute("ac", ac);
            ses.setAttribute("inputText", a);
          Apitaking a1 =new Apitaking();
          
          request.setAttribute("num55", a1.BTextNL(a, 12));
          request.setAttribute("num5", a1.BTextN(a, 8));
          request.setAttribute("num48", a1.BTextN(a, 7));
          request.setAttribute("num45",a1.BTextNL(a, 10));
          request.setAttribute("num43",a1.BTextS(a));
          request.setAttribute("num41",a1.BTextN(a, 6) );
          request.setAttribute("num4",a1.BTextN(a, 5) );
          request.setAttribute("num35",a1.BTextN(a, 4) );
          request.setAttribute("num3", a1.BTextN(a, 3));
          request.setAttribute("num2", a1.BTextN(a, 2));
          request.setAttribute("num1", a1.BTextN(a, 1));
          request.setAttribute("text1", a1.textAllsentence(a));
          request.setAttribute("inputText", a);
          request.getRequestDispatcher("textAllsentence.jsp").forward(request, response);}
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
