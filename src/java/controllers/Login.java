/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controllers;

import GoogleLogin.*;
import dal.DAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.*;

/**
 *
 * @author Admin
 */
public class Login extends HttpServlet {

    DAO dao;
    String err = "";

    @Override
    public void init() {
        dao = new DAO();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Login</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String code = request.getParameter("code");
        if (code == null || code.isEmpty()) {
            RequestDispatcher dis = request.getRequestDispatcher("login.jsp");
            dis.forward(request, response);
        } else {
            String accessToken = GoogleUtils.getToken(code);
            GooglePojo googlePojo = GoogleUtils.getUserInfo(accessToken);
            String email = googlePojo.getEmail();
            if (dao.getAccountByMail(email) == null) {
                String name = googlePojo.getName();
                String picture = googlePojo.getPicture();
                response.sendRedirect(request.getContextPath() + "/register?name=" + name + "&email=" + email + "&picture=" + picture);
            } else {
                Account ac = dao.getAccountByMail(email);
                HttpSession ses = request.getSession();
                
                ses.setAttribute("us", ac);
                if (ac.getRole_id() == 0) {
                    response.sendRedirect(request.getContextPath() + "/Admin/homePage");

                } else {
                    response.sendRedirect(request.getContextPath() + "/grammarFunction");
                }
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (dao.checkEmail(username)) {
            Account ac = dao.getAccountByMail(username);
            if (ac == null) {
                err = "Wrong username or password";
                request.setAttribute("err", err);
                request.getRequestDispatcher("login.jsp").forward(request, response);
            } else {
                if (ac.getPassword().equals(password) && ac.isActive_status()) {
                    HttpSession ses = request.getSession();
                    ses.setAttribute("us", ac);
                    response.sendRedirect(request.getContextPath() + "/grammarFunction");
                } else if (ac.getPassword().equals(password) && !ac.isActive_status()) {
                    err = "Your account has been locked! Contact admin for more information.";
                    request.setAttribute("err", err);
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                } else {
                    err = "Wrong username or password";
                    request.setAttribute("err", err);
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                }
            }
        } else {
            Account ac = dao.getAccountByUsername(username);
            if (ac == null) {
                err = "Wrong username or password";
                request.setAttribute("err", err);
                request.getRequestDispatcher("login.jsp").forward(request, response);
            } else {
                if (ac.getPassword().equals(password) && ac.isActive_status()) {

                    HttpSession ses = request.getSession();
                    ses.setAttribute("us", ac);
                    if (ac.getRole_id() == 0) {
                        response.sendRedirect(request.getContextPath() + "/Admin/homePage");
                    } else {
                        response.sendRedirect(request.getContextPath() + "/grammarFunction");

                    }
                } else if (ac.getPassword().equals(password) && !ac.isActive_status()) {
                    err = "Your account has been locked! Contact admin for more information.";
                    request.setAttribute("err", err);
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                } else {
                    err = "Wrong username or password";
                    request.setAttribute("err", err);
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                }
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
