/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ApiAnswer;

import java.util.List;

/**
 *
 * @author admin
 */
public class ErrorInfo {
    private String error;
    private List<String> suggestions;
    private int startPos;
    private int endPos;

    public ErrorInfo() {
    }

    public ErrorInfo(String error, List<String> suggestions, int startPos, int endPos) {
        this.error = error;
        this.suggestions = suggestions;
        this.startPos = startPos;
        this.endPos = endPos;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<String> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<String> suggestions) {
        this.suggestions = suggestions;
    }

    public int getStartPos() {
        return startPos;
    }

    public void setStartPos(int startPos) {
        this.startPos = startPos;
    }

    public int getEndPos() {
        return endPos;
    }

    public void setEndPos(int endPos) {
        this.endPos = endPos;
    }
    
}
