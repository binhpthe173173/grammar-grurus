/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ApiAnswer;

/**
 *
 * @author admin
 */
import ProWritingAid.*;
import ProWritingAid.auth.*;
import ProWritingAid.SDK.*;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

public class Apitaking {

    //api calling
    private static final String API_KEY = "1FBEFF86-325D-42A0-8487-EEC552BBF866";
    private static final String grammaranspelling = "grammar";
    private static final String allrepeat = "allrepeats";
    private static final TextAnalysisRequest.StyleEnum STYLE = TextAnalysisRequest.StyleEnum.GENERAL;
    private static final TextAnalysisRequest.LanguageEnum LANGUAGE = TextAnalysisRequest.LanguageEnum.EN;
    private static final HtmlAnalysisRequest.StyleEnum STYLE1 = HtmlAnalysisRequest.StyleEnum.GENERAL;
    private static final HtmlAnalysisRequest.LanguageEnum LANGUAGE1 = HtmlAnalysisRequest.LanguageEnum.EN;
    
    private TextApi apiInstance;
    private HtmlApi apiInstance1;
    
    private TextAnalysisRequest createRequest(String text, String REPORT_TYPE) {
        TextAnalysisRequest request = new TextAnalysisRequest();
        request.setText(text);
        request.setReports(Arrays.asList(REPORT_TYPE));
        request.setStyle(STYLE);
        request.setLanguage(LANGUAGE);
        return request;
    }

    private HtmlAnalysisRequest createRequest1(String text, String REPORT_TYPE) {
        HtmlAnalysisRequest request = new HtmlAnalysisRequest();
        request.setHtml(text);
        request.setReports(Arrays.asList(REPORT_TYPE));
        request.setStyle(STYLE1);
        request.setLanguage(LANGUAGE1);
        return request;
    }
    
    public Apitaking() {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        ApiKeyAuth licenseCode = (ApiKeyAuth) defaultClient.getAuthentication("licenseCode");
        licenseCode.setApiKey(API_KEY);
        apiInstance = new TextApi();
        apiInstance1 = new HtmlApi();
    }

    //grammar and spelling
    //grammar
    public List<String> getGrammarSuggestions(String text) {
        List<String> errorsAndSuggestions = new ArrayList<>();

        // Configure API key authorization: licenseCode
        try {
            TextAnalysisRequest request = createRequest(text, grammaranspelling);
            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();

            for (DocTag tag : response.getTags()) {
                if (tag.getCategory().equals("grammargrammar")) {
                    if (tag.getReport().equals("grammar")) {
                        errorsAndSuggestions.add("|" + String.join("| ,|", tag.getSuggestions()) + "| ");
                    }
                } else {
                    System.out.println("//");
                }
            }
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }
        return errorsAndSuggestions;
    }

    public List<String> getGrammarErrors(String text) {
        List<String> errorsAndSuggestions = new ArrayList<>();
        try {
            TextAnalysisRequest request = createRequest(text, grammaranspelling);

            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();
            for (DocTag tag : response.getTags()) {
                if (tag.getCategory().equals("grammargrammar")) {
                    if (tag.getReport().equals("grammar")) { // change this line
                        errorsAndSuggestions.add(tag.getHint() + ":" + tag.getSubcategory());
                    }
                } else {
                    System.out.println("//");
                }
            }
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }

        return errorsAndSuggestions;
    }

    public static String highlightErrorsGrammar(String text, List<DocTag> tags) {
        StringBuilder sb = new StringBuilder();
        int lastIndex = 0;
        for (DocTag tag : tags) {
            if (tag.getCategory().equals("grammargrammar")) {
                sb.append(text.substring(lastIndex, tag.getStartPos()));
                sb.append("<button class='error-button' style='border: none; border-bottom: 2px solid red;' Errorfix='" + tag.getSubcategory() + "' text-def='" + text + "' data-error='" + tag.getHint() + "' data-suggestions='" + String.join(", ", tag.getSuggestions()) + "'>");
                sb.append(text.substring(tag.getStartPos(), tag.getEndPos() + 1));
                sb.append("</button>");
                lastIndex = tag.getEndPos() + 1;
            }
        }
        sb.append(text.substring(lastIndex));
        return sb.toString();
    }

    public String textGrammar(String a) {
        String a1 = new String();
        try {
            TextAnalysisRequest request = createRequest(a, grammaranspelling);

            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();
            a1 = highlightErrorsGrammar(a, response.getTags());
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }

        return a1;
    }

    public String correctTextG(String text) {
        String a1 = new String();
        try {
            TextAnalysisRequest request = createRequest(text, grammaranspelling);

            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();
            List<DocTag> tags = response.getTags();
            System.out.println(result);
            // Apply corrections to the text based on suggestions
            StringBuilder correctedText = new StringBuilder(text);
            int offset = 0; // To adjust for changes in text length

            for (DocTag tag : tags) {
                if (tag.getCategory().equalsIgnoreCase("grammargrammar")) {
                    String suggestion = tag.getSuggestions().get(0); // Assuming only one suggestion for simplicity
                    int startPos = tag.getStartPos() + offset;
                    int endPos = tag.getEndPos() + 1 + offset;

                    // Apply the suggestion to the text
                    correctedText.replace(startPos, endPos, suggestion);

                    // Adjust offset based on the length difference between original and replacement
                    offset += suggestion.length() - (endPos - startPos);
                }
            }

            // Print the corrected text
            a1 = correctedText.toString();

        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }
        return a1;
    }
//spelling

    public String correctTextS(String text) {
        String a1 = new String();
        try {
            TextAnalysisRequest request = createRequest(text, grammaranspelling);

            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();
            List<DocTag> tags = response.getTags();
            System.out.println(result);
            // Apply corrections to the text based on suggestions
            StringBuilder correctedText = new StringBuilder(text);
            int offset = 0; // To adjust for changes in text length

            for (DocTag tag : tags) {
                if (tag.getCategory().equalsIgnoreCase("grammarspelling")) {
                    String suggestion = tag.getSuggestions().get(0); // Assuming only one suggestion for simplicity
                    int startPos = tag.getStartPos() + offset;
                    int endPos = tag.getEndPos() + 1 + offset;

                    // Apply the suggestion to the text
                    correctedText.replace(startPos, endPos, suggestion);

                    // Adjust offset based on the length difference between original and replacement
                    offset += suggestion.length() - (endPos - startPos);
                }
            }

            // Print the corrected text
            a1 = correctedText.toString();

        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }
        return a1;
    }

    public static String highlightErrorsSpelling(String text, List<DocTag> tags) {
        StringBuilder sb = new StringBuilder();
        int lastIndex = 0;
        for (DocTag tag : tags) {
            if (tag.getCategory().equals("grammarspelling")) {
                sb.append(text.substring(lastIndex, tag.getStartPos()));
                sb.append("<button class='error-button' style='border: none; border-bottom: 2px solid red;' Errorfix='" + tag.getSubcategory() + "' text-def='" + text + "' data-error='" + tag.getHint() + "' data-suggestions='" + String.join(", ", tag.getSuggestions()) + "'>");
                sb.append(text.substring(tag.getStartPos(), tag.getEndPos() + 1));
                sb.append("</button>");
                lastIndex = tag.getEndPos() + 1;
            }
        }
        sb.append(text.substring(lastIndex));
        return sb.toString();
    }

    public String textSpelling(String a) {
        String a1 = new String();
        try {
            TextAnalysisRequest request = createRequest(a, grammaranspelling);

            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();
            a1 = highlightErrorsSpelling(a, response.getTags());
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }

        return a1;
    }

    public List<String> getSpellingSuggestions(String text) {
        List<String> errorsAndSuggestions = new ArrayList<>();

        try {
            TextAnalysisRequest request = createRequest(text, grammaranspelling);
            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();

            for (DocTag tag : response.getTags()) {
                if (tag.getCategory().equals("grammarspelling") && tag.getReport().equals(grammaranspelling)) {
                    errorsAndSuggestions.add("|" + String.join("| ,|", tag.getSuggestions()) + "| ");

                }
            }
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }

        return errorsAndSuggestions;
    }

    public String getErrorScore(String text) {
        try {
            TextAnalysisRequest request = createRequest(text, grammaranspelling);
            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();

            for (AnalysisSummary summary : response.getSummaries()) {
                if (summary.getReportName().equals(grammaranspelling)) {
                    for (AnalysisSummaryItem item : summary.getSummaryItems()) {
                        if (item.getText().startsWith("Spelling Score")) {
                            return item.getText();
                        }
                    }
                }
            }
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }

        return "";
    }

    public List<String> getSpellingErrors(String text) {
        List<String> errorsAndSuggestions = new ArrayList<>();

        try {
            TextAnalysisRequest request = createRequest(text, grammaranspelling);
            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();

            for (DocTag tag : response.getTags()) {
                if (tag.getCategory().equals("grammarspelling") && tag.getReport().equals(grammaranspelling)) {
                    errorsAndSuggestions.add(tag.getHint());
                }
            }
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }

        return errorsAndSuggestions;
    }
//repeated 
    //repeat 1

    public List<String> getRepeatWord(String text, String n) {
        List<String> repeatWord = new ArrayList<>();
        try {
            TextAnalysisRequest request = createRequest(text, allrepeat);
            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();
            List<AnalysisSummaryItem> summaryItems = response.getSummaries().get(0).getSummaryItems();
            for (AnalysisSummaryItem item : summaryItems) {
                if (item.getText().equals("Frequent " + n + "-Word Phrases")) {
                    List<AnalysisSummarySubItem> subItems = item.getSubItems();
                    for (AnalysisSummarySubItem subItem : subItems) {
                        repeatWord.add(subItem.getText());
                    }
                }
            }
        } catch (Exception e) {//
        }
        return repeatWord;
    }

    public List<String> analyzeRepeat(String text, String n) {

        // Configure API key authorization: licenseCode
        TextAnalysisRequest request = createRequest(text, allrepeat);

        TextApi apiInstance = new TextApi();
        List<String> suggestions = new ArrayList<>();

        try {
            createRequest(text, allrepeat);
            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();

            // Extract Frequent 4-Word Phrases
            List<AnalysisSummaryItem> summaryItems = response.getSummaries().get(0).getSummaryItems();
            for (AnalysisSummaryItem item : summaryItems) {
                if (item.getText().equals("Frequent " + n + "-Word Phrases")) {
                    List<AnalysisSummarySubItem> subItems = item.getSubItems();
                    for (AnalysisSummarySubItem subItem : subItems) {
                        for (DocTag tag : response.getTags()) {
                            if (tag.getSubcategory().equalsIgnoreCase(subItem.getSubCategory())) {

                                suggestions.add(String.join(",", tag.getSubcategory()));
                            }
                        }
                    }
                }
            }
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }
        return suggestions;
    }

    public List<String> analyzeText(String text, String n) {

        // Configure API key authorization: licenseCode
        TextAnalysisRequest request = createRequest(text, allrepeat);

        TextApi apiInstance = new TextApi();
        List<String> suggestions = new ArrayList<>();

        try {
            createRequest(text, allrepeat);
            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();

            // Extract Frequent 4-Word Phrases
            List<AnalysisSummaryItem> summaryItems = response.getSummaries().get(0).getSummaryItems();
            for (AnalysisSummaryItem item : summaryItems) {
                if (item.getText().equals("Frequent " + n + "-Word Phrases")) {
                    List<AnalysisSummarySubItem> subItems = item.getSubItems();
                    for (AnalysisSummarySubItem subItem : subItems) {
                        for (DocTag tag : response.getTags()) {
                            if (tag.getSubcategory().equalsIgnoreCase(subItem.getSubCategory())) {
                                if (tag.getSuggestions().isEmpty()) {
                                    suggestions.add("space");
                                } else {
                                    suggestions.add(String.join(",", tag.getSuggestions()));

                                }
                            }
                        }
                    }
                }
            }
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }
        return suggestions;
    }
    public String colorTextRepeat2(String a,String replace) {
        TextAnalysisRequest request = createRequest(a, allrepeat);
        TextApi apiInstance = new TextApi();
        String a6 = new String();
        try {
            createRequest(a, allrepeat);
            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();
            List<DocTag> tags = response.getTags();

            // Apply formatting to the text based on the analysis results
            StringBuilder sb = new StringBuilder();
            int currentPos = 0;

            for (DocTag tag : tags) {
                int start = tag.getStartPos();
                int end = tag.getEndPos() + 1;
                List<String> v = new ArrayList<>();
                v.add("Replace word by Space");
                if (tag.getSuggestions().isEmpty()) {
                    tag.setSuggestions(v);
                }
                // Add the text before the error

                if (start > currentPos) {
                    sb.append(a, currentPos, start);
                }

                // Add the error with formatting (ensuring valid indices)
                if (start >= 0 && start < end && end <= a.length()) {
                    if (tag.getSubcategory().equalsIgnoreCase(replace)) {
                        sb.append("<button class='error-button' style='border: none;text-decoration: underline;border-bottom: 2px solid red;' data-error='").append(tag.getSubcategory())
                                .append("' data-suggestions='").append(String.join(", ", tag.getSuggestions()))
                                .append("'><font >")
                                .append(a.substring(start, end))
                                .append("</font></button>");
                    }
                    currentPos = end;
                }
            }

            // Add the text after the last error
            if (currentPos < a.length()) {
                sb.append(a.substring(currentPos));
            }

            // Print the formatted text to the console
            a6 = sb.toString();

        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }
        return a6;
    }
    public String colorTextRepeat1(String a, int n) {
        TextAnalysisRequest request = createRequest(a, allrepeat);
        TextApi apiInstance = new TextApi();
        String a6 = new String();
        try {
            createRequest(a, allrepeat);
            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();
            List<DocTag> tags = response.getTags();

            // Apply formatting to the text based on the analysis results
            StringBuilder sb = new StringBuilder();
            int currentPos = 0;

            for (DocTag tag : tags) {
                int start = tag.getStartPos();
                int end = tag.getEndPos() + 1;
                List<String> v = new ArrayList<>();
                v.add("Replace word by Space");
                if (tag.getSuggestions().isEmpty()) {
                    tag.setSuggestions(v);
                }
                // Add the text before the error

                if (start > currentPos) {
                    sb.append(a, currentPos, start);
                }

                // Add the error with formatting (ensuring valid indices)
                if (start >= 0 && start < end && end <= a.length()) {
                    if (tag.getSubcategory().split(" ").length == n) {
                        sb.append("<button class='error-button' style='border: none;text-decoration: underline;border-bottom: 2px solid red;' data-error='").append(tag.getSubcategory())
                                .append("' data-suggestions='").append(String.join(", ", tag.getSuggestions()))
                                .append("'><font >")
                                .append(a.substring(start, end))
                                .append("</font></button>");
                    }
                    currentPos = end;
                }
            }

            // Add the text after the last error
            if (currentPos < a.length()) {
                sb.append(a.substring(currentPos));
            }

            // Print the formatted text to the console
            a6 = sb.toString();

        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }
        return a6;
    }

    public String colorTextRepeat(String a) {
        TextAnalysisRequest request = createRequest(a, allrepeat);
        TextApi apiInstance = new TextApi();
        String a6 = new String();
        try {
            createRequest(a, allrepeat);
            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();
            List<DocTag> tags = response.getTags();

            // Apply formatting to the text based on the analysis results
            StringBuilder sb = new StringBuilder();
            int currentPos = 0;

            for (DocTag tag : tags) {
                int start = tag.getStartPos();
                int end = tag.getEndPos() + 1;
                List<String> v = new ArrayList<>();
                v.add("Replace word by Space");
                if (tag.getSuggestions().isEmpty()) {
                    tag.setSuggestions(v);
                }
                // Add the text before the error

                if (start > currentPos) {
                    sb.append(a, currentPos, start);
                }

                // Add the error with formatting (ensuring valid indices)
                if (start >= 0 && start < end && end <= a.length()) {
                    if (tag.getSubcategory().split(" ").length == 1) {
                        sb.append("<button style='border: none;text-decoration: underline;border-bottom: 2px solid red;' data-error='").append(tag.getSubcategory())
                                .append("' data-suggestions='").append(String.join(", ", tag.getSuggestions()))
                                .append("'><font >")
                                .append(a.substring(start, end))
                                .append("</font></button>");
                    } else if (tag.getSubcategory().split(" ").length == 2) {
                        sb.append("<button style='border: none; text-decoration: underline;border-bottom: 2px solid blue;' data-error='").append(tag.getSubcategory())
                                .append("' data-suggestions='").append(String.join(", ", tag.getSuggestions()))
                                .append("'><font >")
                                .append(a.substring(start, end))
                                .append("</font></button>");
                    } else if (tag.getSubcategory().split(" ").length == 3) {
                        sb.append("<button style='border: none;text-decoration: underline;border-bottom: 2px solid yellow;' data-error='").append(tag.getSubcategory())
                                .append("' data-suggestions='").append(String.join(", ", tag.getSuggestions()))
                                .append("'><font>")
                                .append(a.substring(start, end))
                                .append("</font></button>");
                    } else {
                        sb.append("<button style='border: none; text-decoration: underline; border-bottom: 2px solid green;' data-error='").append(tag.getSubcategory())
                                .append("' data-suggestions='").append(String.join(", ", tag.getSuggestions()))
                                .append("'><font>")
                                .append(a.substring(start, end))
                                .append("</font></button>");
                    }
                } else {
                    // Handle the case where start and end are invalid
                    System.err.println("Invalid start/end positions: start=" + start + ", end=" + end);
                }

                currentPos = end;
            }

            // Add the text after the last error
            if (currentPos < a.length()) {
                sb.append(a.substring(currentPos));
            }

            // Print the formatted text to the console
            a6 = sb.toString();

        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }
        return a6;
    }
    //tesing:

    public static String highlightErrors(String text, List<DocTag> tags) {
        StringBuilder sb = new StringBuilder();
        int lastIndex = 0;
        for (DocTag tag : tags) {
            if (tag.getCategory().equals("grammargrammar")) {
                sb.append(text.substring(lastIndex, tag.getStartPos()));
                sb.append("<button data-error='" + tag.getHint() + "' data-suggestions='" + String.join(", ", tag.getSuggestions()) + "'><font color='red'>");
                sb.append(text.substring(tag.getStartPos(), tag.getEndPos() + 1));
                sb.append("</font></button>");
                lastIndex = tag.getEndPos() + 1;
            } else {
                sb.append(text.substring(lastIndex, tag.getStartPos()));
                sb.append("<button data-error='" + tag.getHint() + "' data-suggestions='" + String.join(", ", tag.getSuggestions()) + "'><font color='blue'>");
                sb.append(text.substring(tag.getStartPos(), tag.getEndPos() + 1));
                sb.append("</font></button>");
                lastIndex = tag.getEndPos() + 1;
            }
        }
        sb.append(text.substring(lastIndex));
        return sb.toString();
    }

    public String text(String a) {
        String a1 = new String();
        try {
            TextAnalysisRequest request = createRequest(a, grammaranspelling);

            AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
            TextAnalysisResponse response = result.getResult();
            a1 = highlightErrors(a, response.getTags());
        } catch (ApiException e) {
            System.err.println("Exception when calling the API");
            e.printStackTrace();
        }

        return a1;
    }
//Alliteration Analysis
public String textAlliteration(String a){
    String a1 ="";
    try{
    HtmlAnalysisRequest re = createRequest1(a,"alliteration");
    AsyncResponseHtmlAnalysisResponse res = apiInstance1.post(re);
    HtmlAnalysisResponse resp = res.getResult();
    a1 = resp.getHtml();
    }catch(ApiException e){
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return a1;
}
public List<String> alliterationError(String a){
    List<String> a2 = new ArrayList<>();
    try{
    TextAnalysisRequest re = createRequest(a,"alliteration");
    AsyncResponseTextAnalysisResponse res = apiInstance.post(re);
    TextAnalysisResponse resp = res.getResult();
    
    for (DocTag tag : resp.getTags()) {
        a2.add(tag.getSubcategory());
    }
    }catch(ApiException e){
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return a2;
}
public String textAllsentence(String a){
    String a1 ="";
    try{
    HtmlAnalysisRequest re = createRequest1(a,"allsentences");
    AsyncResponseHtmlAnalysisResponse res = apiInstance1.post(re);
    HtmlAnalysisResponse resp = res.getResult();
    a1 = resp.getHtml();
    }catch(ApiException e){
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return a1;
}
public AnalysisSummary BText(String a){
   AnalysisSummary a2= null;
    try {
        TextAnalysisRequest re = createRequest(a,"allsentences");
         AsyncResponseTextAnalysisResponse res = apiInstance.post(re);
         TextAnalysisResponse resp = res.getResult();
         AnalysisSummary sU=resp.getSummaries().get(0);
         a2 = sU;
    } catch (Exception e) {
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return a2;
}
public String BTextN(String a,int b){
   String t="";
    try {
        TextAnalysisRequest re = createRequest(a,"allsentences");
         AsyncResponseTextAnalysisResponse res = apiInstance.post(re);
         TextAnalysisResponse resp = res.getResult();
         AnalysisSummary sU=resp.getSummaries().get(0);
         if(b==1){
            t= sU.getSummaryItems().get(0).getText();
         }else if(b==2){
             t= sU.getSummaryItems().get(1).getText();
         }else if(b==3){
             t= sU.getSummaryItems().get(2).getText();
         }else if(b==4){
              AnalysisSummarySubItem sumS = sU.getSummaryItems().get(2).getSubItems().get(0);
             t= sumS.getText();
         }else if(b==5){
             t= sU.getSummaryItems().get(3).getText();
         }else if(b==6){
             t= sU.getSummaryItems().get(3).getGraph().getName();
         }else if(b==7){
             t= sU.getSummaryItems().get(3).getSubItems().get(0).getText();
         }else if(b==8){
             t= sU.getSummaryItems().get(4).getText();
         }
    } catch (Exception e) {
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return t;
}
public List<Integer> BTextNL(String a,int b){
   List<Integer> t=new ArrayList<>();
    try {
        TextAnalysisRequest re = createRequest(a,"allsentences");
         AsyncResponseTextAnalysisResponse res = apiInstance.post(re);
         TextAnalysisResponse resp = res.getResult();
         AnalysisSummary sU=resp.getSummaries().get(0);
         if(b==10){
         for(int i=0;i<sU.getSummaryItems().get(3).getGraph().getItems().size();i++){
             t.add(sU.getSummaryItems().get(3).getGraph().getItems().get(i).getLength());
         }}else if(b==12){
         for(int i=0;i<sU.getSummaryItems().get(4).getGraph().getItems().size();i++){
             t.add(sU.getSummaryItems().get(4).getGraph().getItems().get(i).getLength());
         }}
         } catch (Exception e) {
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return t;
}
public List<String> BTextS(String a){
   List<String> t= new ArrayList<>();
    try {
        TextAnalysisRequest re = createRequest(a,"allsentences");
         AsyncResponseTextAnalysisResponse res = apiInstance.post(re);
         TextAnalysisResponse resp = res.getResult();
         AnalysisSummary sU=resp.getSummaries().get(0);
         
         for(int i=0;i<sU.getSummaryItems().get(3).getGraph().getItems().size();i++){
             t.add(sU.getSummaryItems().get(3).getGraph().getItems().get(i).getLabel());
         }
         } catch (Exception e) {
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return t;
}
public String textComplexWord(String a){
    String a1 ="";
    try{
    HtmlAnalysisRequest re = createRequest1(a,"complex");
    AsyncResponseHtmlAnalysisResponse res = apiInstance1.post(re);
    HtmlAnalysisResponse resp = res.getResult();
    a1 = resp.getHtml();
    }catch(ApiException e){
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return a1;
}
    public List<String> ComplexWordTextError(String a){
        List<String> a2 = new ArrayList<>();
        try{
    HtmlAnalysisRequest re = createRequest1(a,"complex");
    AsyncResponseHtmlAnalysisResponse res = apiInstance1.post(re);
    HtmlAnalysisResponse resp = res.getResult();
    for(int i=0;i<resp.getSummaries().get(0).getSummaryItems().size();i++){
        a2.add(resp.getSummaries().get(0).getSummaryItems().get(i).getText());
    }
    }catch(ApiException e){
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return a2;
    }
    public String textConsistency(String a){
    String a1 ="";
    try{
    HtmlAnalysisRequest re = createRequest1(a,"consistency");
    AsyncResponseHtmlAnalysisResponse res = apiInstance1.post(re);
    HtmlAnalysisResponse resp = res.getResult();
    a1 = resp.getHtml();
    }catch(ApiException e){
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return a1;
}
    public List<String> ConsistencyWordTextError(String a, int a5) {
    List<String> a2 = new ArrayList<>();
    try {
        HtmlAnalysisRequest re = createRequest1(a, "consistency");
        AsyncResponseHtmlAnalysisResponse res = apiInstance1.post(re);
        HtmlAnalysisResponse resp = res.getResult();
        
        if (a5 == 10) {
            for (int i = 0; i < resp.getSummaries().get(0).getSummaryItems().size(); i++) {
                a2.add(resp.getSummaries().get(0).getSummaryItems().get(i).getText());
            }
        } else {
            List<AnalysisSummarySubItem> subItems = resp.getSummaries().get(0).getSummaryItems().get(a5).getSubItems();
            if (subItems != null && !subItems.isEmpty()) {
                for (int i = 0; i < subItems.size(); i++) {
                    a2.add(subItems.get(i).getText());
                }
            }
        }
    } catch (ApiException e) {
        System.err.println("Exception when calling the API");
        e.printStackTrace();
    }
    return a2;
}
public String textDVA(String a,int a3){
    String a1 ="";
    try{
    HtmlAnalysisRequest re = createRequest1(a,"dva");
    AsyncResponseHtmlAnalysisResponse res = apiInstance1.post(re);
    HtmlAnalysisResponse resp = res.getResult();
    if(a3==1){
    a1 = resp.getHtml();}else if(a3==2){
        a1=resp.getSummaries().get(0).getSummaryItems().get(0).getText();
    }else{
        a1=resp.getSummaries().get(0).getSummaryItems().get(1).getText();
    }
    }catch(ApiException e){
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return a1;
}
public List<String> DVATextError(String a,int a3){
        List<String> a2 = new ArrayList<>();
        try{
    HtmlAnalysisRequest re = createRequest1(a,"dva");
    AsyncResponseHtmlAnalysisResponse res = apiInstance1.post(re);
    HtmlAnalysisResponse resp = res.getResult();
    if(a3 == 1){
    for(int i=0;i<resp.getSummaries().get(0).getSummaryItems().get(0).getSubItems().size();i++){
        a2.add(resp.getSummaries().get(0).getSummaryItems().get(0).getSubItems().get(i).getText());
    }}else{
        for(int i=0;i<resp.getSummaries().get(0).getSummaryItems().get(1).getSubItems().size();i++){
        a2.add(resp.getSummaries().get(0).getSummaryItems().get(1).getSubItems().get(i).getText());
    }
    }
    }catch(ApiException e){
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return a2;
    }
public String textReadabality(String a,int a3){
    String a1 ="";
    try{
    HtmlAnalysisRequest re = createRequest1(a,"preadability");
    AsyncResponseHtmlAnalysisResponse res = apiInstance1.post(re);
    HtmlAnalysisResponse resp = res.getResult();
    if(a3==1){
    a1 = resp.getHtml().replaceAll("<span", "<br/><br/><span");}else if(a3==2){
        a1=resp.getSummaries().get(0).getSummaryItems().get(0).getText();
    }else if(a3==3){
        a1=resp.getSummaries().get(0).getSummaryItems().get(1).getText();
    }else if(a3==4){
        a1=resp.getSummaries().get(0).getSummaryItems().get(2).getText();
    }else{
        a1=resp.getSummaries().get(0).getSummaryItems().get(3).getText();
    }
    }catch(ApiException e){
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return a1;
}
    public List<String> ReadabilityRe(String a, int a5) {
    List<String> a2 = new ArrayList<>();
    try {
        HtmlAnalysisRequest re = createRequest1(a, "preadability");
        AsyncResponseHtmlAnalysisResponse res = apiInstance1.post(re);
        HtmlAnalysisResponse resp = res.getResult();
        
        
            List<AnalysisSummarySubItem> subItems = resp.getSummaries().get(0).getSummaryItems().get(a5).getSubItems();
            if (subItems != null && !subItems.isEmpty()) {
                for (int i = 0; i < subItems.size(); i++) {
                    a2.add(subItems.get(i).getText());
                }
            
        }
    } catch (ApiException e) {
        System.err.println("Exception when calling the API");
        e.printStackTrace();
    }
    return a2;
}
    public String textTransition(String a,int a3){
    String a1 ="";
    try{
    HtmlAnalysisRequest re = createRequest1(a,"transition");
    AsyncResponseHtmlAnalysisResponse res = apiInstance1.post(re);
    HtmlAnalysisResponse resp = res.getResult();
    if(a3==1){
    a1 = resp.getHtml();}else if(a3==2){
        if(resp.getSummaries().get(0).getSummaryItems().get(0).getText()!=null){
        a1=resp.getSummaries().get(0).getSummaryItems().get(0).getText();}
    }else if(a3==3){
        if(resp.getSummaries().get(0).getSummaryItems().size()>=2){
        a1=resp.getSummaries().get(0).getSummaryItems().get(1).getText();}
    }else if(a3==4){
        if(resp.getSummaries().get(0).getSummaryItems().size()>=3){
        a1=resp.getSummaries().get(0).getSummaryItems().get(2).getText();}
    }else{
        if(resp.getSummaries().get(0).getSummaryItems().size()>=3){
        a1=resp.getSummaries().get(0).getSummaryItems().get(3).getText();}
    }
    }catch(ApiException e){
        System.err.println("Exception when calling the API");
            e.printStackTrace();
    }
    return a1;
}
    public List<String> TransitionsTextEro(String a, int a5) {
    List<String> a2 = new ArrayList<>();
    try {
        HtmlAnalysisRequest re = createRequest1(a, "transition");
        AsyncResponseHtmlAnalysisResponse res = apiInstance1.post(re);
        HtmlAnalysisResponse resp = res.getResult();
        
        
            List<AnalysisSummarySubItem> subItems = resp.getSummaries().get(0).getSummaryItems().get(a5).getSubItems();
            if (subItems != null && !subItems.isEmpty()) {
                for (int i = 0; i < subItems.size(); i++) {
                    a2.add(subItems.get(i).getText());
                }
            
        }
    } catch (ApiException e) {
        System.err.println("Exception when calling the API");
        e.printStackTrace();
    }
    return a2;
}
}
