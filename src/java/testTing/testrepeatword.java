/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package testTing;

import ApiAnswer.Apitaking;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author admin
 */
public class testrepeatword extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet testrepeatword</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet testrepeatword at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String a = (String)request.getParameter("text1");
        Apitaking a1 =new Apitaking();
        List<String> errorsG = a1.getRepeatWord(a, "1");
        List<String> GrammarS = a1.analyzeText(a,"1");
        List<String> errorsG2 = a1.getRepeatWord(a, "2");
        List<String> GrammarS2 = a1.analyzeText(a,"2");
        List<String> errorsG3 = a1.getRepeatWord(a, "3");
        List<String> GrammarS3 = a1.analyzeText(a,"3");
        List<String> errorsG4 = a1.getRepeatWord(a, "4");
        List<String> GrammarS4 = a1.analyzeText(a,"4");
        List<String> Allerr = a1.analyzeRepeat(a, "1");
                List<String> Allerr2 = a1.analyzeRepeat(a, "2");
        List<String> Allerr3 = a1.analyzeRepeat(a, "3");
                List<String> Allerr4 = a1.analyzeRepeat(a, "4");


        request.setAttribute("text", a);
        request.setAttribute("suggestion", GrammarS);
        request.setAttribute("errorG", errorsG);
        request.setAttribute("suggestion2", GrammarS2);
        request.setAttribute("error2", errorsG2);
        request.setAttribute("suggestion3", GrammarS3);
        request.setAttribute("error3", errorsG3);
        request.setAttribute("suggestion4", GrammarS4);
        request.setAttribute("error4", errorsG4);
        request.setAttribute("errorAll", Allerr);
        request.setAttribute("errorAll2", Allerr2);
        request.setAttribute("errorAll3", Allerr3);
        request.setAttribute("errorAll4", Allerr4);
        request.getRequestDispatcher("Inputtext2.jsp").forward(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
public static void main(String[] args) {
    String a = "On the cold, wet morning, my class was filled with excitement. Someone  have discover that the next day was our teacher's birthday. Our teacher was the kindest person that ever  exist. Thus it  is no surprise she was the favourite teacher  to the pupils. Everyone  want to get her a present. I, very much wanted to  shown any appreciation too. That afternoon, I  spends the whole afternoon  shop for a present. After a long search, I finally made  on my mind. The next day I  gived her a bouquet of beautiful roses and she exclaimed with pleasure.";
        Apitaking a1 =new Apitaking();
        List<String> errorsG = a1.getRepeatWord(a, "1");
        List<String> GrammarS = a1.analyzeText(a,"1");
        System.out.println(errorsG);
        System.out.println(GrammarS);
    }
}
