package testTing;

import ApiAnswer.Apitaking;
import ProWritingAid.*;
import ProWritingAid.auth.*;
import ProWritingAid.SDK.*;
import java.util.AbstractList;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
public class Grammatest {
    
    
    
     public List<String> getGrammarErrorsAndSuggestions(String text) {
    List<String> errorsAndSuggestions = new ArrayList<>();

    ApiClient defaultClient = Configuration.getDefaultApiClient();

    // Configure API key authorization: licenseCode
    ApiKeyAuth licenseCode = (ApiKeyAuth) defaultClient.getAuthentication("licenseCode");
    licenseCode.setApiKey("1FBEFF86-325D-42A0-8487-EEC552BBF866");

    TextApi apiInstance = new TextApi();
    try {
        TextAnalysisRequest request = new TextAnalysisRequest();
        request.setText(text);
        request.setReports(Arrays.asList("overview"));
        request.setStyle(TextAnalysisRequest.StyleEnum.GENERAL);
        request.setLanguage(TextAnalysisRequest.LanguageEnum.EN);

        AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
        TextAnalysisResponse response = result.getResult();
        for (DocTag tag : response.getTags()) {
            if(tag.getCategory().equals("grammargrammar")){
               if (tag.getReport().equals("grammar")) { // change this line
                errorsAndSuggestions.add("Error: " + tag.getHint());
                errorsAndSuggestions.add("Suggestions: " + String.join(", ", tag.getSuggestions()));
                errorsAndSuggestions.add(""); // add an empty string to separate errors
            }
            }else{System.out.println("//");}
        }
    } catch (ApiException e) {
        System.err.println("Exception when calling the API");
        e.printStackTrace();
    }

    return errorsAndSuggestions;
}

   public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        List<String> a =new ArrayList<String>();
        
        // Configure API key authorization: licenseCode
        ApiKeyAuth licenseCode = (ApiKeyAuth) defaultClient.getAuthentication("licenseCode");
        licenseCode.setApiKey("1FBEFF86-325D-42A0-8487-EEC552BBF866");

        TextApi apiInstance = new TextApi();
        try {
    TextAnalysisRequest request = new TextAnalysisRequest();
    request.setText("This notorious and infamous event put Americas hope to the test, leaving Americans very economically and mentally drained. The Great Depression devastated America and is unlikly to be forgotten. If not for Franklen Delano Roosevelt and his beneficial “New Deal’; who knows what horrid ruins would remain as a result of The Great Depression?\n" +
"\n" +
"During The Great Depression people didn’t have much money because it was not easy to earn, and with the lack of jobs, people ended up working many different part-time jobs to make ends meet so it was difficult to carry on without any hope of economic improvement.\n" +
"\n" +
"Every American’s spirit were being tested alot and the number of those who still had hope was constantly diminishing, with America facing economic doom Americans turned to one man: Franklin Delano Roosevelt. The “New Deal” was created by Roosevelt, and this new policy was supported by most Americans.\n" +
"\n" +
"With the promise of a New Deal to help end the Great Depression Roosevelt handidly won the election by a landslide. He created jobs for three million people between seventeen and twenty three years of age. Roosevelt’s work relief program put eight and a half million Americans to work building roads, bridges airports and more.\n" +
"\n" +
"Although Roosevelt did not end the Great Depression, he provide Americans with work and hope. The Great Depression left Americans mentally as well as economically depleted. This event tested the will of the american people and left some citizens without any hope but through the leadership of Franklin Delano Roosevelt many Americans was inspired to make positive changes in their lives and many people made the decision to retrain for a new career, learn a new skill, and take more control over their own lives.\n" +
"\n" +
"Even though the Depression devastatingly affected tens of millions of people, the way it changed people’s outlooks was very inspiring. People learned to be more resourceful. Instead of tossing a cotton bag in the garbage, people started to use them as towels and dish cloths. A stronger sense of pride in their country helped them work through the hardships together, with patriotism and unity. Together, the country was able to feel more hopful as things began to improve.\n" +
"\n" +
"Above all else, without hope for the future, people would of given up on trying to fix their severely wounded economy. These enhanced senses of resourcefulness, unity, along with patriotism and hope were all ways that the Great Depression affected Americans.");
    request.setReports(Arrays.asList("allsentences"));
    request.setStyle(TextAnalysisRequest.StyleEnum.GENERAL);
    request.setLanguage(TextAnalysisRequest.LanguageEnum.EN);

    AsyncResponseTextAnalysisResponse result = apiInstance.post(request);
    TextAnalysisResponse response = result.getResult();
            AnalysisSummary sU = response.getSummaries().get(0);
            System.out.println(sU.getSummaryItems().get(2).getSubItems().get(0));
            Apitaking e4 = new Apitaking();
            System.out.println(e4.BTextS("This notorious and infamous event put Americas hope to the test, leaving Americans very economically and mentally drained. The Great Depression devastated America and is unlikly to be forgotten. If not for Franklen Delano Roosevelt and his beneficial “New Deal’; who knows what horrid ruins would remain as a result of The Great Depression?\n" +
"\n" +
"During The Great Depression people didn’t have much money because it was not easy to earn, and with the lack of jobs, people ended up working many different part-time jobs to make ends meet so it was difficult to carry on without any hope of economic improvement.\n" +
"\n" +
"Every American’s spirit were being tested alot and the number of those who still had hope was constantly diminishing, with America facing economic doom Americans turned to one man: Franklin Delano Roosevelt. The “New Deal” was created by Roosevelt, and this new policy was supported by most Americans.\n" +
"\n" +
"With the promise of a New Deal to help end the Great Depression Roosevelt handidly won the election by a landslide. He created jobs for three million people between seventeen and twenty three years of age. Roosevelt’s work relief program put eight and a half million Americans to work building roads, bridges airports and more.\n" +
"\n" +
"Although Roosevelt did not end the Great Depression, he provide Americans with work and hope. The Great Depression left Americans mentally as well as economically depleted. This event tested the will of the american people and left some citizens without any hope but through the leadership of Franklin Delano Roosevelt many Americans was inspired to make positive changes in their lives and many people made the decision to retrain for a new career, learn a new skill, and take more control over their own lives.\n" +
"\n" +
"Even though the Depression devastatingly affected tens of millions of people, the way it changed people’s outlooks was very inspiring. People learned to be more resourceful. Instead of tossing a cotton bag in the garbage, people started to use them as towels and dish cloths. A stronger sense of pride in their country helped them work through the hardships together, with patriotism and unity. Together, the country was able to feel more hopful as things began to improve.\n" +
"\n" +
"Above all else, without hope for the future, people would of given up on trying to fix their severely wounded economy. These enhanced senses of resourcefulness, unity, along with patriotism and hope were all ways that the Great Depression affected Americans."));
        } catch (ApiException e) {
        System.err.println("Exception when calling the API");
        e.printStackTrace();
    }
   }}