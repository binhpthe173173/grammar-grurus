package ControllerAdmin;

import dal.DAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Account;

/**
 * Servlet implementation class UpdateAccount
 */
@WebServlet(name = "UpdateAccount", urlPatterns = {"/updateAccount"})
public class UpdateAccount extends HttpServlet {

    DAO userDao;
    String err = "";

    public void init() {
        userDao = new DAO();
        userDao.LoadAccount(); // Ensure accounts are loaded
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("id", id);
        
        Account account = userDao.getAccountById(id);
        request.setAttribute("account", account);
        request.getRequestDispatcher("../updateAccount.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String username = request.getParameter("username");
       // String password = request.getParameter("password");
        String full_name = request.getParameter("full_name");
        String email = request.getParameter("email");
        String image = request.getParameter("image");
        boolean active_status = Boolean.parseBoolean(request.getParameter("active_status"));
        int role_id = Integer.parseInt(request.getParameter("role_id"));
        if(!checkUpdate(id, username, full_name, email)){
            request.setAttribute("error", err);
            request.setAttribute("id", id);
            request.getRequestDispatcher("../updateAccount.jsp").forward(request, response);
            return;
        }
        userDao.updateAccount(id, username, full_name, email, image, active_status, role_id);

        response.sendRedirect(request.getContextPath() + "/Admin/listUser");
    }

    public boolean checkUpdate(int id,String username, String full_name, String email) {
        
        if (!userDao.checkEmail(email)) {
            err = "Pls input invalid email";
            return false;

        }
        if (userDao.checkDuplicateEmailForOtherAccount(email, id)) {
            err = "Email is already taken by another account";
            return false;
        }
        return true;
    }
}
