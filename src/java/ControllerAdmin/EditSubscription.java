/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package ControllerAdmin;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.*;

/**
 *
 * @author Noisy
 */
public class EditSubscription extends HttpServlet {
    DAO dao;
    @Override
    public void init(){
        dao = new DAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Subscription sub = dao.getSubscriptionById(Integer.parseInt(req.getParameter("id")));
        req.setAttribute("sub", sub);
        req.getRequestDispatcher("../editSubscription.jsp").forward(req, resp);
    }
    
    @Override
     protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        int price = Integer.parseInt(request.getParameter("price"));
        int duration = Integer.parseInt(request.getParameter("duration"));
        boolean status = Integer.parseInt(request.getParameter("status"))==1;
        String description = request.getParameter("description");
        dao.updateSubscription(id, name, price, duration, status, description);
        response.sendRedirect(request.getContextPath() + "/Admin/manageSubscription");
    }
}
