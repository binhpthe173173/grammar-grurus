package ControllerAdmin;

import dal.DAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Account;

public class AddAccount extends HttpServlet {

    DAO userDao;
    String err = "";

    @Override
    public void init() {
        userDao = new DAO();
        userDao.LoadAccount(); // Ensure accounts are loaded
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("../addAccount.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String confirm_Password = request.getParameter("confirm_password");
        String full_name = request.getParameter("full_name");
        String email = request.getParameter("email");
        String image = request.getParameter("image");
        boolean active_status = Boolean.parseBoolean(request.getParameter("active_status"));
        int role_id = Integer.parseInt(request.getParameter("role_id"));

        if (!checkRegister(username, full_name, password, confirm_Password, email)) {
            request.setAttribute("err", err);
            request.getRequestDispatcher("../addAccount.jsp").forward(request, response);
            return;
        }

        userDao.addAccount(username, password, full_name, email, image, active_status, role_id);
        response.sendRedirect(request.getContextPath() + "/Admin/listUser");
    }

    public boolean checkRegister(String username, String full_name, String password, String password_confirm, String email) {
        if (!userDao.checkUsername(username)) {
            err = "Invalid Username";
            return false;
        }
        if (!userDao.checkPassword(password)) {
            err = "Password must contain uppercase, lowercase and at least a number";
            return false;
        }
        if (!password.equals(password_confirm)) {
            err = "Confirm password does not match";
            return false;
        }
        if (!userDao.checkEmail(email)) {
            err = "Please input a valid email";
            return false;
        }
        if (userDao.findAccount(username) != null) {
            err = "Account already exists";
            return false;
        }
        if (userDao.findAccountbyEmail(email) != null) {
            err = "Email already used";
            return false;
        }
        return true;
    }
}