/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package ControllerAdmin;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import models.Account;

/**
 *
 * @author Admin
 */
public class SearchByAjax extends HttpServlet {

    DAO accDao;

    public void init() {
        accDao = new DAO();

    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String textSearch = request.getParameter("text");

        String activeStatusParam = request.getParameter("activeStatus");
        String roleIdParam = request.getParameter("roleId");
        Boolean activeStatus = null;
        if (!"all".equals(activeStatusParam)) {
            activeStatus = Boolean.parseBoolean(activeStatusParam);
        }
        Integer roleId = null;
        if (!"all".equals(roleIdParam)) {
            roleId = Integer.parseInt(roleIdParam);
        }
        List<Account> listSearch = accDao.searchByFilters(textSearch, activeStatus, roleId);
        //List<Account> listSearch = accDao.searchByName(textSearch);

        try (PrintWriter out = response.getWriter()) {
            out.println("<table class=\"table table-bordered\">");
            out.println("<thead class=\"thead-dark\">");
            out.println("<tr>");
            out.println("<th>ID</th>");
            out.println("<th>Username</th>");
            out.println("<th>Full Name</th>");
            out.println("<th>Email</th>");
            out.println("<th>Image</th>");
            out.println("<th>Time Created</th>");
            out.println("<th>Active Status</th>");
            out.println("<th>Role ID</th>");
            out.println("<th>Actions</th>");
            out.println("</tr>");
            out.println("</thead>");
            out.println("<tbody>");
            if (listSearch == null || listSearch.isEmpty()) {
                String statusMessage = "all".equals(activeStatusParam) ? "all statuses" : (activeStatus ? "active" : "inactive");
                String roleMessage = "all".equals(roleIdParam) ? "all roles" : (roleId == 1 ? "user" : "admin");
                out.println("<tr>");
                out.println("<td colspan=\"10\" style=\"text-align:center;\">No accounts found with username containing: " + textSearch + ", status: " + statusMessage + " and role: " + roleMessage + "</td>");
                out.println("</tr>");

            } else {
                for (Account account : listSearch) {
                    out.println("<tr>");
                    out.println("<td>" + account.getId() + "</td>");
                    out.println("<td>" + account.getUsername() + "</td>");
                    out.println("<td>" + account.getFull_name() + "</td>");
                    out.println("<td>" + account.getEmail() + "</td>");
                    out.println("<td><img src=\"" + request.getContextPath() + "/" + account.getImage() + "\" alt=\"User Image\" width=\"50\" height=\"50\"/></td>");
                    out.println("<td>" + account.getTime_created() + "</td>");
                    out.println("<td>" + (account.isActive_status() ? "Active" : "Inactive") + "</td>");
                    out.println("<td>" + (account.getRole_id() == 1 ? "User" : "Admin") + "</td>");

                    out.println("<td>");
                    out.println("<a class=\"btn btn-sm btn-primary\" href=\"updateAccount?id=" + account.getId() + "\">Edit</a>");
                    out.println("<a class=\"btn btn-sm btn-danger\" href=\"deleteAccount?id=" + account.getId() + "\" onclick=\"return confirm('Are you sure you want to delete this account?');\">Delete</a>");
                    out.println("</td>");
                    out.println("</tr>");
                }
            }
            out.println("</tbody>");
            out.println("</table>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
