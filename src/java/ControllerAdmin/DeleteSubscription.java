/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package ControllerAdmin;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import models.AccountSubscription;

/**
 *
 * @author Noisy
 */
public class DeleteSubscription extends HttpServlet {

    DAO dao;

    @Override
    public void init() {
        dao = new DAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        List<AccountSubscription> list = dao.getAccountSubscriptionsBySubscriptionId(id);
        if (list.isEmpty()) {
            dao.deleteSubscription(id);
        } else {
            for(AccountSubscription as: list) {
                dao.addFundsToWallet(as.getAccountId(), dao.getSubscriptionById(as.getSubscriptionId()).getPrice());
            }
            dao.deleteAccountSubscription(id);
            dao.deleteSubscription(id);
        }
        response.sendRedirect(request.getContextPath() + "/Admin/manageSubscription");
    }
}
