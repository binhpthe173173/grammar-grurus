package ControllerAdmin;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import dal.DAO;
import java.util.*;
import models.*;


public class ManageWallet extends HttpServlet {

    DAO dao;

    @Override
    public void init() {
        dao = new DAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dao.LoadWallet();
        dao.LoadAccount();
        String q = request.getParameter("q")!= null ? request.getParameter("q") : "";
        List<Wallet> wallets = dao.getWallets();
        Map<Integer, Account> accountMap = new HashMap<>();
        for(Wallet wallet: wallets) {
            Account account = dao.findAccountbyId(wallet.getAccount_id());
            if (account.getFull_name().toLowerCase().contains(q.toLowerCase())
                    || account.getUsername().toLowerCase().contains(q.toLowerCase())
                    || String.valueOf(account.getId()).contains(q))
                accountMap.put(wallet.getId(), account);
        }
        request.setAttribute("wallets", wallets);
        request.setAttribute("accountMap", accountMap);
        request.getRequestDispatcher("../manageWallet.jsp").forward(request, response);
    }
}
