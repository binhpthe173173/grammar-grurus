/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package ControllerAdmin;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Admin
 */
public class DashboardData extends HttpServlet {
   DAO userDao;

    public void init() {
        userDao = new DAO();
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DashboardData</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DashboardData at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        try{
            int role_Id =0;
        
        int numberOfUsers = userDao.getNumberOfAccounts(role_Id);
        int totalTransactions = userDao.fetchTotalTransactions();
        int totalAmount = userDao.fetchTotalAmount();

        // Tạo một đối tượng JSON để chứa dữ liệu
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("numberOfUsers", numberOfUsers);
        jsonResponse.put("totalTransactions", totalTransactions);
        jsonResponse.put("totalAmount", totalAmount);

        // Thiết lập kiểu MIME và ký tự cho response
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        // Gửi dữ liệu JSON về phía client
        PrintWriter out = response.getWriter();
        out.print(jsonResponse.toString());
        out.flush();
   } catch(Exception e){
       
   }
    } 

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
