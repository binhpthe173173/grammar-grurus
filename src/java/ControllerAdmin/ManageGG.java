/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package ControllerAdmin;

import dal.*;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Random;
import models.*;

/**
 *
 * @author admin
 */
public class ManageGG extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        TopictypeDAO adao = new TopictypeDAO();
        String topic = request.getParameter("topic");
        String id = creatRandomId();
        while (adao.getTopicTypeById(id) != null) {
            id = creatRandomId();

        }
        Topictype t = new Topictype(id, "g", topic);
        adao.insertTopictype(t);
        response.sendRedirect("grammarGuide.jsp");
    }

    public String creatRandomId() {
        Random random = new Random();
        StringBuilder idBuilder = new StringBuilder();
        String LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i = 0; i < 4; i++) {
            char randomChar = LETTERS.charAt(random.nextInt(LETTERS.length()));
            idBuilder.append(randomChar);
        }
        for (int i = 0; i < 4; i++) {
            int randomDigit = random.nextInt(10);
            idBuilder.append(randomDigit);
        }

        return idBuilder.toString();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        QandaDAO adao = new QandaDAO();
        String idContent = creatRandomId();
        while (adao.getQandatopicByIdContent(idContent) != null) {
            idContent = creatRandomId();
        }
        String question = request.getParameter("question");
        String content = request.getParameter("content");
        String idTopic = request.getParameter("idTopic");
        Qandatopic a = new Qandatopic(idContent, question, content, idTopic);
        adao.insertQandatopic(a);
        List<Qandatopic> grammarList = adao.getQandatopicsByTopicId("101");
        request.setAttribute("grammarList", grammarList);
        List<Qandatopic> punctuationList = adao.getQandatopicsByTopicId("SYLY1438");
        request.setAttribute("punctuationList", punctuationList);
        List<Qandatopic> mechanicsList = adao.getQandatopicsByTopicId("102");
        request.setAttribute("mechanicsList", mechanicsList);
        request.getRequestDispatcher("grammarGuide.jsp").forward(request, response);
        return;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
