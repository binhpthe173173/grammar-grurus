package ControllerAdmin;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Collections;
import models.Transaction;

public class TransactionHistory extends HttpServlet {

    DAO dao;
    
    @Override
    public void init() {
        dao = new DAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dao.LoadTransaction();
        Collections.reverse(dao.getTransactions());
        request.setAttribute("dao", dao);
        request.getRequestDispatcher("../transactionHistory.jsp").forward(request, response);
    }
}
