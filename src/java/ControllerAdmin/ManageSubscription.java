package ControllerAdmin;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import models.Subscription;

public class ManageSubscription extends HttpServlet {
    DAO dao;
    @Override
    public void init() {
        dao = new DAO();
    }
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dao.LoadSubscription();
        request.setAttribute("dao", dao);
        request.getRequestDispatcher("../manageSubscription.jsp").forward(request, response);
    }
}
