/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package ControllerAdmin;

import controllers.*;
import dal.*;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import models.Account;

/**
 *
 * @author admin
 */
public class DeleteBlog extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DeleteBlog</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DeleteBlog at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    //SendFeedBack
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession ses = request.getSession();
         Account x = (Account) ses.getAttribute("us");
        PrintWriter out = response.getWriter();
        String idBlog = request.getParameter("idBlog").trim();
        BlogDAO dao1 = new BlogDAO();
        int idacc=dao1.getBlogById(idBlog).getIdacount();
        DAO acc = new DAO();
       /* out.println(idBlog);
        out.print(idacc);*/
        String mail =acc.getAccountById(idacc).getEmail();
       String name =acc.getAccountById(idacc).getUsername();
        autoSendEmail auto = new autoSendEmail();
        dao1.deleteBlogById(idBlog);
       
        
        
        String noidung ="<!DOCTYPE html>\n" +
"<html lang=\"en\">\n" +
"<head>\n" +
"    <meta charset=\"UTF-8\">\n" +
"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
"    <title>Your Blog Post with ID:"+  idBlog + "  Has Been Removed by the Administrator</title>\n" +
"    <style>\n" +
"        body {\n" +
"            font-family: Arial, sans-serif;\n" +
"            background-color: #f4f4f4;\n" +
"            margin: 0;\n" +
"            padding: 0;\n" +
"            display: flex;\n" +
"            justify-content: center;\n" +
"            align-items: center;\n" +
"            height: 100vh;\n" +
"        }\n" +
"        .notification-box {\n" +
"            background-color: #ffffff;\n" +
"            padding: 20px 40px;\n" +
"            border: 1px solid #007A7A;\n" +
"            border-radius: 10px;\n" +
"            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);\n" +
"            text-align: center;\n" +
"        }\n" +
"        .notification-box h1 {\n" +
"            color: #007A7A;\n" +
"        }\n" +
"        .notification-box p {\n" +
"            color: #666;\n" +
"            font-size: 1.1em;\n" +
"        }\n" +
"        .notification-box .id-highlight {\n" +
"            color: #ff4c4c;\n" +
"            font-weight: bold;\n" +
"        }\n" +
"    </style>\n" +
"</head>\n" +
"<body>\n" +
"    <div class=\"notification-box\">\n" +
                "        <h1>Dear "+name+"</h1>\n" +
"        <h1>Your Blog Post Has Been Removed</h1>\n" +
"        <p>We regret to inform you that your blog post with ID <span class=\"id-highlight\">"+idBlog+"</span> has been removed by the administrator.</p>\n" +
"        <p>The removal was due to a violation of our community guidelines.</p>\n" +
"        <p>If you have any questions or believe this was a mistake, please contact our support team.</p>\n" +
"    </div>\n" +
"</body>\n" +
"</html>";
        
        

        if(x.getRole_id()==0){ 
        auto.sendEmail(mail, noidung, "GrammarGurus system notifies");}

        response.sendRedirect("includeBlog.jsp");
        
        
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
