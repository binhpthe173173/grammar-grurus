/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package ControllerAdmin;

import dal.*;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Random;
import models.*;

/**
 *
 * @author admin
 */
public class ManageFAQ extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        TopictypeDAO adao = new TopictypeDAO();
        String topic = request.getParameter("topic");
        String id = creatRandomId();
        while (adao.getTopicTypeById(id) != null) {
            id = creatRandomId();

        }
        Topictype t = new Topictype(id, "f", topic);
        adao.insertTopictype(t);
        response.sendRedirect("manageFAQ.jsp");
    }

    public String creatRandomId() {
        Random random = new Random();
        StringBuilder idBuilder = new StringBuilder();
        String LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i = 0; i < 4; i++) {
            char randomChar = LETTERS.charAt(random.nextInt(LETTERS.length()));
            idBuilder.append(randomChar);
        }
        for (int i = 0; i < 4; i++) {
            int randomDigit = random.nextInt(10);
            idBuilder.append(randomDigit);
        }

        return idBuilder.toString();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        QandaDAO adao = new QandaDAO();
        String idContent = creatRandomId();
        while (adao.getQandatopicByIdContent(idContent) != null) {
            idContent = creatRandomId();
        }
        String question = request.getParameter("question");
        String content = request.getParameter("content");
        String idTopic = request.getParameter("idTopic");
        Qandatopic a = new Qandatopic(idContent, question, content, idTopic);
        adao.insertQandatopic(a);
        response.sendRedirect("manageFAQ.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
