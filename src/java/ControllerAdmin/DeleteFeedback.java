/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package ControllerAdmin;
import dal.*;
import models.*;
import controllers.autoSendEmail;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author admin
 */
public class DeleteFeedback extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DeleteFeedback</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DeleteFeedback at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       String id = request.getParameter("id");
       String ms="A";
       request.setAttribute("id", id);
        request.setAttribute("ms", ms);
       
       request.getRequestDispatcher("deleteFeedback.jsp").forward(request, response);
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String sid = request.getParameter("id");
       int id = Integer.parseInt(sid);
        FeedDAO fdao = new FeedDAO();
     int idacc =  fdao.getFeedbackById(id).getAccount_id();
      DAO accdao = new DAO();
     String  email = accdao.getAccountById(idacc).getEmail();
     String name = accdao.getAccountById(idacc).getFull_name();
       noidung(name);
       fdao.deleteFeedbackById(id);
       autoSendEmail auto = new autoSendEmail();
       auto.sendEmail(email,noidung(name), "Notification from GRAMMAR GURUS system ");
       response.sendRedirect("review.jsp");
   /* PrintWriter out = response.getWriter();
      out.print(name);*/
    }

 public String noidung ( String name){
     String nd="<!DOCTYPE html>\n" +
"<html lang=\"en\">\n" +
"<head>\n" +
"    <meta charset=\"UTF-8\">\n" +
"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
"    <title>Feedback Deletion Notification</title>\n" +
"    <style>\n" +
"        .notification-container {\n" +
"            display: flex;\n" +
"            flex-direction: column;\n" +
"            align-items: center;\n" +
"            justify-content: center;\n" +
"            height: 100vh;\n" +
"            background-color: #f8f9fa;\n" +
"            font-family: Arial, sans-serif;\n" +
"        }\n" +
"        .notification {\n" +
"            background-color: #ffffff;\n" +
"            padding: 2rem;\n" +
"            border-radius: 10px;\n" +
"            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);\n" +
"            text-align: center;\n" +
"            max-width: 500px;\n" +
"            width: 100%;\n" +
"        }\n" +
"        h2 {\n" +
"            color: #dc3545;\n" +
"        }\n" +
"        p {\n" +
"            color: #6c757d;\n" +
"        }\n" +
"        .reason {\n" +
"            font-weight: bold;\n" +
"            color: #343a40;\n" +
"        }\n" +
"        .footer {\n" +
"            margin-top: 1.5rem;\n" +
"            color: #6c757d;\n" +
"            font-size: 0.875rem;\n" +
"        }\n" +
"    </style>\n" +
"</head>\n" +
"<body>\n" +
"    <div class=\"notification-container\">\n" +
"        <div class=\"notification\">\n" +
"            <h2>Feedback Deletion Notification</h2>\n" +
"            <p>Dear "+name+",</p>\n" +
"            <p>Your feedback has been removed by our system for violating our community standards.</p>\n" +
            
"            <p>Thank you for your understanding.</p>\n" +
"            <p class=\"footer\">If you have any questions, please contact our support team.</p>\n" +
"        </div>\n" +
"    </div>\n" +
"</body>\n" +
"</html>";
 
 return nd;
 }
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
         
}
