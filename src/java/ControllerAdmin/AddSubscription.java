/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package ControllerAdmin;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Subscription;

/**
 *
 * @author Noisy
 */
public class AddSubscription extends HttpServlet {
    DAO dao;
    @Override
    public void init() {
        dao = new DAO();
    }
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("../addSubscription.jsp").forward(req, resp);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        int price = Integer.parseInt(request.getParameter("price"));
        int duration = Integer.parseInt(request.getParameter("duration"));
        boolean status = Integer.parseInt(request.getParameter("status"))==1;
        String description = request.getParameter("description");
        dao.addSubscription(name, price, duration, status, description);
        response.sendRedirect(request.getContextPath() + "/Admin/manageSubscription");
    }
}
