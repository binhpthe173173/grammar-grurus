package ControllerAdmin;

import dal.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import models.Wallet;

public class AddFunds extends HttpServlet {

    DAO dao;

    @Override
    public void init() {
        dao = new DAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Wallet wallet = dao.getWalletByAccountId(Integer.parseInt(req.getParameter("id")));
        if (wallet != null) {
            req.setAttribute("wallet", wallet);
            req.getRequestDispatcher("../addFunds.jsp").forward(req, resp);
        } else {
            resp.sendRedirect(req.getContextPath() + "/Admin/manageWallet");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int account_id = Integer.parseInt(request.getParameter("accountId"));
        int amount = Integer.parseInt(request.getParameter("amount"));
        dao.addFundsToWallet(account_id, amount);
        response.sendRedirect(request.getContextPath() + "/Admin/manageWallet");
    }
}
