/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;
import java.sql.*;
import models.*;
import java.util.*;

/**
 *
 * @author admin
 */
public class TopictypeDAO {
    
    
    private Connection con;
    public String status;

    public TopictypeDAO() {
        try {
            con = new DBcontext().connection;
        } catch (Exception e) {
            status = "Error at connection " + e.getMessage();
        }
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    // Các phương thức quản lý Feedbacks...
    
    // Lấy tất cả các bản ghi từ bảng topictype
    public List<Topictype> getAllTopicTypes() {
        List<Topictype> topicTypes = new ArrayList<>();
        String xSql = "SELECT * FROM topictype";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String idTopic = rs.getString("id_topic");
                String topic = rs.getString("topic");
                String type = rs.getString("type");

                Topictype topictype = new Topictype(idTopic, type, topic);
                topicTypes.add(topictype);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return topicTypes;
    }

    // Thêm một bản ghi mới vào bảng topictype
    public boolean insertTopictype(Topictype topictype) {
        String xSql = "INSERT INTO topictype (id_topic, topic, type) VALUES (?, ?, ?)";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ps.setString(1, topictype.getIdtopic());
            ps.setString(2, topictype.getTopic());
            ps.setString(3, topictype.getType());
            int rowsAffected = ps.executeUpdate();
            ps.close();
            return rowsAffected > 0; // Return true if at least one row was affected
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    // Lấy các bản ghi từ bảng topictype dựa trên giá trị của cột type
    public List<Topictype> getTopicTypesByType(String type) {
        List<Topictype> topicTypes = new ArrayList<>();
        String xSql = "SELECT * FROM topictype WHERE type = ?";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ps.setString(1, type);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String idTopic = rs.getString("id_topic");
                String topic = rs.getString("topic");

                Topictype topictype = new Topictype(idTopic, type, topic);
                topicTypes.add(topictype);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return topicTypes;
    }
    
    
    public Topictype getTopicTypeById(String id) {
    Topictype topictype = null;
    String xSql = "SELECT * FROM topictype WHERE id_topic = ?";
    try {
        PreparedStatement ps = con.prepareStatement(xSql);
        ps.setString(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            String type = rs.getString("type");
            String topic = rs.getString("topic");

            topictype = new Topictype(id, type, topic);
        }
        rs.close();
        ps.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
    return topictype;
}
}

    

