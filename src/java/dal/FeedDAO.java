/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.*;
import java.util.*;
import dal.*;
import models.*;
public class FeedDAO {

    public String status;
    private Connection con;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public FeedDAO() {
        try {
            con = new DBcontext().connection;
        } catch (Exception e) {
            status = "Error at connection " + e.getMessage();
        }
    }

    public List<Feedback> getRatingsByAccountId(int accountId) {
        List<Feedback> feedbacks = new ArrayList<>();
        String xSql = "SELECT * FROM Feedbacks WHERE account_id = ?";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id_feedback");
                String content = rs.getString("content_feedback");
                int starRate = rs.getInt("star_rate");
                int accountID = rs.getInt("account_id");

                Feedback feedback = new Feedback(id, content, starRate, accountID);
                feedbacks.add(feedback);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return feedbacks;
    }
    public boolean deleteFeedbackById(int id_feedback) {
        boolean check;
        String xSql = "DELETE FROM Feedbacks WHERE id_feedback = ?";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ps.setInt(1, id_feedback);
            ps.executeUpdate();
            ps.close();
             check =true;
        } catch (Exception e) {
            e.printStackTrace();
             check = false;
        }
        return check;
    }

    public List<Feedback> getFeedbacks() {
        List<Feedback> feedbacks = new ArrayList<>();
        String xSql = "SELECT * FROM Feedbacks";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id_feedback");
                String content = rs.getString("feedback_content");
                int starRate = rs.getInt("star_rate");
                int accountId = rs.getInt("account_id");

                Feedback feedback = new Feedback(id, content, starRate, accountId);
                feedbacks.add(feedback);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return feedbacks;
    }
    

    

   public boolean insertFeedback(Feedback feedback) {
    String xSql = "INSERT INTO Feedbacks (feedback_content, star_rate, account_id) VALUES (?, ?, ?)";
    try {
        PreparedStatement ps = con.prepareStatement(xSql);
        ps.setString(1, feedback.getContent());
        ps.setInt(2, feedback.getStar_rate());
        ps.setInt(3, feedback.getAccount_id());
        int rowsAffected = ps.executeUpdate();
        ps.close();
        return rowsAffected > 0;
    } catch (SQLException e) {
        e.printStackTrace();
        return false;
    }
}
public boolean updateFeedbackById(int feedbackId, String feedback_content, int star_rate) {
    String xSql = "UPDATE Feedbacks SET feedback_content = ?, star_rate = ? WHERE id_feedback = ?";
    try {
        PreparedStatement ps = con.prepareStatement(xSql);
        ps.setString(1, feedback_content);
        ps.setInt(2,  star_rate);
        
        ps.setInt(3, feedbackId);
        int rowsAffected = ps.executeUpdate();
        ps.close();
        return rowsAffected > 0;
    } catch (SQLException e) {
        e.printStackTrace();
        return false;
    }
}


  public Feedback getFeedbackById(int id_feedback) {
    Feedback feedback = null;
    String xSql = "SELECT * FROM Feedbacks WHERE id_feedback = ?";
    try {
        PreparedStatement ps = con.prepareStatement(xSql);
        ps.setInt(1, id_feedback);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("id_feedback");
            String content = rs.getString("feedback_content");
            int starRate = rs.getInt("star_rate");
            int accountId = rs.getInt("account_id");

            feedback = new Feedback(id, content, starRate, accountId);
        }
        rs.close();
        ps.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
    return feedback;
}
  
  public List<Feedback> getFeedbackByStar(int starRate) {
    List<Feedback> feedbacks = new ArrayList<>();
    String xSql = "SELECT * FROM Feedbacks WHERE star_rate = ?";
    try {
        PreparedStatement ps = con.prepareStatement(xSql);
        ps.setInt(1, starRate);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("id_feedback");
            String content = rs.getString("feedback_content");
            int rate = rs.getInt("star_rate");
            int accountId = rs.getInt("account_id");

            Feedback feedback = new Feedback(id, content, rate, accountId);
            feedbacks.add(feedback);
        }
        rs.close();
        ps.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
    return feedbacks;
}

  
  
  public Feedback getFeedbackByAc(int idac) {
    Feedback feedback = null;
    String xSql = "SELECT * FROM Feedbacks WHERE account_id = ?";
    try {
        PreparedStatement ps = con.prepareStatement(xSql);
        ps.setInt(1, idac);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("id_feedback");
            String content = rs.getString("feedback_content");
            int starRate = rs.getInt("star_rate");
            int accountId = rs.getInt("account_id");

            feedback = new Feedback(id, content, starRate, accountId);
        }
        rs.close();
        ps.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
    return feedback;}
}


    
    
// CREATE TABLE Feedbacks (
//    id_feedback VARCHAR(255) PRIMARY KEY,
//    content_feedback TEXT NOT NULL,
//    star_rate INT NOT NULL,
//    account_id VARCHAR(255) NOT NULL,
//   
//  
     /*
     
     CREATE TABLE topictype (
    id_topic VARCHAR(255) PRIMARY KEY,
    topic VARCHAR(255) unique NOT NULL,
    type VARCHAR(255) NOT NULL
);

-- Tạo bảng qandatopic
CREATE TABLE qandatopic (
    id_content VARCHAR(255) PRIMARY KEY,
    question TEXT NOT NULL,
    content TEXT NOT NULL,
    id_topic VARCHAR(255),
    FOREIGN KEY (id_topic) REFERENCES topictype(id_topic)
);
     
     
     */
     
     
  


    
    
    
    
    
    

