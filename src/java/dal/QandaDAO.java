/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;
import java.sql.*;
import java.util.*;
import models.*;
/**
 *
 * @author admin
 */
public class QandaDAO {
    
    private Connection con;
    public String status;

    public QandaDAO() {
        try {
            con = new DBcontext().connection;
        } catch (Exception e) {
            status = "Error at connection " + e.getMessage();
        }
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public boolean updateQandatopic(Qandatopic qanda) {
        String sql = "UPDATE qandatopic SET question = ?, content = ? WHERE id_content = ?";

        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, qanda.getQuestion());
            ps.setString(2, qanda.getContent());
            ps.setString(3, qanda.getIdContent());
            int rowsUpdated = ps.executeUpdate();
            return rowsUpdated > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    
public List<Qandatopic> searchQandatopics(String keyword) {
    List<Qandatopic> qandaTopics = new ArrayList<>();
    String xSql = "SELECT * FROM qandatopic \n" +
"WHERE question LIKE ? \n" +
"AND id_topic IN ('101', '102', 'SYLY1438');";
    try {
        PreparedStatement ps = con.prepareStatement(xSql);
        ps.setString(1, "%" + keyword + "%");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            String idContent = rs.getString("id_content");
            String question = rs.getString("question");
            String content = rs.getString("content");
            String idTopic = rs.getString("id_topic");

            Qandatopic qandatopic = new Qandatopic(idContent, question, content, idTopic);
            qandaTopics.add(qandatopic);
        }
        rs.close();
        ps.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
    return qandaTopics;
}


    
    
    
    
    
    
    public List<Qandatopic> getAllQandatopics() {
        List<Qandatopic> qandaTopics = new ArrayList<>();
        String xSql = "SELECT * FROM qandatopic";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String idContent = rs.getString("id_content");
                String question = rs.getString("question");
                String content = rs.getString("content");
                String idTopic = rs.getString("id_topic");

                Qandatopic qandatopic = new Qandatopic(idContent, question, content, idTopic);
                qandaTopics.add(qandatopic);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qandaTopics;
    }

    // Thêm một bản ghi mới vào bảng qandatopic
    public boolean insertQandatopic(Qandatopic qandatopic) {
        String xSql = "INSERT INTO qandatopic (id_content, question, content, id_topic) VALUES (?, ?, ?, ?)";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ps.setString(1, qandatopic.getIdContent());
            ps.setString(2, qandatopic.getQuestion());
            ps.setString(3, qandatopic.getContent());
            ps.setString(4, qandatopic.getIdTopic());
            int rowsAffected = ps.executeUpdate();
            ps.close();
            return rowsAffected > 0; // Return true if at least one row was affected
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // Lấy các bản ghi từ bảng qandatopic dựa trên id_topic
    public List<Qandatopic> getQandatopicsByTopicId(String idTopic) {
        List<Qandatopic> qandaTopics = new ArrayList<>();
        String xSql = "SELECT * FROM qandatopic WHERE id_topic = ?";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ps.setString(1, idTopic);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String idContent = rs.getString("id_content");
                String question = rs.getString("question");
                String content = rs.getString("content");

                Qandatopic qandatopic = new Qandatopic(idContent, question, content, idTopic);
                qandaTopics.add(qandatopic);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qandaTopics;
    }
    public boolean deleteQandatopicByIdContent(String idContent) {
    boolean result = false;
    String xSql = "DELETE FROM qandatopic WHERE id_content = ?";
    try {
        PreparedStatement ps = con.prepareStatement(xSql);
        ps.setString(1, idContent);
        int rowsAffected = ps.executeUpdate();
        if (rowsAffected > 0) {
            result = true;
        }
        ps.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
    return result;
}

    
    public int countQandatopicsByTopicId(String idTopic) {
    int count = 0;
    String xSql = "SELECT COUNT(*) FROM qandatopic WHERE id_topic = ?";
    try {
        PreparedStatement ps = con.prepareStatement(xSql);
        ps.setString(1, idTopic);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            count = rs.getInt(1);
        }
        rs.close();
        ps.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
    return count;
}
    
    


    
    // Lấy các bản ghi từ bảng qandatopic dựa trên id_content
    public Qandatopic getQandatopicByIdContent(String idContent) {
        Qandatopic qandatopic = null;
        String xSql = "SELECT * FROM qandatopic WHERE id_content = ?";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ps.setString(1, idContent);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String question = rs.getString("question");
                String content = rs.getString("content");
                String idTopic = rs.getString("id_topic");

                qandatopic = new Qandatopic(idContent, question, content, idTopic);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qandatopic;
    }
}


