package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import models.*;

public class BlogDAO {
    private Connection con;

    public BlogDAO() {
        try {
            con = new  DBcontext().connection; // DBcontext quản lý kết nối đến cơ sở dữ liệu
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
public List<Blog> searchBlogs(String title, String type) {
    List<Blog> blogs = new ArrayList<>();
    StringBuilder sql = new StringBuilder("SELECT * FROM blog WHERE state = 'duyet'");
    if (title != null && !title.isEmpty()) {
        sql.append(" AND title LIKE ?");
    }
    if (type != null && !type.isEmpty()) {
        sql.append(" AND type = ?");
    }
    try {
        PreparedStatement ps = con.prepareStatement(sql.toString());

        int paramIndex = 1;
        if (title != null && !title.isEmpty()) {
            ps.setString(paramIndex++, "%" + title + "%");
        }
        if (type != null && !type.isEmpty()) {
            ps.setString(paramIndex++, type);
        }
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            int idacount = rs.getInt("idacount");
            String content = rs.getString("content");
            String titleBlog = rs.getString("title");
            String typeBlog = rs.getString("type");
            String state = rs.getString("state");
            Date time_up = rs.getDate("time_up");
            String idBlog = rs.getString("idBlog");

            Blog blog = new Blog(idacount, content, titleBlog, typeBlog, state, time_up, idBlog);
            blogs.add(blog);
        }
        rs.close();
        ps.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return blogs;
}

    
   public List<Blog> searchBlogsByTitle(String title1) {
        List<Blog> blogs = new ArrayList<>();
        String sql = "SELECT * FROM blog WHERE title LIKE ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "%" + title1 + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int idacount = rs.getInt("idacount");
                String content = rs.getString("content");
                String title = rs.getString("title");
                String type = rs.getString("type");
                String state = rs.getString("state");
                Date time_up = rs.getDate("time_up");
                String idBlog = rs.getString("idBlog");

                Blog blog = new Blog(idacount, content, title, type, state, time_up, idBlog);
                blogs.add(blog);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return blogs;
    }
    // Lấy tất cả các bài đăng từ bảng blog
    public List<Blog> getAllBlogs() {
        List<Blog> blogs = new ArrayList<>();
        String sql = "SELECT * FROM blog";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int idacount = rs.getInt("idacount");
                String content = rs.getString("content");
                String title = rs.getString("title");
                String type = rs.getString("type");
                String state = rs.getString("state");
                Date time_up = rs.getDate("time_up");
                String idBlog = rs.getString("idBlog");

                Blog blog = new Blog(idacount, content, title, type, state, time_up, idBlog);
                blogs.add(blog);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return blogs;
    }

    // Thêm một bài đăng mới vào bảng blog
    public boolean insertBlog(Blog blog) {
        String sql = "INSERT INTO blog (idacount, content, title, type, state, time_up, idBlog) VALUES (?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, blog.getIdacount());
            ps.setString(2, blog.getContent());
            ps.setString(3, blog.getTitle());
            ps.setString(4, blog.getType());
            ps.setString(5, blog.getState());
            ps.setDate(6, new java.sql.Date(blog.getTime_up().getTime()));
            ps.setString(7, blog.getIdBlog());
            int rowsAffected = ps.executeUpdate();
            ps.close();
            return rowsAffected > 0; // Return true if at least one row was affected
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    // Cập nhật thông tin của một bài đăng trong bảng blog
    // Cập nhật thông tin của một bài đăng trong bảng blog dựa trên idBlog
public boolean updateBlog(Blog blog) {
    String sql = "UPDATE blog SET idacount=?, content=?, title=?, type=?, state=?, time_up=? WHERE idBlog=?";
    try {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, blog.getIdacount());
        ps.setString(2, blog.getContent());
        ps.setString(3, blog.getTitle());
        ps.setString(4, blog.getType());
        ps.setString(5, blog.getState());
        ps.setDate(6, new java.sql.Date(blog.getTime_up().getTime()));
        ps.setString(7, blog.getIdBlog()); // idBlog as the last parameter
        int rowsAffected = ps.executeUpdate();
        ps.close();
        return rowsAffected > 0; // Return true if at least one row was affected
    } catch (SQLException e) {
        e.printStackTrace();
        return false;
    }
}


    // Xóa một bài đăng từ bảng blog dựa trên idacount
    public boolean deleteBlog(String id) {
        String sql = "DELETE FROM blog WHERE idBlog=?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, id);
            int rowsAffected = ps.executeUpdate();
            ps.close();
            return rowsAffected > 0; // Return true if at least one row was affected
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    // Lấy thông tin của một bài đăng từ bảng blog dựa trên idacount
   // Lấy tất cả các bài đăng từ bảng blog dựa trên idacount
public List<Blog> getAllBlogsByIdacount(int idacount) {
    List<Blog> blogs = new ArrayList<>();
    String sql = "SELECT * FROM blog WHERE idacount=?";
    try {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, idacount);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            String content = rs.getString("content");
            String title = rs.getString("title");
            String type = rs.getString("type");
            String state = rs.getString("state");
            Date time_up = rs.getDate("time_up");
            String idBlog = rs.getString("idBlog");

            Blog blog = new Blog(idacount, content, title, type, state, time_up, idBlog);
            blogs.add(blog);
        }
        rs.close();
        ps.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return blogs;
    
}
public Blog getBlogById(String idBlog) {
    Blog blog = null;
    String sql = "SELECT * FROM blog WHERE idBlog = ?";
    try {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, idBlog);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            int idacount = rs.getInt("idacount");
            String content = rs.getString("content");
            String title = rs.getString("title");
            String type = rs.getString("type");
            String state = rs.getString("state");
            Date time_up = rs.getDate("time_up");

            blog = new Blog(idacount, content, title, type, state, time_up, idBlog);
        }
        rs.close();
        ps.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return blog;
}

// Lấy tất cả các bài đăng từ bảng blog dựa trên trạng thái (state)
public List<Blog> getAllBlogsByState(String state) {
    List<Blog> blogs = new ArrayList<>();
    String sql = "SELECT * FROM blog WHERE state=?";
    try {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, state);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            int idacount = rs.getInt("idacount");
            String content = rs.getString("content");
            String title = rs.getString("title");
            String type = rs.getString("type");
            Date time_up = rs.getDate("time_up");
            String idBlog = rs.getString("idBlog");

            Blog blog = new Blog(idacount, content, title, type, state, time_up, idBlog);
            blogs.add(blog);
        }
        rs.close();
        ps.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return blogs;
}
public void deleteBlogById(String idBlog) {
    String sql = "DELETE FROM blog WHERE idBlog=?";
    try {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, idBlog);
        ps.executeUpdate();
        ps.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
}
public int getAccountIdByIdBlog(String idBlog) {
    int idacount = -1; 
    String sql = "SELECT idacount FROM blog WHERE idBlog = ?";
    try {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, idBlog);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            idacount = rs.getInt("idacount");
        }
        rs.close();
        ps.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return idacount;
}


public List<Blog> getAllBlogsByStateAndType(String state, String type) {
    List<Blog> blogs = new ArrayList<>();
    String sql = "SELECT * FROM blog WHERE state=? AND type=?";
    try {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, state);
        ps.setString(2, type);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            int idacount = rs.getInt("idacount");
            String content = rs.getString("content");
            String title = rs.getString("title");
            Date time_up = rs.getDate("time_up");
            String idBlog = rs.getString("idBlog");

            Blog blog = new Blog(idacount, content, title, type, state, time_up, idBlog);
            blogs.add(blog);
        }
        rs.close();
        ps.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return blogs;
}
public int countBlogsByState(String state) {
    int count = 0;
    String sql = "SELECT COUNT(*) FROM blog WHERE state=?";
    try {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, state);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            count = rs.getInt(1);
        }
        rs.close();
        ps.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return count;
}
public void updateBlogState(String idBlog, String newState) {
    String sql = "UPDATE blog SET state=? WHERE idBlog=?";
    try {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, newState);
        ps.setString(2, idBlog);
        ps.executeUpdate();
        ps.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
}



}

