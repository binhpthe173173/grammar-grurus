package dal;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import models.*;
import java.util.Date;

/**
 *
 * @author Nam
 */
public class DAO {

    int index = 8;
    private Connection con;
    private List<Account> accounts;
    private List<models.Error> errors;
    private List<Feedback> feedbacks;
    private List<Post> posts;
    private List<Role> roles;
    private List<Subscription> subscriptions;
    private List<Wallet> wallets;
    private List<Transaction> transactions;
    String status = "ok";

    public DAO() {
        try {
            con = new DBcontext().connection;
        } catch (Exception e) {
            status = "Error at connection " + e.getMessage();
        }
    }

    public DAO(Connection con, List<Account> accounts, List<models.Error> errors, List<Feedback> feedbacks, List<Post> posts, List<Role> roles) {
        this.con = con;
        this.accounts = accounts;
        this.errors = errors;
        this.feedbacks = feedbacks;
        this.posts = posts;
        this.roles = roles;
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public List<models.Error> getErrors() {
        return errors;
    }

    public void setErrors(List<models.Error> errors) {
        this.errors = errors;
    }

    public List<Feedback> getFeedbacks() {
        return feedbacks;
    }

    public void setFeedbacks(List<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public List<Wallet> getWallets() {
        return wallets;
    }

    public void setWallets(List<Wallet> wallets) {
        this.wallets = wallets;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public void LoadAccount() {
        String sql = "Select * from [Accounts]";
        accounts = new Vector<Account>();
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String username = rs.getString(2);
                String password = rs.getString(3);
                String full_name = rs.getString(4);
                String email = rs.getString(5);
                String image = rs.getString(6);
                java.sql.Date time_created = rs.getDate(7);
                boolean active_status = rs.getBoolean(8);
                int role_id = rs.getInt(9);
                accounts.add(new Account(id, username, password, full_name, email, image, time_created, active_status, role_id));
            }
        } catch (Exception e) {
            status = "Error load at Account " + e.getMessage();
        }
    }

    public void LoadError() {
        String sql = "Select * from [Errors]";
        errors = new Vector<models.Error>();
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String content = rs.getString(2);
                String suggest = rs.getString(3);
                int post_id = rs.getInt(4);
                errors.add(new models.Error(id, content, suggest, post_id));
            }
        } catch (Exception e) {
            status = "Error load at Error " + e.getMessage();
        }
    }

    public void LoadFeedback() {
        String sql = "Select * from [Feedbacks]";
        feedbacks = new Vector<Feedback>();
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String content = rs.getString(2);
                int star_rate = rs.getInt(3);
                int account_id = rs.getInt(4);
                feedbacks.add(new Feedback(id, content, star_rate, account_id));
            }
        } catch (Exception e) {
            status = "Error load at Feedback " + e.getMessage();
        }
    }

    public void LoadPost() {
        String sql = "Select * from [Posts]";
        posts = new Vector<Post>();
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String content = rs.getString(2);
                String fixed_content = rs.getString(3);
                java.sql.Date time_created = rs.getDate(4);
                int account_id = rs.getInt(5);
                posts.add(new Post(id, content, fixed_content, time_created, account_id));
            }
        } catch (Exception e) {
            status = "Error load at Post " + e.getMessage();
        }
    }

    public void LoadRole() {
        String sql = "Select * from [Role]";
        roles = new Vector<Role>();
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                roles.add(new Role(id, name));
            }
        } catch (Exception e) {
            status = "Error load at Post " + e.getMessage();
        }
    }

    public void LoadSubscription() {
        String sql = "Select * from [Subscription]";
        subscriptions = new Vector<Subscription>();
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                int price = rs.getInt(3);
                int duration = rs.getInt(4);
                boolean status = rs.getBoolean(5);
                String description = rs.getString(6);
                java.sql.Date time_created = rs.getDate(7);
                subscriptions.add(new Subscription(id, name, price, duration, status, description, time_created));
            }
        } catch (Exception e) {
            status = "Error load at Subscription " + e.getMessage();
        }
    }

    public void LoadWallet() {
        String sql = "Select * from [Wallet]";
        wallets = new Vector<Wallet>();
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                int account_id = rs.getInt(2);
                int balance = rs.getInt(3);
                wallets.add(new Wallet(id, account_id, balance));
            }
        } catch (Exception e) {
            status = "Error load at Wallet " + e.getMessage();
        }
    }

    public void LoadTransaction() {
        String sql = "SELECT th.id, th.wallet_id, th.amount, th.transaction_type, th.description, th.transaction_date, ac.username\n"
                + "FROM TransactionHistory th\n"
                + "JOIN Wallet w ON th.wallet_id = w.id\n"
                + "JOIN Accounts ac ON w.account_id = ac.id_account;";
        transactions = new Vector<Transaction>();
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                int wallet_id = rs.getInt(2);
                int amount = rs.getInt(3);
                String transaction_type = rs.getString(4);
                String description = rs.getString(5);
                java.sql.Date transaction_date = rs.getDate(6);
                transactions.add(new Transaction(id, wallet_id, amount, transaction_type, description, transaction_date));
            }
        } catch (Exception e) {
            status = "Error load at Wallet " + e.getMessage();
        }
    }

    public Account getAccountByMail(String email) {
        Account ac = null;
        String xSql = "SELECT * FROM Accounts WHERE email = ?";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int idAccount = rs.getInt("id_account");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String fullName = rs.getString("full_name");
                String emailDb = rs.getString("email");
                String image = rs.getString("image");
                java.sql.Date timeCreated = rs.getDate("time_created");
                boolean activeStatus = rs.getBoolean("active_status");
                int roleId = rs.getInt("role_id");

                // Tạo đối tượng User từ dữ liệu lấy được từ ResultSet
                ac = new Account(idAccount, username, password, fullName, emailDb, image, timeCreated, activeStatus, roleId);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ac;
    }

    public Account getAccountByUsername(String username) {
        Account ac = null;
        String xSql = "SELECT * FROM Accounts WHERE username = ?";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int idAccount = rs.getInt("id_account");
                String password = rs.getString("password");
                String fullName = rs.getString("full_name");
                String email = rs.getString("email");
                String image = rs.getString("image");
                java.sql.Date timeCreated = rs.getDate("time_created");
                boolean activeStatus = rs.getBoolean("active_status");
                int roleId = rs.getInt("role_id");

                // Tạo đối tượng User từ dữ liệu lấy được từ ResultSet
                ac = new Account(idAccount, username, password, fullName, email, image, timeCreated, activeStatus, roleId);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ac;
    }

    public void changeImgByIdAc(int Id, String newImg) {
        String query = "UPDATE Accounts SET  image = ? WHERE id_account = ?";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, newImg);
            ps.setInt(2, Id);
            ps.executeUpdate();
            ResultSet rs = ps.executeQuery();
            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void changeNameByIdAc(int Id, String newName) {
        String query = "UPDATE Accounts SET  username = ? WHERE id_account = ?";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, newName);
            ps.setInt(2, Id);
            ps.executeUpdate();
            ResultSet rs = ps.executeQuery();
            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void changePassByIdAc(int Id, String newPass) {
        String query = "UPDATE Accounts SET  password = ? WHERE id_account = ?";
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.setString(1, newPass);
            ps.setInt(2, Id);
            ps.executeUpdate();
            ResultSet rs = ps.executeQuery();
            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Account getAccountById(int id) {
        Account ac = null;
        String xSql = "SELECT * FROM Accounts WHERE id_account = ?";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int idAccount = rs.getInt("id_account");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String fullName = rs.getString("full_name");
                String emailDb = rs.getString("email");
                String image = rs.getString("image");
                java.sql.Date timeCreated = rs.getDate("time_created");
                boolean activeStatus = rs.getBoolean("active_status");
                int roleId = rs.getInt("role_id");

                // Tạo đối tượng User từ dữ liệu lấy được từ ResultSet
                ac = new Account(idAccount, username, password, fullName, emailDb, image, timeCreated, activeStatus, roleId);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ac;
    }

    public Subscription getSubscriptionById(int id) {
        Subscription sub = null;
        String xSql = "SELECT * FROM Subscription WHERE id = ?";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String name = rs.getString(2);
                int price = rs.getInt(3);
                int duration = rs.getInt(4);
                boolean status = rs.getBoolean(5);
                String description = rs.getString(6);
                java.sql.Date timeCreated = rs.getDate(7);
                sub = new Subscription(id, name, price, duration, status, description, timeCreated);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sub;
    }

    public void addAccount(String username, String password, String full_name, String email, String img, boolean active_status, int role_id) {
        String sql = "INSERT INTO [Accounts] ([username], [password], [full_name], [time_created], [email], [image], [active_status], [role_id])\n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, full_name);
            ps.setString(4, "10-10-20");
            ps.setString(5, email);
            ps.setString(6, img);
            ps.setBoolean(7, active_status);
            ps.setInt(8, role_id);
            ps.execute();
        } catch (Exception e) {

        }
    }

    public void addSubscription(String name, int price, int duration, boolean status, String description) {
        String sql = "INSERT INTO [Subscription] ([name], [price], [duration], [status], [description])\n"
                + "VALUES (?, ?, ?, ?, ?);";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, name);
            ps.setInt(2, price);
            ps.setInt(3, duration);
            ps.setBoolean(4, status);
            ps.setString(5, description);
            ps.execute();
        } catch (Exception e) {

        }
    }

    public void createPost(String postContent, String fixedContent, int accountId) {
        Date timeCreated = new Date();
        String sql = "INSERT INTO [Posts] (post_content, fixed_content, time_created, account_id)\n"
                + "VALUES (?,?,?,?);";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, postContent);
            ps.setString(2, fixedContent);
            ps.setDate(3, new java.sql.Date(timeCreated.getTime()));
            ps.setInt(4, accountId);
            ps.execute();
        } catch (Exception e) {//

        }
    }

    public Account findAccount(String username) {
        for (Account account : accounts) {
            if (account.getUsername().equals(username)) {
                return account;
            }
        }
        return null;
    }

    public Account findAccountbyId(int id) {
        for (Account account : accounts) {
            if (account.getId() == id) {
                return account;
            }
        }
        return null;
    }

    public Account findAccountbyEmail(String email) {
        for (Account account : accounts) {
            if (account.getEmail().equalsIgnoreCase(email)) {
                return account;
            }
        }
        return null;
    }

    public boolean checkEmail(String email) {
        String regex = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
        return email.matches(regex);
    }

    public boolean checkUsername(String username) {
        String regex = "^[A-Za-z]\\w{5,29}$";
        return username.matches(regex);
    }

    public boolean checkPassword(String password) {
        String regex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$";
        return password.matches(regex);
    }

    public List<Account> getAllAcc() {
        List<Account> allAccounts = new ArrayList<>();
        String sql = "Select * from [Accounts]";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String username = rs.getString(2);
                String password = rs.getString(3);
                String full_name = rs.getString(4);
                String email = rs.getString(5);
                String image = rs.getString(6);
                java.sql.Date time_created = rs.getDate(7);
                boolean active_status = rs.getBoolean(8);
                int role_id = rs.getInt(9);
                allAccounts.add(new Account(id, username, password, full_name, email, image, time_created, active_status, role_id));
            }
        } catch (Exception e) {
            status = "Error load at Account " + e.getMessage();
        }
        return allAccounts;
    }

    public void deleteAccount(int id) {
        String sql = "DELETE FROM TransactionHistory WHERE wallet_id IN (SELECT id FROM Wallet WHERE account_id = ?);\n"
                + "DELETE FROM Wallet WHERE account_id = ?;\n"
                + "DELETE FROM Accounts WHERE id_account = ?;";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            ps.setInt(2, id);
            ps.setInt(3, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateAccount(int id, String username, String full_name, String email, String image, boolean active_status, int role_id) {
        String sql = "UPDATE Accounts SET username = ?, full_name = ?, email = ?, image = ?, active_status = ?, role_id = ? WHERE id_account = ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, username);
            ps.setString(2, full_name);
            ps.setString(3, email);
            ps.setString(4, image);
            ps.setBoolean(5, active_status);
            ps.setInt(6, role_id);
            ps.setInt(7, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updatePost(int id, String fixedContent, int accountId) {
        String sql = "UPDATE Posts SET fixed_content =?, account_id =? WHERE id_post =?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, fixedContent);
            ps.setInt(2, accountId);
            ps.setInt(3, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateSubscription(int id, String name, int price, int duration, boolean status, String description) {
        String sql = "UPDATE subscription SET [name]=?, [price]=?, [duration]=?, [status]=?, [description]=? WHERE [id]=?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, name);
            ps.setInt(2, price);
            ps.setInt(3, duration);
            ps.setBoolean(4, status);
            ps.setString(5, description);
            ps.setInt(6, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean getAccountByMailTF(String email) {
        String sql = "SELECT COUNT(*) FROM Accounts WHERE email = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int count = rs.getInt(1);
                return count > 0; // Trả về true nếu có ít nhất một tài khoản có địa chỉ email này
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean checkDuplicateEmailForOtherAccount(String email, int accountId) {
        String sql = "SELECT COUNT(*) FROM Accounts WHERE email = ? AND id_account <> ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, email);
            ps.setInt(2, accountId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int count = rs.getInt(1);
                return count > 0; // Trả về true nếu có tài khoản khác có địa chỉ email này và khác ID
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false; // Trả về false nếu không có tài khoản khác nào có địa chỉ email này hoặc có lỗi xảy ra
    }

    public Post getPostByContent(String postContent, int id) {
        String sql = "SELECT * FROM [Posts] WHERE post_content = ? and account_id = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, postContent);
            ps.setInt(2, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Post post = new Post();
                post.setId(rs.getInt("id_post"));
                post.setContent(rs.getString("post_content"));
                post.setFixed_content(rs.getString("fixed_content"));
                post.setTime_created(rs.getDate("time_created"));
                post.setAccount_id(rs.getInt("account_id"));
                return post;
            }
        } catch (Exception e) {
            // handle the exception
        }
        return null;
    }

    public void createError(String errorContent, String errorSuggest, int postId) {
        String sql = "INSERT INTO [Errors] (error_content, error_suggest, post_id)\n"
                + "VALUES (?,?,?);";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, errorContent);
            ps.setString(2, errorSuggest);
            ps.setInt(3, postId);
            ps.execute();
        } catch (Exception e) {
            // handle the exception
        }
    }

    public void deleteSubscription(int id) {
        String sql = "DELETE FROM Subscription WHERE id=?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void deleteAccountSubscription(int subId) {
        String sql = "DELETE FROM AccountSubscription WHERE subscription_id=?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, subId);
            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Wallet getWalletByAccountId(int account_id) {
        Wallet wallet = null;
        String sql = "SELECT * FROM Wallet WHERE account_id = ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, account_id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                wallet = new Wallet();
                wallet.setId(rs.getInt(1));
                wallet.setAccount_id(rs.getInt(2));
                wallet.setBalance(rs.getInt(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return wallet;
    }

    public List<Transaction> getTransactionByWalletId(int wallet_id) {
        ArrayList<Transaction> trans = new ArrayList<Transaction>();
        String sql = "SELECT * FROM TransactionHistory WHERE wallet_id = ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, wallet_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                int wallet = rs.getInt(2);
                int amount = rs.getInt(3);
                String type = rs.getString(4);
                String des = rs.getString(5);
                java.util.Date date = rs.getDate(6);
                Transaction tr = new Transaction(id, wallet, amount, type, des, date);
                trans.add(tr);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return trans;
    }

    public void updateWalletBalance(int walletId, int amount) {
        String sql = "UPDATE Wallet SET Balance = Balance + ? WHERE ID = ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, amount);
            ps.setInt(2, walletId);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addTransaction(int wallet_id, int amount, String transaction_type, String description) {
        String sql = "INSERT INTO TransactionHistory ([wallet_id], [amount], [transaction_type], [description]) VALUES (?, ?, ?, ?)";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, wallet_id);
            ps.setInt(2, amount);
            ps.setString(3, transaction_type);
            ps.setString(4, description);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addFundsToWallet(int account_id, int amount) {
        String sql = "UPDATE Wallet SET balance = balance + ? WHERE account_id = ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setDouble(1, amount);
            ps.setInt(2, account_id);
            ps.executeUpdate();

            String getWalletIdSql = "SELECT * FROM Wallet WHERE account_id = ?";
            try (PreparedStatement psGet = con.prepareStatement(getWalletIdSql)) {
                psGet.setInt(1, account_id);
                ResultSet rs = psGet.executeQuery();
                if (rs.next()) {
                    int wallet_id = rs.getInt(1);
                    addTransaction(wallet_id, amount, "Deposit", "Admin add funds");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean processSubscription(int accountId, int subscriptionId) {
        String sql = "SELECT [price] FROM Subscription WHERE [id]=?";
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            pst = con.prepareStatement(sql);
            pst.setInt(1, subscriptionId);
            rs = pst.executeQuery();

            if (rs.next()) {
                int price = rs.getInt("price");

                sql = "SELECT [balance], [id] FROM Wallet WHERE [account_id]=?";
                pst = con.prepareStatement(sql);
                pst.setInt(1, accountId);
                rs = pst.executeQuery();

                if (rs.next()) {
                    int balance = rs.getInt(1);
                    int wallet_id = rs.getInt(2);

                    if (balance >= price) {
                        sql = "UPDATE Wallet SET balance = balance - ? WHERE [account_id]=?";
                        pst = con.prepareStatement(sql);
                        pst.setInt(1, price);
                        pst.setInt(2, accountId);
                        pst.executeUpdate();

                        sql = "INSERT INTO TransactionHistory (wallet_id, amount, transaction_type, description) VALUES (?, ?, ?, ?)";
                        pst = con.prepareStatement(sql);
                        pst.setInt(1, wallet_id);
                        pst.setInt(2, price);
                        pst.setString(3, "Subscription");
                        pst.setString(4, "Subscribed to package: " + subscriptionId);
                        pst.executeUpdate();
                        return true;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public List<models.Error> getErrorsByPostId(int postId) {
        List<models.Error> errors_post = new ArrayList<>();
        String sql = "SELECT * FROM Errors WHERE post_id = ?";

        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, postId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id_error");
                String content = rs.getString("error_content");
                String suggest = rs.getString("error_suggest");
                int post_id = rs.getInt("post_id");
                errors_post.add(new models.Error(id, content, suggest, post_id));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return errors_post;
    }

    public List<Post> getPostsByAccountId(int accountId) {
        List<Post> posts_account = new Vector<>();
        String sql = "SELECT * FROM Posts WHERE account_id = ?";

        try (PreparedStatement ps = con.prepareStatement(sql)) {

            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id_post");
                String content = rs.getString("post_content");
                String fixed_content = rs.getString("fixed_content") == null ? "" : rs.getString("fixed_content");
                java.util.Date time_created = rs.getDate("time_created");
                int account_id = rs.getInt("account_id");
                String title = rs.getString("title");
                posts_account.add(new Post(id, content, fixed_content, time_created, account_id, title));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return posts_account;
    }

    public Post getPostbyId(int id) {
        Post post = null;
        String sql = "SELECT * FROM Posts WHERE id_post = ?";

        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String content = rs.getString("post_content");
                String fixed_content = rs.getString("fixed_content") == null ? "" : rs.getString("fixed_content");
                java.util.Date time_created = rs.getDate("time_created");
                int account_id = rs.getInt("account_id");
                post = new Post(id, content, fixed_content, time_created, account_id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return post;
    }

    public int getNumberPage() {
        String sql = "SELECT COUNT(*) FROM Accounts";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int total = rs.getInt(1);
                int countPage = total / index;
                if (total % index != 0) {
                    countPage++;
                }
                return countPage;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Account> getPage(int page) {

        List<Account> list = new ArrayList<>();
        String sql = "SELECT * FROM Accounts "
                + "ORDER BY id_account "
                + "OFFSET ? ROWS "
                + "FETCH  NEXT " + index + " ROW ONLY;";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, (page - 1) * index);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String username = rs.getString(2);
                String password = rs.getString(3);
                String full_name = rs.getString(4);
                String email = rs.getString(5);
                String image = rs.getString(6);
                java.sql.Date time_created = rs.getDate(7);
                boolean active_status = rs.getBoolean(8);
                int role_id = rs.getInt(9);
                list.add(new Account(id, username, password, full_name, email, image, time_created, active_status, role_id));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Account> searchByName(String text) {
        List<Account> listSearch = new ArrayList<>();
        String sql = "select * from Accounts where username LIKE ?";
        try {
            PreparedStatement st = con.prepareStatement(sql);
            st.setString(1, "%" + text + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Account acc = new Account();
                acc.setId(rs.getInt("id_account"));
                acc.setUsername(rs.getString("username"));
                //acc.setPassword(rs.getString("password"));
                acc.setFull_name(rs.getString("full_name"));
                acc.setEmail(rs.getString("email"));
                acc.setImage(rs.getString("image"));
                acc.setTime_created(rs.getDate("time_created"));
                acc.setActive_status(rs.getBoolean("active_status"));
                acc.setRole_id(rs.getInt("role_id"));
                listSearch.add(acc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listSearch;
    }

    // Existing code for database connection
    public List<Account> searchByFilters(String text, Boolean activeStatus, Integer roleId) {
        List<Account> accounts = new ArrayList<>();
        StringBuilder query = new StringBuilder("SELECT * FROM accounts WHERE username LIKE ?");

        if (activeStatus != null) {
            query.append(" AND active_status = ?");
        }
        if (roleId != null) {
            query.append(" AND role_id = ?");
        }

        try (
                PreparedStatement ps = con.prepareStatement(query.toString())) {

            int paramIndex = 1;
            ps.setString(paramIndex++, "%" + text + "%");
            if (activeStatus != null) {
                ps.setBoolean(paramIndex++, activeStatus);
            }
            if (roleId != null) {
                ps.setInt(paramIndex++, roleId);
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account account = new Account();
                account.setId(rs.getInt("id_account"));
                account.setUsername(rs.getString("username"));
                account.setFull_name(rs.getString("full_name"));
                account.setEmail(rs.getString("email"));
                account.setImage(rs.getString("image"));
                account.setTime_created(rs.getDate("time_created"));
                account.setActive_status(rs.getBoolean("active_status"));
                account.setRole_id(rs.getInt("role_id"));
                accounts.add(account);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return accounts;
    }

    public List<Account> searchByNameAndActiveStatus(String name, Boolean activeStatusTrue, Boolean activeStatusFalse) {
        List<Account> accounts = new ArrayList<>();
        String sql = "SELECT * FROM accounts WHERE username LIKE ? ";

        if (activeStatusTrue != null || activeStatusFalse != null) {
            sql += "AND (";
            if (activeStatusTrue != null) {
                sql += "active_status = 1";
            }
            if (activeStatusTrue != null && activeStatusFalse != null) {
                sql += " OR ";
            }
            if (activeStatusFalse != null) {
                sql += "active_status = 0";
            }
            sql += ")";
        }

        try (
                PreparedStatement ps = con.prepareStatement(sql)) {

            ps.setString(1, "%" + name + "%");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account account = new Account();
                account.setId(rs.getInt("id_account"));
                account.setUsername(rs.getString("username"));
                account.setFull_name(rs.getString("full_name"));
                account.setEmail(rs.getString("email"));
                account.setImage(rs.getString("image"));
                account.setTime_created(rs.getTimestamp("time_created"));
                account.setActive_status(rs.getBoolean("active_status"));
                account.setRole_id(rs.getInt("role_id"));
                accounts.add(account);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return accounts;
    }

    public int getNumberOfAccounts(int roleId) {
        int numberOfAccounts = 0;
        String sql = "SELECT COUNT(*) FROM [Accounts] WHERE role_id <> ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, roleId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                numberOfAccounts = rs.getInt(1);
            }
        } catch (Exception e) {
            status = "Error load at Account " + e.getMessage();
        }
        return numberOfAccounts;
    }

    public int fetchTotalTransactions() throws SQLException {
        String sqlTotalTransactions = "SELECT COUNT(*) FROM TransactionHistory WHERE CAST(transaction_date AS DATE) = CAST(GETDATE() AS DATE);";

        int totalTransactions = 0;
        try {
            PreparedStatement ps = con.prepareStatement(sqlTotalTransactions);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                totalTransactions = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return totalTransactions;
    }

    public int fetchTotalAmount() {
        String sqlTotalAmount = "SELECT SUM(amount) FROM TransactionHistory WHERE transaction_type = 'Subscription' AND CAST(transaction_date AS DATE) = CAST(GETDATE() AS DATE);";

        int totalAmount = 0;
        try {
            PreparedStatement ps = con.prepareStatement(sqlTotalAmount);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                totalAmount = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return totalAmount;
    }

    public void addAccountSubscription(int accountId, int subscriptionId, LocalDateTime startDate, LocalDateTime endDate) {
        String sql = "INSERT INTO AccountSubscription (account_id, subscription_id, start_date, end_date, status) VALUES (?, ?, ?, ?, ?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, accountId);
            ps.setInt(2, subscriptionId);
            ps.setTimestamp(3, Timestamp.valueOf(startDate));
            ps.setTimestamp(4, Timestamp.valueOf(endDate));
            ps.setBoolean(5, true);

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<AccountSubscription> getAccountSubscriptionsByAccountId(int accountId) {
        List<AccountSubscription> subs = new ArrayList<>();
        String sql = "SELECT id, account_id, subscription_id, start_date, end_date, status FROM AccountSubscription WHERE account_id = ?";

        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt(1);
                int subscriptionId = rs.getInt(3);
                LocalDateTime startDate = rs.getTimestamp(4).toLocalDateTime();
                LocalDateTime endDate = rs.getTimestamp(5).toLocalDateTime();
                boolean status = rs.getBoolean(6);
                AccountSubscription as = new AccountSubscription(id, accountId, subscriptionId, startDate, endDate, status);
                subs.add(as);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subs;
    }
    
    public List<AccountSubscription> getAccountSubscriptionsBySubscriptionId(int subscriptionId) {
        List<AccountSubscription> subs = new ArrayList<>();
        String sql = "SELECT id, account_id, subscription_id, start_date, end_date, status FROM AccountSubscription WHERE subscription_id = ?";

        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, subscriptionId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt(1);
                int accountId = rs.getInt(2);
                LocalDateTime startDate = rs.getTimestamp(4).toLocalDateTime();
                LocalDateTime endDate = rs.getTimestamp(5).toLocalDateTime();
                boolean status = rs.getBoolean(6);
                AccountSubscription as = new AccountSubscription(id, accountId, subscriptionId, startDate, endDate, status);
                subs.add(as);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subs;
    }

    public void updatePremium(int accountId) {
        String sql = "UPDATE [dbo].[Accounts]\n"
                + "   SET [role_id] = 2\n"
                + " WHERE id_account = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, accountId);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void renamePost(int postId, String postTitle) {
        String sql = "UPDATE [dbo].[Posts]\n"
                + "   SET [title] = ?\n"
                + " WHERE id_post = ?";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, postTitle);
            ps.setInt(2, postId);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException {
        DAO dao = new DAO();
        Account ac = dao.getAccountByUsername("user4");
        System.out.println(ac.isActive_status());
    }
}

//DAO dao = new DAO();
//        int number = dao.fetchTotalTransactions();
//        System.out.println("totalAmount:" + number);
//        // Test 1: Search by username only
//        System.out.println("Test 1: Search by username only");
//        List<Account> result1 = dao.searchByFilters("admin", null, null);
//        for (Account account : result1) {
//            System.out.println(account);
//        }
//
//        // Test 2: Search by username and active status true
//        System.out.println("Test 2: Search by username and active status true");
//        List<Account> result2 = dao.searchByFilters("admin", true, null);
//        for (Account account : result2) {
//            System.out.println(account);
//        }
//
//        // Test 3: Search by username and active status false
//        System.out.println("Test 3: Search by username and active status false");
//        List<Account> result3 = dao.searchByFilters("admin", false, null);
//        for (Account account : result3) {
//            System.out.println(account);
//        }
//
//        // Test 4: Search by username and role ID
//        System.out.println("Test 4: Search by username and role ID");
//        List<Account> result4 = dao.searchByFilters("admin", null, 1);
//        for (Account account : result4) {
//            System.out.println(account);
//        }
//
//        // Test 5: Search by username, active status true, and role ID
//        System.out.println("Test 5: Search by username, active status true, and role ID");
//        List<Account> result5 = dao.searchByFilters("admin", true, 1);
//        for (Account account : result5) {
//            System.out.println(account);
//        }
//
//        // Test 6: Search by username, active status false, and role ID
//        System.out.println("Test 6: Search by username, active status false, and role ID");
//        List<Account> result6 = dao.searchByFilters("admin", false, 1);
//        for (Account account : result6) {
//            System.out.println(account);
//        }
