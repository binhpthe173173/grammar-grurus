<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Guest</title>
        <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            display: flex;
            flex-direction: column;
            min-height: 100vh;
            background-color: #f0f0f0;
        }
        header, footer {
            background-color: #ffffff;
            padding: 10px 0;
            text-align: center;
            box-shadow: 0 2px 4px rgba(0,0,0,0.1);
        }
        header div {
            display: flex;
            justify-content: center;
            align-items: center;
            gap: 20px;
        }
        header div a {
            color: #333;
            text-decoration: none;
            font-size: 1em;
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            transition: color 0.3s;
        }
        header div a:hover {
            color: #218B82 ;
        }
        .logo {
            font-size: 1.5em;
            font-weight: bold;
            color: #000;
            margin-right: 490px;
            left: 20px;
        }
        .content {
            flex: 1;
            padding: 50px 20px;
            text-align: center;
            background: linear-gradient(to right, #ffffff, #e6f7ff);
        }
        .content h1 {
            font-size: 2.5em;
            margin: 20px 0;
            color: #333;
            display: inline-block;
        }
        .content p {
            font-size: 1.2em;
            margin-bottom: 40px;
            color: #666;
        }
        .flex-container {
            display: flex;
            justify-content: center;
            align-items: center;
            gap: 20px;
        }
        .flex-container .buttons a {
            margin-right: 50px;
            padding: 15px 30px;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            font-size: 1em;
            transition: background 0.3s;
        }
        .get-started {
            background-color: #1a9c90;
        }
        .get-started:hover {
            background-color: #148f82;
        }
        .purchase {
            background-color: #007bff;
        }
        .purchase:hover {
            background-color: #0056b3;
        }
        .grammar-image img {
            max-width: 100%;
            height: auto;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0,0,0,0.1);
        }
        footer {
            margin-top: auto;
            padding: 10px 0;
        }
        .footer-links a ,h1 {
            margin: 0 10px;
            color: #333;
            text-decoration: none;
            transition: background 0.3s;
        }
        .footer-links a :hover {
            text-decoration: underline;
            color: #218B82 ;
        }
    </style>
    </head>
    <body>
        <header>
        <div>
            <a href="#" class="logo">Grammar Gurus</a>
            <a href="userBlog.jsp">Blog</a>
             <a href="grammarGuideList">GrammarGuide</a>
            <a href="listFAQ.jsp">FAQ</a>
            <a href="confirmLogin">Use App</a>
            <a href="register.jsp">Register</a>
        </div>
    </header>
    
    <div class="content">
        
        <div class="flex-container">
          
            <div class="buttons">
                
                <h1>Knowledge is power</h1><br/>
                <h1 style="margin-right: 140px;
    margin-top: 0px;">use it wisely.</h1>
                <p>Grammar Gurus helps you craft, polish, and elevate your writing.</p>
                <br>
                <a href="confirmLogin" class="get-started">Get Started — it's free</a>
<!--                <a href="#" class="purchase">Purchase</a>-->
            </div>
            <div class="grammar-image">
                <img src="https://assets.prowritingaid.com/f/145420/1096x800/b8c0377c98/hero_creativewriting_icpexperiments.png/m/684x500/" alt="Grammar Checking">
            </div>
        </div>
    </div>

    <footer>
        <div class="footer-links">
            <h1>Contact Us</h1>
            <a href="#">Gmail</a> |  
            <a href="https://www.facebook.com/truonglv1705/">Facebook</a> | 
            <a href="https://www.linkedin.com/in/truonglv1705certifications">LinkedIn</a>
        </div>
        <div>&copy; 2024 Grammar Gurus</div>
    </footer>
        
    </body>
</html>
