<%@ page import="models.*" %>
<%@ page import="dal.*" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<% 
    String blogId = request.getParameter("id");
    BlogDAO blogDAO = new BlogDAO();
    Blog blog = blogDAO.getBlogById(blogId);
    Account account = (Account) session.getAttribute("us");
    DAO dao1 = new DAO();
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><%= blog.getTitle() %></title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 800px;
            margin: 20px auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }
        h1 {
            color: #007A7A;
        }
        h2 {
            font-size: 1.5em;
            color: #333;
        }
        p {
            color: #666;
            line-height: 1.6;
        }
        .meta {
            margin-bottom: 20px;
        }
        .meta span {
            margin-right: 10px;
            color: #999;
        }
        .back-link {
            display: inline-block;
            margin-top: 20px;
            padding: 10px 15px;
            background-color: #007A7A;
            color: white;
            text-decoration: none;
            border-radius: 5px;
            transition: background-color 0.3s;
        }
        .back-link:hover {
            background-color: #005a5a;
        }
        .article-content {
        white-space: pre-wrap;
          white-space: pre-line;
    }
    </style>
</head>
<body>
    <div class="container">
        <h1><%= blog.getTitle() %></h1>
        <div class="meta">
             <span>Id Blog: <%= blog.getIdBlog() %></span><br>
            <span>Type: <%= blog.getType() %></span><br>
            <span>Posted on: <%= blog.getTime_up() %></span><br>
            <span>Author: <%= dao1.getAccountById(blog.getIdacount()).getUsername() %></span><br>
        </div>
        <div class="content">
            <p class="article-content" ><%= blog.getContent() %></p>
        </div>
        <a class="back-link" href="javascript:history.back()">Go Back</a>
    </div>
</body>
</html>
