<%-- 
    Document   : Inputtext2
    Created on : Jun 9, 2024, 7:43:56 PM
    Author     : admin
--%>
<%@ page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <style>
            body {
    font-family: Arial, sans-serif;
    background-color: #f0f0f0;
}

.dropdown {
    position: relative;
    display: inline-block;
    margin: 20px;
}

.dropbtn {
    background-color: #3498DB;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
    border-radius: 5px;
}

.dropbtn:hover,.dropbtn:focus {
    background-color: #2980B9;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    border-radius: 5px;
    padding: 10px;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown a:hover {background-color: #ddd;}

.show {display: block;}

table {
    border-collapse: collapse;
    width: 100%;
    margin: 20px;
}

th, td {
    border: 1px solid black;
    padding: 8px;
    text-align: left;
}

        </style>
    </head>
    <body>
        
        <script>
            /* When the user clicks on the button, 
            toggle between hiding and showing the dropdown content */
            function myFunction1() {
                document.getElementById("myDropdown1").classList.toggle("show");
            }

            function myFunction2() {
                document.getElementById("myDropdown2").classList.toggle("show");
            }

            function myFunction3() {
                document.getElementById("myDropdown3").classList.toggle("show");
            }

            function myFunction4() {
                document.getElementById("myDropdown4").classList.toggle("show");
            }

            // Close the dropdown if the user clicks outside of it
            window.onclick = function(event) {
                if (!event.target.matches('.dropbtn')) {
                    var dropdowns = document.getElementsByClassName("dropdown-content");
                    var i;
                    for (i = 0; i < dropdowns.length; i++) {
                        var openDropdown = dropdowns[i];
                        if (openDropdown.classList.contains('show') &&!event.target.parentNode.contains(openDropdown)) {
                            openDropdown.classList.remove('show');
                        }
                    }
                }
            };
        </script>
        <% 
            List<String> errorG = (List<String>) request.getAttribute("errorG");
            List<String> suggestG=(List<String>) request.getAttribute("suggestion");
            List<String> errorG2 = (List<String>) request.getAttribute("error2");
            List<String> suggestG2=(List<String>) request.getAttribute("suggestion2");
            List<String> errorG3 = (List<String>) request.getAttribute("error3");
            List<String> suggestG3=(List<String>) request.getAttribute("suggestion3");
            List<String> errorG4 = (List<String>) request.getAttribute("error4");
            List<String> suggestG4=(List<String>) request.getAttribute("suggestion4");
            List<String> errorAll = (List<String>) request.getAttribute("errorAll");
            List<String> errorAll2 = (List<String>) request.getAttribute("errorAll2");
            List<String> errorAll3 = (List<String>) request.getAttribute("errorAll3");
            List<String> errorAll4 = (List<String>) request.getAttribute("errorAll4");
            String texted = (String) request.getAttribute("text");
            String err = "No infor";
        %>
        <form action="testrepeatword" method="post">
            <input type="text" name="text1" 
            <%if(texted!=null &&!texted.equals("")){%>
            value="<%=texted%>"
            <%}else{%>
            value=""
            <%}%>/>
            <br/>
            <input type="submit" value="Save"/></form>
            
              
 <div class="dropdown">
    <% if(errorG == null)
    {
        }
    else{%>
    <button onclick="myFunction1()" class="dropbtn">Frequent 1-Word Phrases</button>
    <div id="myDropdown1" class="dropdown-content">
        <% for (int i = 0; i < errorG.size(); i++) { %>
            <a><%=errorG.get(i)%></a>
        <%}%>
    <div class="col-sm-3"">
    <table style="border: 1px solid black; border-collapse: collapse;">
                <tr>
                    <th>Frequent 1-Word Phrases</th>
                </tr>
    <%
 if (errorAll!= null) { %>
        <% for (int i = 0; i < errorAll.size(); i++) { %>
            <tr style="border: 1px solid black; border-collapse: collapse;">
                <td style="border: 1px solid black; border-collapse: collapse;"><p><%= errorAll.get(i) %></p></td>
                <td style="border: 1px solid black; border-collapse: collapse;"><p>
                    <% if ( suggestG.get(i)!=null) { %>
                        <%= suggestG.get(i) %>
                       
                    <% } else if(suggestG.get(i).equals("")) { %>
                        <!-- no suggestion for this rror -->
                    <% } %>
                </p></td>
            </tr>
        <% } %>
    <% } %>
</table>
</div>
</div></div>  
        <%}%>
 
  
<div class="dropdown">
    <%if(errorG2==null){ }else{%>
    <button onclick="myFunction2()" class="dropbtn">Frequent 2-Word Phrases</button>
    <div id="myDropdown2" class="dropdown-content">
        <% for (int i = 0; i < errorG2.size(); i++) { %>
            <a><%=errorG2.get(i)%></a>
        <%}%>
    <div class="col-sm-3"">  
        <table style="border: 1px solid black; border-collapse: collapse;">
                <tr>
                    <th>Frequent 2-Word Phrases</th>
                </tr>
    <% if (errorAll2!= null) { %>
        <% for (int i = 0; i < errorAll2.size(); i++) { %>
            <tr style="border: 1px solid black; border-collapse: collapse;">
                <td style="border: 1px solid black; border-collapse: collapse;"><p><%= errorAll2.get(i) %></p></td>
                <td style="border: 1px solid black; border-collapse: collapse;"><p>
                    <% if ( suggestG.get(i)!=null) { %>
                        <%= suggestG.get(i) %>
                       
                    <% } else if(suggestG.get(i).equals("")) { %>
                        <!-- no suggestion for this rror -->
                    <% } %>
                </p></td>
            </tr>
        <% } %>
    <% } %>
</table></div>
            </div>
</div><%}%>

   
<div class="dropdown">
    <%if(errorG3==null){ }else{%>
    <button onclick="myFunction3()" class="dropbtn">Frequent 3-Word Phrases</button>
    <div id="myDropdown3" class="dropdown-content">
        <% for (int i = 0; i < errorG3.size(); i++) { %>
            <a><%=errorG3.get(i)%></a>
        <%}%>
        <div class="col-sm-3""> 
    <table style="border: 1px solid black; border-collapse: collapse;">
                <tr>
                    <th>Frequent 3-Word Phrases</th>
                </tr>
    <% if (errorAll3!= null) { %>
        <% for (int i = 0; i < errorAll3.size(); i++) { %>
            <tr style="border: 1px solid black; border-collapse: collapse;">
                <td style="border: 1px solid black; border-collapse: collapse;"><p><%= errorAll3.get(i) %></p></td>
                <td style="border: 1px solid black; border-collapse: collapse;"><p>
                    <% if ( suggestG.get(i)!=null) { %>
                        <%= suggestG.get(i) %>
                       
                    <% } else if(suggestG.get(i).equals("")) { %>
                        <!-- no suggestion for this rror -->
                    <% } %>
                </p></td>
            </tr>
        <% } %>
    <% } %>
</table></div>
</div>
</div><%}%>

 
<div class="dropdown">
    <%if(errorG4==null){ }else{%>
    <button onclick="myFunction4()" class="dropbtn">Frequent 4-Word Phrases</button>
    <div id="myDropdown4" class="dropdown-content">
        <% for (int i = 0; i < errorG4.size(); i++) { %>
            <a><%=errorG4.get(i)%></a>
        <%}%>
        <div class="col-sm-3"">   
   <table style="border: 1px solid black; border-collapse: collapse;">
                <tr>
                    <th>Frequent 2-Word Phrases</th>
                </tr>
    <% if (errorAll4!= null) { %>
        <% for (int i = 0; i < errorAll4.size(); i++) { %>
            <tr style="border: 1px solid black; border-collapse: collapse;">
                <td style="border: 1px solid black; border-collapse: collapse;"><p><%= errorAll4.get(i) %></p></td>
                <td style="border: 1px solid black; border-collapse: collapse;"><p>
                    <% if ( suggestG.get(i)!=null) { %>
                        <%= suggestG.get(i) %>
                       
                    <% } else if(suggestG.get(i).equals("")) { %>
                        <!-- no suggestion for this rror -->
                    <% } %>
                </p></td>
            </tr>
        <% } %>
    <% } %>
</table>  </div></div>
</div><%}%>
           

        
    </body>
</html>
