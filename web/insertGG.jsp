<%-- 
    Document   : insertFaq
    Created on : Jul 14, 2024, 10:36:55 AM
    Author     : admin
--%>
<%@page import="models.*"%>
<%@page import="dal.*"%>

<%@page import="java.util.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Insert Qandatopic</title>
        <style>
            body {
                font-family: Arial, sans-serif;
            }
            form {
                width: 1000px;
                margin: 0 auto;
            }
            label {
                display: block;
                margin-bottom: 5px;
            }
            input[type="text"] {
                width: 100%;
                padding: 5px;
                margin-bottom: 10px;
            }
            input[type="submit"] {
                width: 100%;
                padding: 5px;
                background-color: #007A7A;
                color: white;
                border: none;
                cursor: pointer;
                height: 50px; /* Điều chỉnh chiều cao ở đây */
            }

            input[type="submit"]:hover {
                background-color: #005959;
            }

            select {
                width: 100%;
                padding: 5px;
                margin-bottom: 10px;
            }
        </style>
    </head>
    <%
        QandaDAO qdao = new QandaDAO();
        TopictypeDAO tdao = new TopictypeDAO();
        List<Topictype> listtop = tdao.getTopicTypesByType("g");
    %>
    <body>  <%
       Account x = (Account) session.getAttribute("us");
       if(x!=null&& x.getRole_id()==0)
       {
        %>

       

        <h2 style="text-align: center;">Insert Grammar Guide</h2>
        <form action="manageGG" method="post">


            <label for="question">Question:</label>
            <input type="text" id="question" name="question" required>

            <label for="content">Content:</label>
            <textarea name="content" rows="10" cols="135"></textarea><br>



            <label for="idTopic">Topic:</label>
            <select id="idTopic" name="idTopic" required>
                <option value="">Select a topic</option>
                <% if (listtop != null) { 
                for (Topictype topic : listtop) { %>
                <option value="<%= topic.getIdtopic() %>"><%= topic.getTopic() %></option>
                <% } 
            } %>
            </select>

            <input type="submit" value="Add">
        </form>


        <h2 style="text-align: center;">Add Kind of Grammar Guide</h2>
        <form action="manageGG" method="get">
            <label for="question">Name of Kind GG</label>
            <input type="text"  name="topic" required>
            <input type="submit" value="ADD">
        </form>
        <%}%>
    </body>
</html>
