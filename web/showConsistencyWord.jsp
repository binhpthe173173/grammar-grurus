<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="Css/menu.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link rel="stylesheet" type="text/css" href="Css/style.css?v=1.0">
        <style>
            span{
                border: none; text-decoration: underline;border-bottom: 2px solid blue;
            }
            .dropdown-content {
            display: none;
            text-align: center;
            position: fixed;
            background-color: #f9f9f9;
            min-width: 350px;
            background: #9AECDB;
            z-index: 1;
            border: 1px solid;
            border-radius: 5px;
        }

        .pwa-selector:hover .dropdown-content {
            display: block;
        }

    .dropdown-content div {
        padding: 8px 12px;
    }

    .error-button {
        position: relative;
    }

    .suggestion-item {
        cursor: pointer;
    }

    .suggestion-item:hover {
        background-color: #f1f1f1;
    }
    .dropdown, .dropdown1,.dropdown2,.dropdown3,.dropdown4,.dropdown5,.dropdown6,.dropdown7,.dropdown8,.dropdown9.dropdown10 {
                margin-bottom: 5px;
            }
            .dropdown-menu {
                position: static;
                box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
                border-radius: 5px;
                width: 100%;
            }
            .dropdown.open .dropdown-menu {
                display: block;
                position: absolute;
                top: 100%;
                left: 0;
                z-index: 1000;

            }
            .btn-primary {
                background-color: #f5f5f5;
                color: #333;
                border: 1px solid #ddd;
            }
            .btn-primary:hover, .btn-primary:focus, .btn-primary:active {
                background-color: #e6e6e6;
                color: #333;
            }
            .caret {
                margin-left: 10px;
            }
            .dropdown-menu > li > a {
                padding: 10px 20px;
                color: #333;
            }
            .dropdown-menu > li > a:hover {
                background-color: #e6e6e6;
                color: #333;
            }
            .btn-primary {
                background-color: #00BFA6;
                padding: 3px 5px;
                color: #fff;
                text-transform: uppercase;
                letter-spacing: 2px;
                cursor: pointer;
                border-radius: 5px;
                border: 2px dashed #00BFA6;
                transition: .4s;
            }



            .btn-primary:hover {
                transition: .4s;
                border: 2px dashed #00BFA6;
                background-color: #fff;
                color: #00BFA6;

            }

            .btn-primary:active {
                background-color: #87dbd0;
            }
            .Linkbutton{
                border: none;
                background: none;
            }
                        .Linkbutton:hover{
                border: none;
                background: none;
            }
        </style>
        <script>
            function submitForm(action) {
                var form = document.getElementById('myForm');
                var input = document.querySelector('input[name="inputText"]');
                if (input.value.trim() === "") {
                    alert("Please enter text before submitting.");
                    return false;
                }
                form.action = action;
                form.submit();
            }

            $(document).ready(function () {
                $('.dropdown').on('show.bs.dropdown', function () {
                    var $dropdowns = $('.dropdown');
                    var $drodowns1 = $('.dropdown1');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown').on('hide.bs.dropdown', function () {
                    $('.dropdown').css('margin-top', 0);
                });
            });
            function showUploadModal() {
            $('#uploadModal').modal('show');
        }

        function handleFileUpload() {
            var fileInput = document.getElementById('fileInput');
            if (fileInput.files.length === 0) {
                alert("Please select a file before uploading.");
                return false;
            }
            var form = document.getElementById('fileUploadForm');
            form.submit();
        }
            function handleNonSubscribe() {
                window.alert("You have to subscribe to use this function");
            }
       
        $(document).ready(function () {
                $('.dropdown1').on('show.bs.dropdown1', function () {
                    var $dropdowns = $('.dropdown1');
                    var $drodowns1 = $('.dropdown2');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown1').on('hide.bs.dropdown1', function () {
                    $('.dropdown2').css('margin-top', 0);
                });
            });
            $(document).ready(function () {
                $('.dropdown2').on('show.bs.dropdown2', function () {
                    var $dropdowns = $('.dropdown2');
                    var $drodowns1 = $('.dropdown3');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown2').on('hide.bs.dropdown2', function () {
                    $('.dropdown3').css('margin-top', 0);
                });
            });
            $(document).ready(function () {
                $('.dropdown3').on('show.bs.dropdown4', function () {
                    var $dropdowns = $('.dropdown3');
                    var $drodowns1 = $('.dropdown4');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown3').on('hide.bs.dropdown3', function () {
                    $('.dropdown4').css('margin-top', 0);
                });
            });
            $(document).ready(function () {
                $('.dropdown4').on('show.bs.dropdown4', function () {
                    var $dropdowns = $('.dropdown4');
                    var $drodowns1 = $('.dropdown5');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown4').on('hide.bs.dropdown4', function () {
                    $('.dropdown5').css('margin-top', 0);
                });
            });
            $(document).ready(function () {
                $('.dropdown6').on('show.bs.dropdown6', function () {
                    var $dropdowns = $('.dropdown6');
                    var $drodowns1 = $('.dropdown7');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown6').on('hide.bs.dropdown6', function () {
                    $('.dropdown7').css('margin-top', 0);
                });
            });
            $(document).ready(function () {
                $('.dropdown7').on('show.bs.dropdown7', function () {
                    var $dropdowns = $('.dropdown7');
                    var $drodowns1 = $('.dropdown8');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown7').on('hide.bs.dropdown7', function () {
                    $('.dropdown8').css('margin-top', 0);
                });
            });
            $(document).ready(function () {
                $('.dropdown8').on('show.bs.dropdown8', function () {
                    var $dropdowns = $('.dropdown8');
                    var $drodowns1 = $('.dropdown9');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown8').on('hide.bs.dropdown8', function () {
                    $('.dropdown9').css('margin-top', 0);
                });
            });
            $(document).ready(function () {
                $('.dropdown9').on('show.bs.dropdown9', function () {
                    var $dropdowns = $('.dropdown9');
                    var $drodowns1 = $('.dropdown10');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown9').on('hide.bs.dropdown9', function () {
                    $('.dropdown10').css('margin-top', 0);
                });
            });
            
        document.addEventListener('DOMContentLoaded', function () {
  const pwaSpans = document.querySelectorAll('.pwa');

  pwaSpans.forEach(span => {
    const titleText = span.getAttribute('title');
    const dropdown = document.createElement('div');
    dropdown.classList.add('dropdown-content');
    dropdown.style.display = 'none';

    const title = document.createElement('div');
    title.textContent = titleText;
    dropdown.appendChild(title);

    span.appendChild(dropdown);

    span.addEventListener('mouseenter', function () {
      dropdown.style.display = 'block';
    });

    span.addEventListener('mouseleave', function () {
      dropdown.style.display = 'none';
    });
  });
});
        </script>
    </head>
    <body>
        
        <div class="container"><br/>
            <div class="menubar">
            <div class="flexde">
        <% 
        List<String> error = (List<String>) request.getAttribute("error");
        %>

       <aside class="sidebar">
            <ul>
                <jsp:include page="sideBarUser.jsp" />
            </ul>
        </aside>
        <br/>
        <main class="editor-container"><form class="form-class" id="myForm" method="post">
                <div class="button-group">
                                <button class="toolbar-button" type="button" onclick="submitForm('grammarFunction')">Grammar Checking <i class="fas fa-spell-check"></i> </button>
                                <button class="toolbar-button" type="button" onclick="submitForm('spellFunction')">Spell Checking <i class="fas fa-spell-check"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('sticky')">Sticky <i class="fas fa-lock"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('repeatFunction')">All Repeat <i class="fas fa-redo"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('cliche')">Cliche <i class="fas fa-pen-alt"></i></button>
                                <c:if test="${ac.role_id==1}">
                                    <button class="toolbar-button" type="button" onclick="handleNonSubscribe()">Overview <i class="fas fa-pen-alt"></i></button>
                                </c:if>
                                <c:if test="${ac.role_id==2 || ac.role_id==0}">
                                    <button class="toolbar-button" type="button" onclick="submitForm('overview')">Overview <i class="fas fa-pen-alt"></i></button>
                                </c:if>
                                <button class="toolbar-button" type="button" onclick="submitForm('alliterationFunction')">Alliteration <i class="fas fa-music"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('allsentencesFunction')">Length <i class="fas fa-ruler-horizontal"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('complexWordText')">Complex Word <i class="fas fa-comment-alt"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('consistencyText')">Consistency Word <i class="fas fa-align-center"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('Text2DVA')">DVA <i class="fas fa-chart-bar"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('eadabilityTextResult')">Readability <i class="fas fa-book-open"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('transitionsTextEr')">Transitions Check <i class="fas fa-arrows-alt-h"></i></button>
                                <c:if test="${ac.role_id==1}">
                                    <button class="toolbar-button" type="button" onclick="handleNonSubscribe()">Upload File <i class="fas fa-file-upload"></i></button>
                                </c:if>
                                <c:if test="${ac.role_id==2 || ac.role_id==0}">
                                    <button class="toolbar-button" type="button" onclick="showUploadModal()">Upload File <i class="fas fa-file-upload"></i></button>
                                </c:if>
                            </div>
                <input type="hidden" class="text-confix" name="inputText" value="${inputText}"/>
            </form>
            <div class="text-decoration" >${text1}</div><br/>
        </main>
        <aside class="editor-notes">
                    <% if (error != null) { %><% for (int i = 0; i < error.size(); i++) { %>
                    <div class="dropdown<%=i+1%>">
                        
                        <button class="btn-primary dropdown-toggle" type="button" data-toggle="dropdown<%=i+1%>"><%= error.get(i) %><br/>
                        <% List<String> E = (List<String>) request.getAttribute("error" + (i + 1)); %>
                        <% if (E != null || !E.isEmpty()) { %>
                        
                            <% for (int j = 0; j < E.size(); j++) { %>
                            <div style="background-color:#9AECDB"><%= E.get(j) %></div><br>
                            
                            <% } %>
                        <% } %>
                        </button>
                    </div><br/>
                    <% } %><% } %>
                </aside>
</div>
        </div>
         <!-- Modal for File Upload -->
    <div id="uploadModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload File</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body upload-modal-body">
                    <form id="fileUploadForm" method="post" action="fileUpload" enctype="multipart/form-data">
                        <input type="file" id="fileInput" name="file" accept=".docx" />
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="handleFileUpload()">Upload</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
