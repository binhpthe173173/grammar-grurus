<%-- 
    Document   : login
    Created on : Feb 22, 2024, 12:09:42 AM
    Author     : mactu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="Css/login.css">
        <title>Login</title>
    </head>
    <body> 
            
            <header class="header">
            
            <div class="search" />
        </header>
        <div class="background"></div>
        <section class="home">
            <div class="content">
                <h2> Welcome!</h2>
                <h3> Grammar Gurus </h3>
                <pre> Thank you for coming to us! </pre>
                
            </div>
            
            <div class="login">
                <nav class="nav">
                    <a href="homeGuest.jsp">Home</a>
            </nav>
                <h2> Login </h2>
                
                <form action="login" method="post">
                    <div class="input">
                        <input type="text" class="input1" name="username" placeholder="Username or Email" required  >
                    </div>
                    <div class="input">
                        <input type="password" class="input1" name="password" placeholder="Password" required>
                    </div>
                    <div class="check">
                        <a href="forgot.jsp"> Forgot Password?</a>
                    </div>
                    <div class="button">
                        <button class="btn" type="submit"> Login </button>
                    </div>
                </form>
                <div class="sign-up">
                    <p> Don't have an account? </p>
                    <a href="register.jsp"> Register</a>
                </div>
                <div>
                    <a href="https://accounts.google.com/o/oauth2/auth?scope=profile+email&redirect_uri=http://localhost:8080/GrammarGurusv0.0/login&response_type=code
                       &client_id=558703980156-m02ntfo1997h2tfpij4f9g7c16augou2.apps.googleusercontent.com&approval_prompt=force">
                        <img src="https://developers.google.com/identity/images/btn_google_signin_dark_normal_web.png" alt="Sign in with Google"/>
                    </a>
                    <div style="color: red">${err}</div>
                </div>
            </div>
        </section>
    </body>
</html>
