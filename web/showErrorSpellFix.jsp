<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Grammar and Spelling check</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="Css/menu.css">
        <link rel="stylesheet" type="text/css" href="Css/style.css?v=1.0">
    </head>
    <body>
        <style>
    .dropdown-content {
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        border: 1px solid #ccc;
        padding: 10px;
    }

    .dropdown-content div {
        padding: 8px 12px;
    }

    .error-button {
        position: relative;
    }

    .suggestion-item {
        cursor: pointer;
    }

    .suggestion-item:hover {
        background-color: #f1f1f1;
    }
</style>


        <script>
            function submitForm(action) {
                var form = document.getElementById('myForm');
                var input = document.querySelector('input[name="inputText"]');
                if (input.value.trim() === "") {
                    alert("Please enter text before submitting.");
                    return false;
                }
                form.action = action;
                form.submit();
            }

            document.addEventListener('DOMContentLoaded', function () {
    const buttons = document.querySelectorAll('.error-button');

    buttons.forEach(button => {
        const dropdown = document.createElement('div');
        dropdown.classList.add('dropdown-content');
        dropdown.style.display = 'none';

        const error = document.createElement('div');
        error.textContent = 'Error: ' + button.getAttribute('data-error');
        dropdown.appendChild(error);

        const suggestions = button.getAttribute('data-suggestions').split(', ');
        suggestions.forEach(suggestion => {
            const suggestionItem = document.createElement('div');
            suggestionItem.textContent = suggestion;
            suggestionItem.classList.add('suggestion-item');
            suggestionItem.addEventListener('click', function () {
                const form = document.createElement('form');
                form.action = 'subFixError';
                form.method = 'get';

                const errorInput = document.createElement('input');
                errorInput.type = 'hidden';
                errorInput.name = 'wordError';
                errorInput.value = button.getAttribute('Errorfix');

                const suggestionInput = document.createElement('input');
                suggestionInput.type = 'hidden';
                suggestionInput.name = 'wordSuggest';
                suggestionInput.value = suggestion;
                form.appendChild(errorInput);
                form.appendChild(suggestionInput);

                document.body.appendChild(form);
                form.submit();
            });
            dropdown.appendChild(suggestionItem);
        });

        button.appendChild(dropdown);

        button.addEventListener('mouseenter', function () {
            dropdown.style.display = 'block';
        });

        button.addEventListener('mouseleave', function () {
            dropdown.style.display = 'none';
        });
    });
});

window.onclick = function (event) {
    if (!event.target.matches('.error-button') && !event.target.matches('.suggestion-item')) {
        const dropdowns = document.querySelectorAll('.dropdown-content');
        dropdowns.forEach(dropdown => {
            if (dropdown.style.display === 'block') {
                dropdown.style.display = 'none';
            }
        });
    }
};
function showUploadModal() {
            $('#uploadModal').modal('show');
        }

        function handleFileUpload() {
            var fileInput = document.getElementById('fileInput');
            if (fileInput.files.length === 0) {
                alert("Please select a file before uploading.");
                return false;
            }
            var form = document.getElementById('fileUploadForm');
            form.submit();
        }
        function handleNonSubscribe() {
                window.alert("You have to subscribe to use this function");
            }
        </script>

        <div class="container"><br/>
            <div class="menubar"></div>
            <div class="flexde">
                <% 
                List<String> errorGrammar = (List<String>) request.getAttribute("error");
                List<String> suggestGrammar = (List<String>) request.getAttribute("suggestion");
                String texted = (String) request.getAttribute("text1");
                String error =  (String) request.getAttribute("score");
                String fixError = (String) request.getAttribute("fixText");
                String word = null;
               
                %>

                <aside class="sidebar">
                      <jsp:include page="sideBarUser.jsp" />
                </aside>
                <main class="editor-container">
                    <form class="form-class" id="myForm" method="post">
                        <div class="button-group">
                                <button class="toolbar-button" type="button" onclick="submitForm('grammarFunction')">Grammar Checking <i class="fas fa-spell-check"></i> </button>
                                <button class="toolbar-button" type="button" onclick="submitForm('spellFunction')">Spell Checking <i class="fas fa-spell-check"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('sticky')">Sticky <i class="fas fa-lock"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('repeatFunction')">All Repeat <i class="fas fa-redo"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('cliche')">Cliche <i class="fas fa-pen-alt"></i></button>
                                <c:if test="${ac.role_id==1}">
                                    <button class="toolbar-button" type="button" onclick="handleNonSubscribe()">Overview <i class="fas fa-pen-alt"></i></button>
                                </c:if>
                                <c:if test="${ac.role_id==2 || ac.role_id==0}">
                                    <button class="toolbar-button" type="button" onclick="submitForm('overview')">Overview <i class="fas fa-pen-alt"></i></button>
                                </c:if>
                                <button class="toolbar-button" type="button" onclick="submitForm('alliterationFunction')">Alliteration <i class="fas fa-music"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('allsentencesFunction')">Length <i class="fas fa-ruler-horizontal"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('complexWordText')">Complex Word <i class="fas fa-comment-alt"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('consistencyText')">Consistency Word <i class="fas fa-align-center"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('Text2DVA')">DVA <i class="fas fa-chart-bar"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('eadabilityTextResult')">Readability <i class="fas fa-book-open"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('transitionsTextEr')">Transitions Check <i class="fas fa-arrows-alt-h"></i></button>
                                <c:if test="${ac.role_id==1}">
                                    <button class="toolbar-button" type="button" onclick="handleNonSubscribe()">Upload File <i class="fas fa-file-upload"></i></button>
                                </c:if>
                                <c:if test="${ac.role_id==2 || ac.role_id==0}">
                                    <button class="toolbar-button" type="button" onclick="showUploadModal()">Upload File <i class="fas fa-file-upload"></i></button>
                                </c:if>
                            </div>

                        <input type="hidden" class="text-confix" name="inputText" value="${inputText}"/>
                    </form>
                    <br/><%if(texted!=null && fixError==null){%>
                    <div class="text-decoration" >${text1}</div><br/>
                    <form action="fixErrorSpell" method="post">
                        <input type="hidden" value="${inputText}" name="inputText"/>
                        <input type="hidden" value="${text1}" name="text1"/>
                        <input class="toolbar-button" type="submit" value="Fix all Error"/>
                    </form><%}else if(texted!=null && fixError!=null){%><br/>
                    <div class="flex_box">
                        <div class="text-decoration" > <div class="header-text">Old</div><br/> ${text1}</div>
                        <div class="text-decoration"><div class="header-text">New</div><br/> ${fixText}</div></div><%}%>
                </main>
                <aside class="editor-notes" >
                    <% if (errorGrammar != null && suggestGrammar != null) { %>

                    <div class="notes-header"><strong>Error</strong>--------<strong>Suggestions</strong></div>

                    <% for (int i = 0; i < errorGrammar.size(); i++) {
                     String word2 = "";
                    %>
                    <div class="note" ><%= errorGrammar.get(i) %>-><%= suggestGrammar.get(i) %>

                        <%String[] parts = errorGrammar.get(i).split(":");
                            if (parts.length > 1) {
                            String[] subParts = parts[1].split("->");
                            word = subParts[0];}
                        %>
                        <%if(texted!=null && fixError==null){%>
                        <%
                             String[] suggests = suggestGrammar.get(i).split("|");
                            for(int j=1;j<=suggests.length;j++){
                            if(!suggests[j].equals("|")){
                           word2 =word2+suggests[j]; 
                            }else{
                            break;
                            }
                            }
                        %>
                        <form action="subFixError" method="get">
                            <input type="hidden" value="${inputText}" name="inputText"/>
                            <input type="hidden" value="<%=word%>" name="wordError"/>
                            <input type="hidden" value="<%=word2%>" name="wordSuggest"/>
                            <input class="btn" type="submit"/>
                        </form><%}else if(texted!=null && fixError!=null){%><%
                            if(error==null){
                             String[] suggests = suggestGrammar.get(i).split("|");
                            for(int j=1;j<=suggests.length;j++){
                            if(!suggests[j].equals("|")){
                           word2 =word2+suggests[j]; 
                            }else{
                            break;
                            }
                            }
                        %>
                        <form action="fixErrorAndSubFix" method="get">
                            <input type="hidden" value="${inputText}" name="inputText"/>
                            <input type="hidden" value="<%=word%>" name="wordError"/>
                            <input type="hidden" value="<%=word2%>" name="wordSuggest"/>
                            <input class="btn" type="submit"/>
                        </form><%}%><%}%>
                    </div><%}%>

                    <% } %>

                </aside>

            </div>
        </div>

 <!-- Modal for File Upload -->
    <div id="uploadModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload File</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body upload-modal-body">
                    <form id="fileUploadForm" method="post" action="fileUpload" enctype="multipart/form-data">
                        <input type="file" id="fileInput" name="file" accept=".docx" />
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="handleFileUpload()">Upload</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>