<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Subscription Success</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                line-height: 1.6;
                padding: 20px;
                background-color: #f4f4f9;
                text-align: center;
            }
            h1 {
                color: green;
            }
            a {
                margin-right: 10px; 
                display: inline-block;
            }
        </style>
    </head>
    <body>
        <h1>Subscription Successful!</h1>
        <p>Your subscription has been processed successfully.</p>
        <a href="subscriptions">Back to Subscriptions</a>
        <a href="./grammarFunction">Back to Home Page</a>
    </body>
</html>
