<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Accounts List</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <style>
            body {
                font-family: Arial, sans-serif;
                line-height: 1.6;
                padding: 0px;
                display: flex;
            }
            .sidebar {
                height: 100vh;
                position: fixed;
                padding: 24px;
                background-color: #9AECDB;
            }
            .sidebar-brand {
                text-align: center;
                margin-bottom: 20px;
            }
            .sidebar-brand h2 {
                margin: 0;
                font-size: 28px;
                color: #9AECDB;
            }
            .main {
                margin-left: 200px;
                padding: 24px;
            }
            .action-container {
                display: flex;
                justify-content: space-between;
                align-items: center;
                margin-bottom: 20px;
            }
            .search-input {
                flex: 1;
                padding: 10px;
                border: 2px solid #ced4da;
                border-radius: 4px;
                box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
                transition: border-color 0.3s, box-shadow 0.3s;
                margin-right: 10px;
            }
            .search-input:focus {
                border-color: #80bdff;
                outline: 0;
                box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(128, 189, 255, 0.6);
            }
            .btn-primary {
                padding: 10px 20px;
            }
            .btn-danger{
                padding: 10px 20px;
            }
            .filter-container {
                display: flex;
                align-items: center;
                margin-bottom: 20px;
            }
            .filter-container label {
                margin-right: 10px;
            }
        </style>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            function applyFilters() {
                var textS = document.getElementById("search-input").value;
                var activeStatus = document.querySelector('input[name="active-status"]:checked').value;
                var roleId = document.querySelector('input[name="role-id"]:checked').value;

                $.ajax({
                    url: "${pageContext.request.contextPath}/searchByAjax",
                    type: "get",
                    data: {
                        text: textS,
                        activeStatus: activeStatus,
                        roleId: roleId
                    },
                    success: function (data) {
                        var row = document.getElementById("content");
                        row.innerHTML = data;
                    },
                    error: function (xhr) {
                        console.log("An error occurred: " + xhr.status + " " + xhr.statusText);
                    }
                });
            }
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar" style="padding-left: 24px;padding-right: 24px;">
                    <%@ include file="sidebar.jsp" %>
                </nav>
                
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 bg-light main">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<%= request.getContextPath() %>/Admin/homePage">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">List Account</li>
                        </ol>
                    </nav>
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Accounts List</h1>
                    </div>
                    <div class="action-container">
                        <input id="search-input" oninput="applyFilters()" type="text" name="text" placeholder="Search by Username" class="search-input">
                        <a class="btn btn-primary" href="addAccount">Add New Account</a>
                    </div>
                    <div class="filter-container">
                        <label>Active Status:</label>
                        <label>
                            <input type="radio" name="active-status" value="all" checked onclick="applyFilters()"> All
                        </label>
                        <label>
                            <input type="radio" name="active-status" value="true" onclick="applyFilters()"> Active
                        </label>
                        <label>
                            <input type="radio" name="active-status" value="false" onclick="applyFilters()"> Inactive
                        </label>

                        <label style="margin-left: 20px;">Role ID:</label>
                        <label>
                            <input type="radio" name="role-id" value="all" checked onclick="applyFilters()"> All
                        </label>
                        <label>
                            <input type="radio" name="role-id" value="1" onclick="applyFilters()"> User
                        </label>
                        <label>
                            <input type="radio" name="role-id" value="0" onclick="applyFilters()"> Admin
                        </label>
                        <!-- Add more role IDs as needed -->
                    </div>

                    <c:if test="${not empty errorMessage}">
                        <div class="alert alert-danger">${errorMessage}</div>
                    </c:if>

                    <div id="content">
                        <table class="table table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th>ID</th>
                                    <th>Username</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Image</th>
                                    <th>Time Created</th>
                                    <th>Active Status</th>
                                    <th>Role ID</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="account" items="${accounts}">
                                    <tr>
                                        <td>${account.id}</td>
                                        <td>${account.username}</td>
                                        <td>${account.full_name}</td>
                                        <td>${account.email}</td>
                                        <td><img src="${pageContext.request.contextPath}/${account.image}" alt="User Image" width="50" height="50"/></td>
                                        <td>${account.time_created}</td>
                                        <td>${account.active_status}</td>
                                        <td>${account.role_id}</td>
                                        <td>
                                            <a class="btn btn-sm btn-primary" href="updateAccount?id=${account.id}">Edit</a>
                                            <a class="btn btn-sm btn-danger" href="deleteAccount?id=${account.id}" onclick="return confirm('Are you sure you want to delete this account?');">Delete</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>   
                    <nav>
                        <ul class="pagination">
                            <c:forEach begin="1" end="${numberPage}" var="i">
                                <li class="page-item ${i == currentPage ? 'active' : ''}">
                                    <a class="page-link" href="listUser?page=${i}">${i}</a>
                                </li>
                            </c:forEach>
                        </ul>
                    </nav>
                </main>
            </div>
        </div>
    </body>
</html>
