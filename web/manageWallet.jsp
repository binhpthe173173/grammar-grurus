<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="models.*" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Manage Wallets</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>
            body {
                font-family: Arial, sans-serif;
                padding: 0px;
                background-color: #f8f9fa;
            }
            .table-container {
                display: flex;
                flex-wrap: wrap;
            }
            .sidebar {
                color: #fff;
                padding: 15px;
                border-radius: 4px;
                margin-right: 20px;
                height: 100%;
                position: fixed;
                width: 250px;
            }
            .main-content {
                margin-left: 300px;
                padding: 20px;
                background-color: #fff;
                border-radius: 4px;
                flex-grow: 1;
            }
            table {
                width: 100%;
                border-collapse: collapse;
            }
            .table {
                margin-bottom: 0;
            }
            th, td {
                text-align: left;
            }
            th {
                width: 15%;
            }
            button {
                padding: 8px 12px;
                cursor: pointer;
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 4px;
                margin-top: 10px;
                width: 100px;
            }
            button:hover {
                background-color: #0056b3;
            }
            a {
                text-decoration: none;
                color: #fff;
            }
            .sidebar {
                height: 100vh;
                position: fixed;
                padding: 24px;
                background-color: #9AECDB;
            }
            .sidebar-brand {
                text-align: center;
                margin-bottom: 20px;
            }
            .sidebar-brand h2 {
                margin: 0;
                font-size: 28px;
                color: #9AECDB;
            }
            .main {
                margin-left: 200px;
                padding: 24px;
            }

            
            .breadcrumb a {
                text-decoration: none;
                color: #007bff;
            }
            .table thead th {
                background-color: #007bff;
                color: #fff;
            }
            .table tbody tr:nth-child(even) {
                background-color: #f2f2f2;
            }
        </style>

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar" style="padding-left: 24px;padding-right: 24px;">
                    <%@ include file="sidebar.jsp" %>
                </nav>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 bg-light main">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<%= request.getContextPath() %>/Admin/homePage">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Wallet</li>
                    </ol>
                </nav>
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 style="text-align: center">Manage Wallets</h1>
                        </div>
                <form class="form-inline mb-3" action="" method="get">
                    <input type="text" name="q" class="form-control mr-2" placeholder="Search..." />
                    <button type="submit" class="btn btn-primary">Search</button>
                </form>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th>ID</th>
                                <th>Account ID</th>
                                <th>Username</th>
                                <th>Full Name</th>
                                <th>Balance</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="wallet" items="${wallets}">
                                <c:set var="account" value="${accountMap[wallet.id]}" />
                                <c:if test="${account != null}">
                                    <tr>
                                        <td>${wallet.id}</td>
                                        <td>${wallet.account_id}</td>
                                        <td>${account.username}</td>
                                        <td>${account.full_name}</td>
                                        <td>${wallet.balance}</td>
                                        <td><button><a href="addFund?id=${wallet.account_id}">Add Fund</a></button></td>
                                    </tr>
                                </c:if>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </main>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </body>
</html>
