<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Edit Package</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            padding: 20px;
        }
        h1, h2 {
            color: #333;
        }
        form {
            max-width: 600px;
            margin-top: 20px;
        }
        form div {
            margin-bottom: 10px;
        }
        form label {
            display: block;
            margin-bottom: 5px;
            color: #333;
        }
        form input[type="text"],
        form input[type="number"],
        form textarea {
            width: 100%;
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 4px;
            font-size: 16px;
        }
        form button {
            padding: 8px 12px;
            cursor: pointer;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 4px;
            margin-top: 10px;
        }
        form button:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <h1>Edit Subscription</h1>
    <form action="" method="post">
        <div>
            <label for="subscriptionName">Name:</label>
            <input type="text" id="subscriptionName" name="name" value="${sub.name}" required>
        </div>
        <div>
            <label for="subscriptionPrice">Price (VND):</label>
            <input type="number" id="subscriptionPrice" name="price" value="${sub.price}" min="0" required>
        </div>
        <div>
            <label for="subscriptionDuration">Duration (in days):</label>
            <input type="number" id="subscriptionDuration" name="duration" value="${sub.duration}" min="1" required>
        </div>
        <div>
            <label for="subscription-status">Status:</label>
            <select id="subscription-status" name="status" required>
                <option value="1" ${sub.status?'selected':''}>Actived</option>
                <option value="0" ${sub.status?'':'selected'}>Inactived</option>
            </select>
        </div>
        <div>
            <label for="subscriptionDescription">Description:</label>
            <textarea id="subscriptionDescription" name="description" rows="4" required>${sub.description}</textarea>
        </div>
        <input type="hidden" name="id" value="${sub.id}">
        <button type="submit">Save Changes</button>
    </form>
</body>
</html>
