
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sidebar Example</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <style>
            
        /* CSS ??nh d?ng cho sidebar */
        body {
                font-family: Arial, sans-serif;
                line-height: 1.6;
                padding: 0px;
                display: flex;
            }
        
            /* CSS ??nh d?ng cho sidebar */
            .sidebar-sticky {
                width: 250px;
                height: 100vh;
                background-color: #2c2c2c;
                padding-top: 33px;
                position: fixed;
                box-shadow: 2px 0 5px rgba(0, 0, 0, 0.1);
                color: #fff;
            }


            .sidebar-brand h2 {
                color: #e91e63;
                text-align: center;
                margin-bottom: 30px;
            }

            .nav-link {
                display: flex;
                align-items: center;
                color: #bdbdbd;
                padding: 10px 20px;
                text-decoration: none;
                font-size: 16px;
                transition: background-color 0.3s, color 0.3s;
            }

            .nav-link .fas {
                margin-right: 10px;
                font-size: 18px;
            }

            .nav-link:hover {
                background-color: #e91e63;
                color: #fff;
            }

            .nav-link.active {
                background-color: #e91e63;
                color: #fff;
            }

            .nav-link.active .fas {
                color: #fff;
            }
        </style>

    </head>
    <body>


        <div class="sidebar-sticky" style="    right: 1423px;
                          right: 1423px;
             top: 0px;
             left: 0px
             ">
            <!-- Sidebar Brand -->
            <div class="sidebar-brand">
                <h2>Grammar Gurus</h2>
            </div>

            <ul id="adminMenu" class="nav flex-column" style="padding-left: 30px">
                <li class="nav-item">
                    <a id="dashboard-link" class="nav-link" href="<%= request.getContextPath() %>/Admin/homePage">
                        <span class="fas fa-tachometer-alt"></span>
                        Home Admin
                    </a>
                </li>
                <li class="nav-item">
                    <a id="list-account-link" class="nav-link" href="<%= request.getContextPath() %>/Admin/listUser">
                        <span class="fas fa-users"></span>
                        List Account
                    </a>
                </li>
                <li class="nav-item">
                    <a id="manager-subscription-link" class="nav-link" href="<%= request.getContextPath() %>/Admin/manageSubscription">
                        <span class="fas fa-credit-card"></span>
                        Manager Subscription
                    </a>
                </li>
                <li class="nav-item">
                        <a id="manager-wallet-link" class="nav-link" href="<%= request.getContextPath() %>/Admin/manageWallet">
                        <span class="fas fa-wallet"></span>
                        Manager Wallet
                    </a>
                </li>
                <li class="nav-item">
                        <a id="manager-wallet-link" class="nav-link" href="<%= request.getContextPath() %>/grammarGuideList">
                        <span class="fas fa-chalkboard-teacher"></span>
                        Manager Grammar Guide
                    </a>
                </li>
                
                <li class="nav-item">
                    <a id="manager-Blog-link" class="nav-link" href="<%= request.getContextPath() %>/includeBlog.jsp">
                        <span class="fas  fa-blog"></span>
                        Manager Blog
                    </a>
                </li>
                <li class="nav-item">
                    <a id="manager-Feedback-link"class="nav-link" href="<%= request.getContextPath() %>/review.jsp">
                        <span class="fas fa-star-half-alt"></span>
                        Manager Feedback
                    </a>
                </li>
                <li class="nav-item">
                    <a id="manager-FAQ-link"class="nav-link" href="<%= request.getContextPath() %>/manageFAQ.jsp">
                        <span class="fas fa-question-circle"></span>
                        Manager FAQ
                    </a>
                </li>

                <li class="nav-item">
                    <a id="manager-Transaction-link" class="nav-link" href="<%= request.getContextPath() %>/Admin/transactionHistory">
                        <span class="fas fa-history"></span>
                        Transaction History
                    </a>
                </li>
               
                <li class="nav-item">
                    <a id="logout-link" class="nav-link" href="<%= request.getContextPath() %>/logout">
                        <span class="fas fa-sign-out-alt"></span>
                        Logout
                    </a>
                </li>
                <li class="nav-item">
                    <a id="Change-link" class="nav-link" href="<%= request.getContextPath() %>/grammarFunction">
                        <span class="fas fa-user"></span>
                        Change to User
                    </a>
                </li>
            </ul>
        </div>

        
          <script>document.addEventListener("DOMContentLoaded", function() {
            // Set active class based on the current URL
            var currentUrl = window.location.pathname;
            document.querySelectorAll('.nav-link').forEach(function(link) {
                if (link.getAttribute('href') === currentUrl) {
                    link.classList.add('active');
                }
            });
        });

        

            // Event listener for dynamic activation
            document.querySelectorAll('.nav-link').forEach(function (link) {
                link.addEventListener('click', function () {
                    document.querySelectorAll('.nav-link').forEach(function (link) {
                        link.classList.remove('active');
                    });
                    this.classList.add('active');
                });
            });</script>          
    </body>
</html>
