<%@ page import="java.util.List" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="models.*"%>
<%@ page import="dal.*"%>
<style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f4f4f4;
        margin: 0;
        padding: 0;
    }
    .search-container {
        text-align: center;
        margin: 20px 0;
    }
    .search-container input[type="text"] {
        padding: 10px;
        width: 300px;
        border: 1px solid #007A7A;
        border-radius: 5px;
    }
    .search-container input[type="submit"] {
        padding: 10px 20px;
        background-color: #007A7A;
        color: white;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s;
    }
     .search-container select  {
            padding: 10px;
            width: 170px;
            border: 1px solid #007A7A;
            border-radius: 5px;
        }
    .search-container input[type="submit"]:hover {
        background-color: #005a5a;
    }
    .blog-container {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        padding: 20px;
    }
    .blog-container h4 {
        color: #007A7A;
    }
    .blog-item {
        flex: 0 0 30%;
        box-sizing: border-box;
        margin: 1.5%;
        padding: 20px;
        border: 1px solid #007A7A;
        border-radius: 10px;
        background-color: #E6F0FB;
        transition: transform 0.2s, box-shadow 0.2s;
    }
    .blog-item:hover {
        transform: translateY(-10px);
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
    }
    .blog-item h2 {
        font-size: 1.5em;
        color: #007A7A;
    }
    .blog-item p {
        color: #666;
    }
    .read-more {
        display: inline-block;
        margin-top: 5px;
        padding: 5px 5px;
        background-color: #007A7A;
        color: white;
        text-decoration: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s;
    }
    .read-more:hover {
        background-color: #005a5a;
    }
    .haha {
        text-align: center;
        margin-top: 20px;
        
    }
    .pagination {
        text-align: center;
        margin-top: 20px;
         margin-bottom: 40px;
    }
    .pagination a {
        color: #007A7A;
        text-decoration: none;
        margin: 0 5px;
        padding: 5px 10px;
        border: 1px solid #007A7A;
        border-radius: 5px;
        transition: background-color 0.3s, color 0.3s;
    }
    .pagination a:hover {
        background-color: #007A7A;
        color: #fff;
    }
    .pagination a.active {
        background-color: #007A7A;
        color: #fff;
        
    }
    body {
    font-family: Arial, sans-serif;
    background-color: #f4f4f4;
    margin: 0;
    padding: 0;
}

header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 10px 20px;
    background-color: #fff;
    border-bottom: 1px solid #ddd;
}

header .logo {
    font-size: 24px;
    font-weight: bold;
    color: #000;
    text-decoration: none;
}

header a {
    margin: 0 15px;
    color: #000;
    text-decoration: none;
    transition: color 0.3s;
}

header a:hover {
    color: #007A7A;
}

.main-content {
    text-align: center;
    padding: 50px 20px;
    background-color: #E6F0FB;
}

.main-content h1 {
    font-size: 36px;
    margin-bottom: 20px;
    color: #333;
}

.main-content p {
    font-size: 18px;
    margin-bottom: 30px;
    color: #666;
}

.main-content .cta-button {
    padding: 15px 30px;
    background-color: #007A7A;
    color: white;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    font-size: 16px;
    text-decoration: none;
    transition: background-color 0.3s;
}

.main-content .cta-button:hover {
    background-color: #005a5a;
}

.contact-section {
    text-align: center;
    padding: 50px 20px;
    background-color: #fff;
}

.contact-section h2 {
    font-size: 28px;
    margin-bottom: 20px;
    color: #333;
}

.contact-section p {
    font-size: 16px;
    color: #666;
}

.contact-section .contact-links a {
    margin: 0 10px;
    color: #007A7A;
    text-decoration: none;
    font-size: 16px;
    transition: color 0.3s;
}

.contact-section .contact-links a:hover {
    color: #005a5a;
}

</style>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Blog user</title>
    <% 
    Account x1 = (Account) session.getAttribute("us");
    DAO dao1 = new DAO();
    
    BlogDAO dao = new BlogDAO();
    
    String searchQuery = (String)request.getAttribute("search");
    String typeQuery = (String)request.getAttribute("type");
    
    List<Blog> blogs;
    
    if ((searchQuery != null || typeQuery != null)) {
    blogs = dao.searchBlogs(searchQuery, typeQuery);
    } else {
    typeQuery= request.getParameter("type");
    searchQuery = request.getParameter("search");
    if (searchQuery != null || typeQuery != null) {
    blogs = dao.searchBlogs(searchQuery, typeQuery);
        
        }
        else {
        blogs = dao.getAllBlogsByState("duyet");
    }
        }

    int pageSize = 6;
    int pageNumber = 1; 
    String pageParam = request.getParameter("page");
    if (pageParam != null) {
        try {
            pageNumber = Integer.parseInt(pageParam);
        } catch (NumberFormatException e) {
            // handle the exception
        }
    }
    int start = (pageNumber - 1) * pageSize;
    int end = Math.min(start + pageSize, blogs.size());
    int totalPages = (int) Math.ceil(blogs.size() / (double) pageSize);
%>

</head>
<body>
     
     <header>
        <div>
            <a href="#" class="logo">Grammar Gurus</a>
        </div>
        <div>
           
          <%        if(x1!=null){%>
            <a href="checkFunction.jsp">Home</a>
            <%}%>
          
          <%        if(x1==null){%>
          <a href="userBlog.jsp">Blog</a>
            <a href="listFAQ.jsp">FAQ</a> 
           
             <a href="grammarGuideList">Grammar Guide</a>
             <a href="login.jsp">Use App</a>
            <a href="register.jsp">Register</a>
            <%}%>
        </div>
        </div>
    </header>
    
    <div class="main-content">
        <h1>Knowledge is power, use it wisely.</h1>
        <p>Grammar Gurus helps you craft, polish, and elevate your writing.</p>
        
    </div>
    <div class="grammar-image">
                <img src="https://assets.prowritingaid.com/f/145420/1096x800/8598723b2d/hero_esl_featurespage.png/m/684x500/" alt="Grammar Checking">
            </div>
    
    
   <div class="search-container">
    <form action="searchBlog" method="get">
        <input type="text" name="search" placeholder="Search by title" value="<%= searchQuery != null ? searchQuery : "" %>">
        <select name="type">
            <option value="">All Types</option>
            <option value="Grammar">Grammar</option>
            <option value="Business Writing">Business Writing</option>
            <option value="Creative Writing">Creative Writing</option>
            <option value="Student Writing">Student Writing</option>
            <option value="Inspiration">Inspiration</option>
            <option value="GrammarGurus Help">GrammarGurus Help</option>
            <option value="Other">Other</option>
        </select>
        <input type="submit" value="Search">
    </form>
</div>

    
    <div class="blog-container">
        <% for (int i = start; i < end; i++) { 
            Blog blog = blogs.get(i);
        %>
            <div class="blog-item">
                <h4><%= blog.getType() %></h4>
                <h3> <a href="fullContentblog.jsp?id=<%= blog.getIdBlog() %>" style="color: #007A7A;"><%= blog.getTitle() %></a></h3>
                
                <p><%=dao1.getAccountById(blog.getIdacount()).getUsername() %></p>
                <p><%= blog.getTime_up() %></p>
                
                <% if(x1 != null &&( x1.getRole_id() == 0||x1.getId()==blog.getIdacount())) { %>
                  <h4><span class="read-more" onclick="window.location.href='confirmDeleteB.jsp?id=<%= blog.getIdBlog() %>'">DELETE</span></h4>
                <% } %>
            </div>
        <% } %>
    </div>
    
    <div class="pagination">
    <% for (int i = 1; i <= totalPages; i++) { %>
        <a href="userBlog.jsp?page=<%= i %>&search=<%= searchQuery != null ? searchQuery : "" %>&type=<%= typeQuery != null ? typeQuery : "" %>" class="<%= (i == pageNumber) ? "active" : "" %>"><%= i %></a>
    <% } %>
</div>

    <%@ include file="footer.jsp" %>

</body>
</html>
