<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="model.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Q&A Topics</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #f4f4f4;
            }
            .container {
                width: calc(100% - 400px);
                margin: 20px auto;
                display: flex;
                flex-wrap: wrap;
                gap: 20px;
                justify-content: center;
            }
            .box {
                width: calc(33.333% - 40px);
                padding: 20px;
                background-color: white;
                box-shadow: 0 2px 5px rgba(0,0,0,0.1);
                border-radius: 10px;
                transition: transform 0.3s, box-shadow 0.3s;
                text-align: center;
            }
            .box:hover {
                transform: translateY(-10px);
                box-shadow: 0 5px 15px rgba(0,0,0,0.3);
            }
            .box h3 {
                color: #007a7a;
                margin-bottom: 10px;
            }
            .box p {
                color: #333;
            }
            h1 {
                color: #007a7a; 
                text-align: center;
            }
            .read-more {
                color: #007a7a;
                cursor: pointer;
                display: inline-block;
                margin-top: 10px;
            }
        </style>
    </head>
    <body>
        <%
            TopictypeDAO topicdao = new TopictypeDAO();
            String id = (String) request.getAttribute("topicid");
            
            String text = (String) request.getAttribute("text");
            QandaDAO dao = new QandaDAO();

            // Insert sample Qandatopic data
            Qandatopic qanda1 = new Qandatopic("content197", "What is the return policy31?", "Our return policy lasts 30 days...", "topic11");
            Qandatopic qanda2 = new Qandatopic("content265", "How to track my order?111", "You can track your order using the tracking number...", "topic22");
            Qandatopic qanda3 = new Qandatopic("content32", "What are the payment options11?", "We accept various payment options including...", "topic22");
            Qandatopic qanda4 = new Qandatopic("content412", "How to contact customer service11?", "You can contact our customer service via...", "topic22");
         //  Qandatopic qanda5 = new Qandatopic("contenttho","How to contact customer service?",  text, "topic11");

            boolean isInserted1 = dao.insertQandatopic(qanda1);
            boolean isInserted2 = dao.insertQandatopic(qanda2);
            boolean isInserted3 = dao.insertQandatopic(qanda3);
            boolean isInserted4 = dao.insertQandatopic(qanda4);
           // boolean isInserted5 = dao.insertQandatopic(qanda5);
        %>
        <h1><%= topicdao.getTopicTypeById(id).getTopic() %></h1>
        
        <br><br>
        <div class="container">
            <%
                List<Qandatopic> listquestion = dao.getQandatopicsByTopicId(id);
                for (Qandatopic qanda : listquestion) {
                    String content = qanda.getContent();
                    boolean needsTruncation = content.length() > 80;
            %>
                <div class="box">
                    <h3><%= qanda.getQuestion() %></h3>
                    <p>
                        <%= needsTruncation ? content.substring(0, 80) + "..." : content %>
                    </p>
                    <% if (needsTruncation) { %>
                        <span class="read-more" onclick="window.location.href='fullContent.jsp?id=<%= qanda.getIdContent() %>'">Read More</span>
                    <% } %>
                </div>
            <% } %>
        </div>
    </body>
</html>
