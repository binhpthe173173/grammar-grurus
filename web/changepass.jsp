<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="models.*"%>
<%@ page import="dal.*"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Change Password</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        .container {
            background-color: #fff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            width: 400px;
            max-width: 100%;
        }
        h3 {
            margin-top: 0;
            text-align: center;
        }
        .message {
            margin-bottom: 20px;
            padding: 10px;
            border-radius: 5px;
        }
        .message.error {
            background-color: #f8d7da;
            color: #721c24;
            border: 1px solid #f5c6cb;
        }
        .message.success {
            background-color: #d4edda;
            color: #155724;
            border: 1px solid #c3e6cb;
        }
        form {
            display: flex;
            flex-direction: column;
        }
        label {
            margin-bottom: 5px;
            font-weight: bold;
        }
        input[type="text"], input[type="password"] {
            padding: 10px;
            margin-bottom: 15px;
            border: 1px solid #ccc;
            border-radius: 5px;
            width: calc(100% - 22px);
        }
        input[type="submit"] {
            padding: 10px;
            border: none;
            border-radius: 5px;
            background-color: #007bff;
            color: #fff;
            font-weight: bold;
            cursor: pointer;
            transition: background-color 0.3s;
            width: 100%;
        }
        input[type="submit"]:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <div class="container">
        <% 
           
            String ms = (String) request.getAttribute("ms");
            if (ms != null) {
                String messageClass = "error";
                if (ms.equals("Updated password successfully")) {
                    messageClass = "success";
                }
        %>
            <div class="message <%= messageClass %>">
                <%= ms %>
            </div>
        <% 
                // Xóa các thuộc tính sau khi hiển thị
                request.removeAttribute("ms");
               
            }
        %>
        
        <h3>Change Password</h3>
        <form action="changePass" method="post">
            <label for="oldpass">Enter old password</label>
            <input type="password" id="oldpass" name="oldpass" required>
            
            <label for="pass">Enter your new password</label>
            <input type="password" id="pass" name="pass" required >
            
            <label for="repass">Re-enter the password</label>
            <input type="password" id="repass" name="repass" required>
            
            <input type="submit" value="Submit">
        </form>
    </div>
</body>
</html>
