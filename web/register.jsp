<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Create your account</title>
        <link rel="stylesheet" href="Css/register.css">
    </head>
    <body>
        <div class="background"></div>
        <section class="home">
            <div class="Register">
                <nav class="nav">
                    <a href="homeGuest.jsp">Home</a>
            </nav>
                <h2>Register</h2>
                <form action="register" method="post">
                    <div class="input">
                        <input type="text" class="input1" placeholder="Username" name="username" required />
                    </div>
                    <div class="input">
                        <input type="text" class="input1" placeholder="Full name" name="full_name" value='${name}' required />
                    </div>
                    <div class="input">
                        <input type="password" class="input1" name="password" placeholder="Password" required />
                    </div>
                    <div class="input">
                        <input type="password" class="input1" placeholder="Confirm password" name="password_confirm" required />
                    </div>
                    <div class="input">
                        <input type="text" class="input1" name="email" placeholder="Email" value='${email}' ${email!=null?"readonly":""} required />
                    </div>
                    <div class="button">
                        <button class="btn" type="submit"> Register </button>
                    </div>
                    <div>
                        <p style="color: red">${err}</p>
                    </div>
                </form>
            </div>
        </section>
    </body>
</html>
