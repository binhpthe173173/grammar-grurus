<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cliches checking</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="Css/style.css?v=1.0">
        <style>
            .check-form {}
            .check-btn {
                background-color: #4CAF50;
                color: #ffffff;
                padding: 10px 20px;
                border: none;
                border-radius: 5px;
                cursor: pointer;
            }
            .check-btn:hover {
                background-color: #3e8e41;
            }
            .check-form textarea {
                width: 100%;
                font-size: 24px;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 5px;
                box-sizing: border-box;
                resize: none;
            }
            .chat-box {
                border: 1px solid #ccc;
                border-radius: 5px;
                padding: 10px;
                width: 100%;
                height: 300px;
                overflow-y: scroll;
                margin-top: 20px;
                background-color: pink;
            }
            .chat-box p {
                margin: 0;
                padding: 5px;
            }
        </style>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
         <script>
        function submitForm(action) {
            var form = document.getElementById('myForm');
            var textarea = document.querySelector('textarea[name="inputText"]');
            if (textarea.value.trim() === "") {
                alert("Please enter text before submitting.");
                return false;
            }
            form.action = action;
            form.submit();
        }

        function showUploadModal() {
            $('#uploadModal').modal('show');
        }

        function handleFileUpload() {
            var fileInput = document.getElementById('fileInput');
            if (fileInput.files.length === 0) {
                alert("Please select a file before uploading.");
                return false;
            }
            var form = document.getElementById('fileUploadForm');
            form.submit();
        }
        function handleNonSubscribe() {
                window.alert("You have to subscribe to use this function");
            }
    </script>
    </head>
    <body>
            <% 
                List<String> errorGrammar = (List<String>) request.getAttribute("error");
                List<String> lc = (List<String>) request.getAttribute("lc");
                List<String> suggestGrammar = (List<String>) request.getAttribute("suggestion");
                String texted = (String) request.getAttribute("inputText");
            %>

    <div class="container"><br/>
            <div class="flexde">
        
                <aside class="sidebar">
                    <ul>
                        <jsp:include page="sideBarUser.jsp" />
                    </ul>
                </aside>
                <br/>-----
            <main class="editor-container">
                    <form class="form-class" id="myForm" method="post">
                        <div class="button-group">
                                <button class="toolbar-button" type="button" onclick="submitForm('grammarFunction')">Grammar Checking <i class="fas fa-spell-check"></i> </button>
                                <button class="toolbar-button" type="button" onclick="submitForm('spellFunction')">Spell Checking <i class="fas fa-spell-check"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('sticky')">Sticky <i class="fas fa-lock"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('repeatFunction')">All Repeat <i class="fas fa-redo"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('cliche')">Cliche <i class="fas fa-pen-alt"></i></button>
                                <c:if test="${ac.role_id==1}">
                                    <button class="toolbar-button" type="button" onclick="handleNonSubscribe()">Overview <i class="fas fa-pen-alt"></i></button>
                                </c:if>
                                <c:if test="${ac.role_id==2 || ac.role_id==0}">
                                    <button class="toolbar-button" type="button" onclick="submitForm('overview')">Overview <i class="fas fa-pen-alt"></i></button>
                                </c:if>
                                <button class="toolbar-button" type="button" onclick="submitForm('alliterationFunction')">Alliteration <i class="fas fa-music"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('allsentencesFunction')">Length <i class="fas fa-ruler-horizontal"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('complexWordText')">Complex Word <i class="fas fa-comment-alt"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('consistencyText')">Consistency Word <i class="fas fa-align-center"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('Text2DVA')">DVA <i class="fas fa-chart-bar"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('eadabilityTextResult')">Readability <i class="fas fa-book-open"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('transitionsTextEr')">Transitions Check <i class="fas fa-arrows-alt-h"></i></button>
                                <c:if test="${ac.role_id==1}">
                                    <button class="toolbar-button" type="button" onclick="handleNonSubscribe()">Upload File <i class="fas fa-file-upload"></i></button>
                                </c:if>
                                <c:if test="${ac.role_id==2 || ac.role_id==0}">
                                    <button class="toolbar-button" type="button" onclick="showUploadModal()">Upload File <i class="fas fa-file-upload"></i></button>
                                </c:if>
                            </div>
                        <textarea name="inputText" rows="9" cols="50" required><%= (texted != null && !texted.isEmpty()) ? texted : "Check your essay here!!" %></textarea>
</form>
                </main>
                     <% if (lc != null) {
                     int count=1;
                     %> 
                     <aside class="editor-notes" >
                    <div class="note" >
                        <% for (String string : lc) { String Show=count+" - "+string; %>
                            <p><%= Show %></p>
                        <%count++; } %>
                    </div>
                
            <% } %> 
                </aside>
         
       </div>
        </div>
 <!-- Modal for File Upload -->
    <div id="uploadModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload File</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body upload-modal-body">
                    <form id="fileUploadForm" method="post" action="fileUpload" enctype="multipart/form-data">
                        <input type="file" id="fileInput" name="file" accept=".docx" />
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="handleFileUpload()">Upload</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
