<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="models.*"%>
<%@page import="dal.*"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Details Page</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #F5F5F5;
        }
        .header {
            background-color: #007A7A;
            padding: 20px;
            text-align: center;
        }
        .header h1 {
            margin: 0;
            font-size: 24px;
        }
        .content {
            margin: 20px;
            padding: 20px;
            background-color: white;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        .article-content {
        white-space: pre-wrap;
          white-space: pre-line;
    }
    </style>
</head>
<body>
    
        
        
    
    
    <div class="header">
        <h1>Details Page</h1>
    </div>
    
     
    <div class="content">
        <%
       Account x = (Account) session.getAttribute("us");
       if(x!=null&& x.getRole_id()==0)
       {
        %>
        <form action="deleteGG" method="post">
            
            <input type="hidden" name="id" value="${qanda.idContent}">
            <input type="submit"  value="Delete" >
        </form><%}%>
        
        
        <h2>Question Details</h2>
        <p>ID Content: ${qanda.idContent}</p>
        <p>Question: ${qanda.question}</p>
        <p class="article-content">Content: ${qanda.content}</p>
      
    </div>
</body>
</html>
