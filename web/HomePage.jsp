<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Menu</title>
    <link rel="stylesheet" type="text/css" href="Css/menu.css">
</head>
<body>
    <div class="hero">
        <nav>
            <img src="Image/logo.png" class="logo" />
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Services</a></li>
                <li><a href= "http://localhost:9999/GrammarGurusv0.0/Upfiledocx.jsp">Upload File</a></li>
            </ul>
            <img src="Image/user.jpg" class="user-pic" onclick="toggleMenu()" />
            <div class="sub-menu-wrap" id="subMenuWrap">
                <div class="sub-menu">
                    <div class="user-info">
                        <img src="Image/user.jpg" />
                        <h3>Unknown</h3>
                    </div>
                    <hr/>
                    <a href="#" class="sub-menu-link">
                        <img src="Image/profile.png" />
                        <p>Edit Profile</p>
                        <span>-</span>
                    </a>
                    <a href="#" class="sub-menu-link">
                        <img src="Image/setting.png" />
                        <p>Setting & Privacy</p>
                        <span>-</span>
                    </a>
                    <a href="#" class="sub-menu-link">
                        <img src="Image/help.png" />
                        <p>Help & Support</p>
                        <span>-</span>
                    </a>
                    <a href="/login" class="sub-menu-link">
                        <img src="Image/logout.png" />
                        <p>Login</p>
                        <span>-</span>
                    </a>
                </div>
            </div>
        </nav>
        <input type="text" placeholder="Write your thoughts..." style="width: 800px;  height: 300px; border: 1px solid gray; padding-left: 10px; padding-bottom: 150px; font-size: 16px; font-family: Arial;" />
    </div>

    <script src="Css/menu.js"></script>
</body>
</html>