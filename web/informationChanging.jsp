<%-- 
    Document   : informationChanging
    Created on : Jun 17, 2024, 3:43:59 PM
    Author     : admin
--%>
<%@page import="models.Account" %>
<%@page import="dal.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Information Changing</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            background-color: #f9f9f9;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        
        .form-container {
            width: 100%;
            max-width: 500px;
            margin: 20px;
            padding: 30px;
            background-color: #ffffff;
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
            text-align: center;
        }
        
        .form-container h2 {
            margin-bottom: 20px;
            color: #4CAF50;
        }
        
        .profile-image {
            width: 50px;
            height: 50px;
            border-radius: 50%;
            margin: 10px;
            border: 2px solid #4CAF50;
        }
        
        .form-group {
            margin-bottom: 20px;
        }
        
        .form-group label {
            display: block;
            font-weight: bold;
            margin-bottom: 5px;
            color: #333;
        }
        
        .form-group input[type="text"],
        .form-group input[type="password"] {
            width: calc(100% - 22px);
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            font-size: 16px;
        }
        
        .form-group a {
            color: #4CAF50;
            text-decoration: none;
            font-size: 14px;
        }
        
        .form-group a:hover {
            text-decoration: underline;
        }
        
        .form-group input[type="submit"] {
            background-color: #4CAF50;
            color: #ffffff;
            border: none;
            padding: 10px 20px;
            font-size: 16px;
            border-radius: 5px;
            cursor: pointer;
            transition: background-color 0.3s;
        }
        
        .form-group input[type="submit"]:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <div class="form-container">
        <h2>Change Information</h2>
        <% Account a = (Account)request.getAttribute("ac"); %>
        <form action="inforChanges" method="post">
            <div class="form-group">
                <label>Choose Image:</label>
                <label><input type="radio" name="Image" value="Image.jpg" checked> <img src="Image/user.jpg" alt="User Image" class="profile-image" /></label>
                <label><input type="radio" name="Image" value="Image/Usergirl.jpg"> <img src="Image/Usergirl.jpg" alt="User Image" class="profile-image" /></label>
                <label><input type="radio" name="Image" value="Image/userman.png"> <img src="Image/userman.png" alt="User Image" class="profile-image" /></label>
            </div>
            <div class="form-group">
                <label for="name">User Name:</label>
                <input type="text" id="name" name="name" value="<%= a.getUsername() %>" />
            </div>
            <div class="form-group">
                <a href="changepass.jsp">Change password</a>
            </div>
            <div class="form-group">
                <input type="submit" value="Update" />
            </div>
        </form>
    </div>
</body>
</html>
