<%-- 
    Document   : manageFAQ
    Created on : Jul 12, 2024, 11:47:44 AM
    Author     : Hải Hoàn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="models.*"%>
<%@page import="dal.*"%>

<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8">
    <title>FAQ MANAGER</title>
    <style>
         .custom-breadcrumb {
            background-color: #e9ecef;
            padding: 10px 15px;
            border-radius: 5px;
            margin: 20px 0;
        }
        
        .custom-breadcrumb .breadcrumb {
            background-color: transparent;
            padding: 0;
            margin: 0;
            list-style: none;
            display: flex;
        }
        
        .custom-breadcrumb .breadcrumb-item + .breadcrumb-item::before {
            content: ">";
            color: #6c757d;
            padding: 0 5px;
        }
        
        .custom-breadcrumb .breadcrumb-item a {
            color: #007A7A;
            text-decoration: none;
        }
        
        .custom-breadcrumb .breadcrumb-item a:hover {
            color: #005a5a;
            text-decoration: underline;
        }
        
        .custom-breadcrumb .breadcrumb-item.active {
            color: #6c757d;
        }
        body {
            margin: 0;
            padding: 0;
        }
        .container {
            display: flex;
            flex-wrap: wrap;
            gap: 10px;
            justify-content: space-between;
            padding: 0 200px;
        }
        .box h2 {
            color: #007a7a;
            text-align: center;
        }
        .box {
            border: 1px solid #007a7a;
            padding: 10px;
            width: calc(30% - 20px);
            box-sizing: border-box;
            transition: transform 0.2s ease-in-out;
            cursor: pointer;
            border-radius: 8px;
            text-decoration: none;
            color: inherit;
            margin-bottom: 20px;
        }
        .box:hover {
            transform: scale(1.03);
            box-shadow: 0 0 10px rgba(0, 122, 122, 0.3);
        }
        .box strong, .box p {
            color: black;
            text-align: center;
        }
        h1 {
            color: #007A7A;
            text-align: center;
        }
        .contact-container {
            margin: 20px 0;
            display: flex;
            justify-content: center;
            align-items: center;
            gap: 10px;
        }
        .contact-question {
            color: #808080;
            font-weight: normal;
        }
        .contact-link {
            color: #007a7a; 
            font-weight: bold;
            text-decoration: none;
        }
        .contact-link:hover {
            text-decoration: underline;
        }
          .add-faq-container {
            display: flex;
            justify-content: center;
            margin-bottom: 20px;
            color: #007a7a;
        }
          .custom-breadcrumb .breadcrumb-item + .breadcrumb-item::before {
            content: ">";
        }
        .custom-breadcrumb .breadcrumb-item a {
            color: #007A7A;
        }
        .custom-breadcrumb .breadcrumb-item a:hover {
            color: #005a5a;
        }

    </style>
</head>
<body>
    
    <nav aria-label="breadcrumb" class="custom-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<%= request.getContextPath() %>/Admin/homePage">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Manager FAQ</li>
        </ol>
    </nav>
    <%
       Account x = (Account) session.getAttribute("us");
       if(x!=null&& x.getRole_id()==0)
       {
      %>
    <h1>FAQ MANAGER</h1>
    <br><br>
    
  
     
    
    <div class="add-faq-container">
        <a href="insertFaq.jsp" class="add-faq-button"><h2>Add FAQ</h2></a>
    </div>
    
    
    <%
        TopictypeDAO dao = new TopictypeDAO();
        QandaDAO qadao = new QandaDAO();

       
        List<Topictype> topictypeList = dao.getTopicTypesByType("f");
    %>
    <div class="container">
        <% for (Topictype topictype : topictypeList) { %>
        <a href="manageContentFAQ?idtopic=<%= topictype.getIdtopic() %>" class="box">
            <h2><%= topictype.getTopic() %></h2>
            <p><%= qadao.countQandatopicsByTopicId(topictype.getIdtopic()) %> <strong>Articles</strong></p>
        </a>
        <% } %>
    </div>
    
    <%}else {%>
    <h2 corlor="green">Please <a href="login.jsp">Login</a> To Use Many Other Interesting Features</h2>
    <%}%>
</body>  
</html>

