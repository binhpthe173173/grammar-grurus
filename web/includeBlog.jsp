<%-- 
    Document   : bloginclude
    Created on : Jul 3, 2024, 12:45:20 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="models.*" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <style>
       .custom-breadcrumb {
            background-color: #e9ecef;
            padding: 10px 15px;
            border-radius: 5px;
            margin: 20px 0;
        }
        
        .custom-breadcrumb .breadcrumb {
            background-color: transparent;
            padding: 0;
            margin: 0;
            list-style: none;
            display: flex;
        }
        
        .custom-breadcrumb .breadcrumb-item + .breadcrumb-item::before {
            content: ">";
            color: #6c757d;
            padding: 0 5px;
        }
        
        .custom-breadcrumb .breadcrumb-item a {
            color: #007A7A;
            text-decoration: none;
        }
        
        .custom-breadcrumb .breadcrumb-item a:hover {
            color: #005a5a;
            text-decoration: underline;
        }
        
        .custom-breadcrumb .breadcrumb-item.active {
            color: #6c757d;
        }

     .describle{
        text-align: center;
        
    }  
    h1 {
        color: #007A7A;
    }
    h2{
        
        text-align: center
    }
    h3 {
        margin-bottom: 30px
    }
    
    
</style>
 <nav aria-label="breadcrumb" class="custom-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<%= request.getContextPath() %>/Admin/homePage">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Manager Blogs</li>
        </ol>
    </nav>
<div class="describle">
<h1>Grammar Gurus</h1>
<H3><p >Manager Blog</p></h3>




   <%@ include file="showBlog.jsp" %>
</br>
 
   
    
      </br>
   
    </body>
</html>

