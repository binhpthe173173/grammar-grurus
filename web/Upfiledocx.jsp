<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Menu</title>
        <title>File Upload Result</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f4f4f4;
                margin: 0;
                padding: 0;
            }
            .container {
                width: 90%;
                max-width: 1200px;
                margin: 20px auto;
                padding: 20px;
                background-color: #fff;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                border-radius: 8px;
            }
            h1, h2 {
                color: #0056b3;
                text-align: center;
            }
            p {
                font-size: 16px;
                line-height: 1.6;
                text-align: center;
            }
            form {
                width: 100%;
                display: flex;
                flex-direction: column;
                align-items: center;
                margin-bottom: 20px;
            }
            input[type="file"] {
                margin-bottom: 10px;
                font-size: 16px;
            }
            input[type="submit"] {
                margin: 10px 0;
                padding: 10px 20px;
                font-size: 16px;
                background-color: #3e8e41;
                color: #fff;
                border: none;
                border-radius: 5px;
                cursor: pointer;
                transition: background-color 0.3s;
            }
            input[type="submit"]:hover {
                background-color: #3e8e41;
            }
            textarea {
                width: 100%;
                height: 60vh;
                overflow: auto;
                white-space: pre-wrap;
                box-sizing: border-box;
                padding: 10px;
                font-size: 14px;
                line-height: 1.5;
                border: 1px solid #ccc;
                border-radius: 5px;
                margin-top: 20px;
            }
            .button-group {
                display: flex;
                justify-content: center;
                gap: 10px;
                margin-top: 20px;
            }
            .button-group button {
                padding: 10px 20px;
                font-size: 16px;
                color: #fff;
                background-color: #3e8e41;
                border: none;
                border-radius: 5px;
                cursor: pointer;
                transition: background-color 0.3s;
            }
            .button-group button:hover {
                background-color: #3e8e41;
            }
        </style>
        <script>
            function submitForm(action) {
                var form = document.getElementById('myForm');
                var textarea = document.querySelector('textarea[name="text1"]');
                if (textarea.value.trim() === "") {
                    alert("Please enter text before submitting.");
                    return false;
                }
                form.action = action;
                form.submit();
            }

            function validateFileUpload() {
                var fileInput = document.querySelector('input[type="file"]');
                if (fileInput.files.length === 0) {
                    alert("Please choose a file to upload.");
                    return false;
                }
                return true;
            }
        </script>
    </head>
    <body>

        <div class="container">
            <jsp:include page="menujsp.jsp" />
        </div>
        <div class="container">
            <h1>File Upload</h1>
            <form action="fileUpload" method="post" enctype="multipart/form-data" onsubmit="return validateFileUpload()">
                <input type="file" name="file" accept=".docx" />
                <input type="submit" value="Upload"/>
            </form>

            <h1>File Upload Result</h1>
            <p><strong>Uploaded File Name:</strong> ${fileName}</p>
            <h2>File Content:</h2>
            <form id="myForm" method="post">
                <textarea name="text1">${content}</textarea>
                <input type="hidden" name="fileName" value="${fileName}" />
                <div class="button-group">
                    <button type="button" onclick="submitForm('checkFunction')">Grammar, Spell</button>
                    <button type="button" onclick="submitForm('Cliche')">Cliche</button>
                    <button type="button" onclick="submitForm('sticky')">Sticky</button>
                    <button type="button" onclick="submitForm('allRepeatword')">All Repeat</button>
                </div>
            </form>
        </div>
    </body>
</html>
