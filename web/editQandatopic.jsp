<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="models.*"%>
<%@ page import="dal.*"%>
<%@ page import="java.util.*"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Full Content</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 20px;
                background-color: #f4f4f4;
            }
            .content-box {
                max-width: 1200px;
                margin: 20px auto;
                padding: 20px;
                background-color: white;
                box-shadow: 0 2px 5px rgba(0,0,0,0.1);
                border-radius: 10px;
            }
            .content-box h1 {
                color: #007a7a;
            }
            .content-box p {
                color: #333;
            }
            .article-content {
                white-space: pre-wrap;
                white-space: pre-line;
            }
            .content-box form {
                display: flex;
                flex-direction: column;
            }
            .content-box input[type="text"],
            .content-box textarea {
                width: 100%;
                padding: 10px;
                margin: 10px 0;
                border: 1px solid #ddd;
                border-radius: 5px;
            }
            .content-box input[type="text"] {
                font-size: 2em;
                color: #007a7a;
            }
            .content-box input[type="submit"] {
                height: 50px;
background-color: #007a7a;
                color: whitesmoke;
            }
            .content-box textarea {
                height: 500px;
                font-size: 1em;
            }
        </style>
    </head>
    <body>
        <div class="content-box">
            <%
      String idContent =(String) request.getAttribute("id");          
      QandaDAO dao = new QandaDAO();
                Qandatopic qanda = dao.getQandatopicByIdContent(idContent);
                if (qanda != null) {
            %>
            <form action="editQandatopic" method="post">
                <input type="hidden" name="id" value="<%= qanda.getIdContent() %>">
                <h3><input type="text" name="question" value="<%= qanda.getQuestion() %>"></h3>
                <textarea name="content" rows="10" cols="100"><%= qanda.getContent() %></textarea>
                <input type="submit" value="Update">
            </form>
            <% } else { %>
            <p>Content not found.</p>
            <% } %>
        </div>
    </body>
</html>
