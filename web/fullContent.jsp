<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="models.*"%>
<%@page import="dal.*"%>

<%@page import="java.util.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Full Content</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 20px;
                background-color: #f4f4f4;
            }
            .content-box {
                max-width: 800px;
                margin: 20px auto;
                padding: 20px;
                background-color: white;
                box-shadow: 0 2px 5px rgba(0,0,0,0.1);
                border-radius: 10px;
            }
            .content-box h1 {
                color: #007a7a;
            }
            .content-box p {
                color: #333;
            }
            .article-content {
        white-space: pre-wrap;
          white-space: pre-line;
    }
        </style>
    </head>
    <body>
        <div class="content-box">
            <%
                String idContent = request.getParameter("id");
                QandaDAO dao = new QandaDAO();
                Qandatopic qanda = dao.getQandatopicByIdContent(idContent);
                if (qanda != null) {
            %>
                <h1><%= qanda.getQuestion() %></h1>
                <p class="article-content" ><%= qanda.getContent() %></p>
                
            <% } else { %>
                <p>Content not found.</p>
            <% } %>
        </div>
    </body>
</html>
