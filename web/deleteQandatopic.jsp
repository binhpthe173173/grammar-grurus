<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="models.*"%>
<%@page import="dal.*"%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Delete Qandatopic</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }
        .container {
            width: 100%;
            max-width: 600px;
            margin: 20px auto;
            padding: 20px;
            background-color: white;
            box-shadow: 0 2px 5px rgba(0,0,0,0.1);
            border-radius: 10px;
        }
        p { text-align: center;
            
        }
        h1 {
            color: #007a7a;
            text-align: center;
        }
        .confirmation {
            margin-bottom: 20px;
        }
        .btn-container {
            display: flex;
            justify-content: center;
            gap: 10px;
        }
        .cancel-btn, .delete-btn {
            padding: 8px 16px;
            background-color: #007a7a;
            color: white;
            border: none;
            cursor: pointer;
            border-radius: 5px;
            text-decoration: none;
            transition: background-color 0.3s;
        }
        .delete-btn:hover {
            background-color: #005959;
        }
    </style>
</head>
<body>
    <div class="container">
        <%
            String idContent = request.getParameter("id");
           
        %>
        <h1>confirm deletion </h1>
        <div class="confirmation">
            <p>Are you sure you want to delete the FAQ content with ID: <%= idContent %>?</p>
        </div>
        <form action="deleteQandatopic" method="post">
            <input type="hidden" name="id" value="<%= idContent %>" >
    
    <div class="btn-container">
          <a href="javascript:history.back()" class="cancel-btn">Cancel</a>
       
        <button type="submit"  class="delete-btn"> Confirm </button>
    </div>
</form>



    </div>
</body>
</html>
