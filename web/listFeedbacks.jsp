<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="models.*" %>
<%@ page import="dal.*" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Reviews</title>
        <style>
            /* Các CSS của bạn */
            .reviews-container {
                display: flex;
                flex-wrap: wrap;
                gap: 2rem;
                justify-content: center;
            }
            h2 {
                text-align: center; /* Căn giữa tiêu đề */
                color: #007A7A; /* Màu chữ cho tiêu đề */
            }
            h3 {
                text-align: center; /* Căn giữa tiêu đề */
                color: black; /* Màu chữ cho tiêu đề */
            }
            .card {
                background-color: rgba(243, 244, 246, 1);
                padding: 2rem;
                width: calc(33.33% - 2rem); /* 3 cards per row with gap */
                max-width: 320px;
                border-radius: 10px;
                box-shadow: 0 20px 30px -20px rgba(5, 5, 5, 0.24);
                margin-bottom: 2rem;
            }
            .header {
                display: flex;
                align-items: center;
                gap: 1rem;
            }
            .header .image {
                height: 4rem;
                width: 4rem;
                border-radius: 9999px;
                object-fit: cover;
                background-color: royalblue;
            }
            .stars {
                display: flex;
                justify-content: center;
                gap: 0.125rem;
                color: rgba(255, 215, 0, 1); /* Màu vàng cho sao */
            }
            .stars svg {
                height: 1rem;
                width: 1rem;
            }
            .name {
                margin-top: 0.25rem;
                font-size: 1.125rem;
                line-height: 1.75rem;
                font-weight: 600;
                color: #007A7A;
                text-align: center;
            }
            .message {
                margin-top: 1rem;
                color: rgba(107, 114, 128, 1);
            }
            .pagination {
                display: flex;
                justify-content: center;
                gap: 1rem;
                margin-top: 2rem;
            }
            .pagination a {
                padding: 0.5rem 1rem;
                background-color: #007A7A;
                border-radius: 5px;
                text-decoration: none;
                color: white;
            }
            .pagination a.active {
                background-color: gray;
                color: white;
            }
            .filter {
                margin-top: 40px;
                display: flex;
                justify-content: center;
                margin: 1rem 0;
                margin-bottom: 40px;
            }
            .filter a {
                display: flex;
                align-items: center;
                margin: 0 0.5rem;
                text-decoration: none;
                color: #007A7A;
                font-weight: bold;
                transition: background-color 0.3s, color 0.3s;
                padding: 0.5rem;
                border-radius: 5px;
            }
            .filter a:hover {
                background-color: #007A7A;
                color: white;
            }
            .filter a.active {
                background-color: #007A7A;
                color: white;
            }
            .filter a svg {
                margin-right: 0.5rem;
            }
            .rating {
                display: flex;
                flex-direction: row-reverse;
                gap: 0.3rem;
            }
            .rating input {
                display: none;
            }
            .rating label {
                cursor: pointer;
            }
            .rating svg {
                width: 2rem;
                height: 2rem;
                overflow: visible;
                fill: transparent;
                stroke: #666;
                stroke-linejoin: bevel;
                stroke-dash: 0.2s;
            }
            .rating label:hover svg {
                stroke: #ffc73a;
            }
            .rating input:checked ~ label svg {
                transition: 0s;
                fill: #ffc73a;
                stroke: #ffc73a;
                stroke-opacity: 0;
                stroke-dasharray: 0;
                stroke-linejoin: miter;
                stroke-width: 8px;
            }
             .rate-feed {
                max-width: 600px;
                margin: 20px auto;
                padding: 20px;
                border: 1px solid #ccc;
                border-radius: 8px;
            }
            textarea {
                width: 100%;
                height: 100px;
                margin-bottom: 10px;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
                resize: vertical;
            }
            button {
                background-color: #007A7A;
                color: white;
                padding: 10px 20px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            button:hover {
                background-color: #45a049;
            }
                     header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 10px 20px;
    background-color: #fff;
    border-bottom: 1px solid #ddd;
}

header .logo {
    font-size: 24px;
    font-weight: bold;
    color: #000;
    text-decoration: none;
}

header a {
    margin: 0 15px;
    color: #000;
    text-decoration: none;
    transition: color 0.3s;
}

header a:hover {
    color: #007A7A;
}

        </style>
    </head>
    <body>
        <%
            Account x = (Account) session.getAttribute("us");
            FeedDAO u = new FeedDAO();
                String ms = (String) session.getAttribute("mserro");

            String st = (String) request.getAttribute("star");
            int star = 0;
            if (st != null) {
                star = Integer.parseInt(st);
            } else {
                String starParam = request.getParameter("star");
                star = (starParam != null && !starParam.isEmpty()) ? Integer.parseInt(starParam) : 0;
            }
    
            List<Feedback> feedbacks;
            if (star != 0) {
                feedbacks = u.getFeedbackByStar(star);
            } else {
                feedbacks = u.getFeedbacks();
            }
        %>
        
        <%
               if(x==null|| x.getRole_id()!=0)
 {%>
        
 
         <header>
        <div>
            <a href="#" class="logo">Grammar Gurus</a>
        </div>
        <div>
            
             <%        if(x!=null){%>
            <a href="checkFunction.jsp">Home</a>
            <%}%>
          
          <%        if(x==null){%>
          <a href="userBlog.jsp">Blog</a>
            <a href="listFAQ.jsp">FAQ</a> 
            
             <a href="grammarGuideList">Grammar Guide</a>
             <a href="login.jsp">Use App</a>
            <a href="register.jsp">Register</a>
            <%}%>
        </div>
        </div>
    </header>
        <%}%>
        
        
        
       
        
        
        
        
        
        
        
        <h2>Creative writers never publish without us</h2>
        <h3>From sensory and pacing feedback to finding those pesky missing commas,</h3>
        <h2>Grammar Gurus</h2>
        <div class="filter">
            <% for (int s = 1; s <= 5; s++) { %>
            <a href="fillterStar?star=<%=s%>" >
                <% for (int j = 1; j <= s; j++) { %>
                <svg fill="gold" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" width="20" height="20">
                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"/>
                </svg>
                <% } %>Star
            </a>
            <% } %>
        </div>

        <%
            int pageSize = 8;
            int totalPages = (int) Math.ceil((double) feedbacks.size() / pageSize);
            String pageParam = request.getParameter("page");
            int currentPage = (pageParam != null && !pageParam.isEmpty()) ? Integer.parseInt(pageParam) : 1;

            int startIndex = (currentPage - 1) * pageSize;
            int endIndex = Math.min(startIndex + pageSize, feedbacks.size());
        %>

        <div class="reviews-container">
            <% for (int i = startIndex; i < endIndex; i++) {
                int idac = feedbacks.get(i).getAccount_id();
                DAO dao = new DAO();
                Account acc = dao.getAccountById(idac);
            %>
            <div class="card">
                <div class="stars">
                    <% 
                        int rating = feedbacks.get(i).getStar_rate();
                        for (int j = 0; j < rating; j++) { %>
                    <svg fill="gold" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"/>
                    </svg>
                    <% } %>
                </div>
                <div class="header">
                    <img src="<%= acc.getImage() %>" alt="User Image" class="image">
                </div>
                <div class="name"><%= acc.getUsername() %></div>
                <p class="message"><%= feedbacks.get(i).getContent() %></p>

<% if (x!=null&& x.getRole_id() == 0) { %>
                <form action="deleteFeedback" method="GET">
                    <input type="hidden" name="id" value="<%= feedbacks.get(i).getId() %>">
                    <button type="submit" class="delete-btn">Delete</button>
                </form><%}%>
              
            </div> 
                
            <% } %>
        </div>

        <div class="pagination">
            <% for (int i = 1; i <= totalPages; i++) { %>
            <a href="?page=<%= i %>" class="<%= (i == currentPage) ? "active" : "" %>"><%= i %></a>
            <% } %>
        </div>

        <%
            if (x != null) {
                int userID = x.getId();
               Feedback feedbackAccount = u.getFeedbackByAc(userID);
        

                if (feedbackAccount==null) {
        %>
        <form class="rate-feed"  action="sendFeedBack" method="POST">
            <h2>Please rate the system and contribute your development comments to our community</h2>
            <div class="rating">
                <input type="radio" id="star-5" name="rating" value="5" checked >
                <label for="star-5">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                    </svg>
                </label>
                <input type="radio" id="star-4" name="rating" value="4">
                <label for="star-4">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                    </svg>
                </label>
                <input type="radio" id="star-3" name="rating" value="3">
                <label for="star-3">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                    </svg>
                </label>
                <input type="radio" id="star-2" name="rating" value="2">
                <label for="star-2">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                    </svg>
                </label>
                <input type="radio" id="star-1" name="rating" value="1">
                <label for="star-1">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                    </svg>
                </label>
            </div><% if(ms!=null) {%>
         <p style="color: red"><%=ms%></p>
         <%
            
session.removeAttribute("mserro");
         %>
         <%}%>
        <input  type="hidden" name="name" value="<%=x.getUsername()%>"> 
        <input  type="hidden" name="idacc" value="<%=x.getId()%>">
            <textarea name="review" placeholder="Update your feedback..."></textarea>
            <button type="submit">Submit</button>
        </form>
        <%
                }
        else {%>

        <form class="rate-feed" action="deleteFeedbackUser" method="get">
    <h2>Your Feedback & Rate</h2>
    <div class="rating">
        <% int userRating = feedbackAccount.getStar_rate(); %>
        <input type="radio" id="star-5" name="rating" value="5" <%= (feedbackAccount.getStar_rate() == 5) ? "checked" : "" %>>
        <label for="star-5">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
            </svg>
        </label>
        <input type="radio" id="star-4" name="rating" value="4" <%= (feedbackAccount.getStar_rate() == 4) ? "checked" : "" %>>
        <label for="star-4">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
            </svg>
        </label>
        <input type="radio" id="star-3" name="rating" value="3" <%= (feedbackAccount.getStar_rate() == 3) ? "checked" : "" %>>
        <label for="star-3">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
            </svg>
        </label>
        <input type="radio" id="star-2" name="rating" value="2" <%= (feedbackAccount.getStar_rate() == 2) ? "checked" : "" %>>
        <label for="star-2">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
            </svg>
        </label>
        <input type="radio" id="star-1" name="rating" value="1" <%= (feedbackAccount.getStar_rate() == 1) ? "checked" : "" %>>
        <label for="star-1">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
            </svg>
        </label>
    </div>  <% if(ms!=null) {%>
         <p style="color: red"><%=ms%></p>
         <%
          session.removeAttribute("mserro");
         
         
        }%>
         <input type="hidden" name="id" value="<%= feedbackAccount.getId() %>">
    <textarea name="feedback"><%= feedbackAccount.getContent() %></textarea>
    <button type="submit" name="action" value="edit">Edit</button>
    <button type="submit" name="action" value="delete">Delete</button>
</form>




        <%}


            }
        %>

    </body>
</html>
