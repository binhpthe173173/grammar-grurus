<%@ page import="java.util.List" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="models.*"%>
<%@ page import="dal.*"%>
<style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        .search-container {
            text-align: center;
            margin: 20px 0;
        }
        .search-container input[type="text"] {
            padding: 10px;
            width: 300px;
            border: 1px solid #007A7A;
            border-radius: 5px;
        }
        .search-container input[type="submit"] {
            padding: 10px 20px;
            background-color: #007A7A;
            color: white;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            transition: background-color 0.3s;
        }
        .search-container select  {
            padding: 10px;
            width: 170px;
            border: 1px solid #007A7A;
            border-radius: 5px;
        }
        .search-container input[type="submit"]:hover {
            background-color: #005a5a;
        }
        .blog-container {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            padding: 20px;
        }
        .blog-container h4 {
            color: #007A7A;
        }
        .blog-item {
            flex: 0 0 30%;
            box-sizing: border-box;
            margin: 1.5%;
            padding: 20px;
            border: 1px solid #007A7A;
            border-radius: 10px;
            background-color: #E6F0FB;
            transition: transform 0.2s, box-shadow 0.2s;
        }
       
        .blog-item h2 {
            font-size: 1.5em;
            color: #007A7A;
        }
        .blog-item p {
            color: #666;
        }
        
        .haha {
            text-align: center;
            margin-top: 20px;
        }
        .pagination {
            text-align: center;
            margin-top: 20px;
        }
        .pagination a {
            color: #007A7A;
            text-decoration: none;
            margin: 0 5px;
            padding: 5px 10px;
            border: 1px solid #007A7A;
            border-radius: 5px;
            transition: background-color 0.3s, color 0.3s;
        }
        .pagination a:hover {
            background-color: #007A7A;
            color: #fff;
        }
        .pagination a.active {
            background-color: #007A7A;
            color: #fff;
        }
        .write-blog-btn {
            display: block;
            width: 150px;
            margin: 20px auto;
            padding: 10px 15px;
            background-color: #007A7A;
            color: white;
            text-align: center;
            text-decoration: none;
            border-radius: 5px;
            font-weight: bold;
            transition: background-color 0.3s;
        }
        .write-blog-btn:hover {
            background-color: #005a5a;
        }
        .button-w-A{
            display: flex;
        }
   .title-check {
    text-decoration: none;
    color: black; 
    transition: color 0.3s; 
}

.title-check:hover {
    color: #007A7A; 
}
    </style>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Blog List</title>
    <% 
    Account x1 = (Account) session.getAttribute("us");
    DAO dao1 = new DAO();
    
    BlogDAO dao = new BlogDAO();
    
    
   String searchQuery = (String)request.getAttribute("search");
    String typeQuery = (String)request.getAttribute("type");
    
    List<Blog> blogs;
    
    if ((searchQuery != null || typeQuery != null)) {
    blogs = dao.searchBlogs(searchQuery, typeQuery);
    } else {
    typeQuery= request.getParameter("type");
    searchQuery = request.getParameter("search");
    
    blogs = dao.searchBlogs(searchQuery, typeQuery);
        
        }
       
        
    int pageSize = 6;
    int pageNumber = 1; 
    String pageParam = request.getParameter("page");
    if (pageParam != null) {
        try {
            pageNumber = Integer.parseInt(pageParam);
        } catch (NumberFormatException e) {
            
        
        }
    }
    int start = (pageNumber - 1) * pageSize;
    int end = Math.min(start + pageSize, blogs.size());
    int totalPages = (int) Math.ceil(blogs.size() / (double) pageSize);
%>

</head>
<body>
    <% if(x1 != null && x1.getRole_id() == 0) { %>
    
    <div class="grammar-image">
                <img src="https://assets.prowritingaid.com/f/145420/750x469/7be4eba205/blog-title-copywriting-vs-content-writing.png/m/" alt="Grammar Checking">
            </div>
    
    <div class="button-w-A">
        
     <a href="blog.jsp" class="write-blog-btn">Write Blog</a>
    <a href="blogApproval.jsp" class="write-blog-btn">   Blog Approval:<%=dao.countBlogsByState("choduyet")%></a>  
    
    
    </div>
    
    <div class="search-container">
        <form action="searchBlog" method="post">
        <input type="text" name="search" placeholder="Search by title" value="<%= searchQuery != null ? searchQuery : "" %>">
        <select name="type">
            <option value="">All Topic</option>
            <option value="Grammar">Grammar</option>
            <option value="Business Writing">Business Writing</option>
            <option value="Creative Writing">Creative Writing</option>
            <option value="Student Writing">Student Writing</option>
            <option value="Inspiration">Inspiration</option>
            <option value="GrammarGurus Help">GrammarGurus Help</option>
            <option value="Other">Other</option>
        </select>
        <input type="submit" value="Search">
    </form>
</div>

  <%
  if(dao.searchBlogs(searchQuery, typeQuery)==null){
  %>
  
  <h2><p>Result of Searching</p></h2>
  
  
  
  <%}%>
    
    <div class="blog-container">
        <% for (int i = start; i < end; i++) { 
            Blog blog = blogs.get(i);
        %>
            <div class="blog-item">
                <h2><%= blog.getType() %></h2>
                <h4> <a  href="fullContentblog.jsp?id=<%= blog.getIdBlog() %> "class="title-check" style="color: #007A7A;"><%= blog.getTitle() %></a></h4>
                
                <p><%=dao1.getAccountById(blog.getIdacount()).getUsername() %></p>
                <p><%= blog.getTime_up() %></p>
                
                <% if(x1 != null &&( x1.getRole_id() == 0||x1.getId()==blog.getIdacount())) { %>
                  <h4><span class="read-more" onclick="window.location.href='confirmDeleteB.jsp?id=<%= blog.getIdBlog() %>'">DELETE</span></h4>
                <% } %>
            </div>
        <% } %>
    </div>
    
    <div class="pagination">
    <% for (int i = 1; i <= totalPages; i++) { %>
        <a href="includeBlog.jsp?page=<%= i %>&search=<%= searchQuery != null ? searchQuery : "" %>&type=<%= typeQuery != null ? typeQuery : "" %>" class="<%= (i == pageNumber) ? "active" : "" %>"><%= i %></a>
    <% } %>
    
</div><%}%>
</body>
</html>
