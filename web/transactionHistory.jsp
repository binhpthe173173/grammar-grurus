<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Transaction History</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>
            body {
                font-family: Arial, sans-serif;
                padding: 0px;
                background-color: #f8f9fa;
            }
            .table-container {
                display: flex;
                flex-wrap: wrap;
            }
            .sidebar {
                background-color: #f8f9fa;
                padding: 15px;
                border-radius: 4px;
                margin-right: 20px;
                height: 100%;
            }
            .main-content {
                flex: 1;
                padding: 20px;
                background-color: #fff;
                border-radius: 4px;
            }
            table {
                width: 100%;
                border-collapse: collapse;
            }
            .table {
                margin-bottom: 0;
            }
            th, td {
                text-align: left;
            }
            th {
                width: 15%;
            }
            .sidebar {
                height: 100vh;
                position: fixed;
                padding: 24px;
                background-color: #9AECDB;
            }
            .sidebar-brand {
                text-align: center;
                margin-bottom: 20px;
            }
            .sidebar-brand h2 {
                margin: 0;
                font-size: 28px;
                color: #9AECDB;
            }
            .main {
                margin-left: 200px;
                padding: 24px;
            }

            
            .breadcrumb a {
                text-decoration: none;
                color: #007bff;
            }
            .table thead th {
                background-color: #007bff;
                color: #fff;
            }
            .table tbody tr:nth-child(even) {
                background-color: #f2f2f2;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar" style="padding-left: 24px;padding-right: 24px;">
                    <%@ include file="sidebar.jsp" %>
                </nav>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 bg-light main">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<%= request.getContextPath() %>/Admin/homePage">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Transaction History</li>
                    </ol>
                </nav>
                <h1>Transaction History</h1>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                            <tr>
                                <th>Transaction ID</th>
                                <th>Wallet ID</th>
                                <th>Amount</th>
                                <th>Type</th>
                                <th>Description</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="transaction" items="${dao.transactions}">
                                <tr>
                                    <td>${transaction.id}</td>
                                    <td>${transaction.wallet_id}</td>
                                    <td>${transaction.amount}</td>
                                    <td>${transaction.transaction_type}</td>
                                    <td>${transaction.description}</td>
                                    <td>${transaction.transaction_date}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </main>
            </div>
        </div>
        
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </body>
</html>
