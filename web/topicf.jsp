<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="models.*"%>
<%@page import="dal.*"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Q&A Topics</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #f4f4f4;
            }
            .container {
                width: calc(100% - 400px);
                margin: 20px auto;
                display: flex;
                flex-wrap: wrap;
                gap: 20px;
                justify-content: center;
            }
            .box {
                width: calc(33.333% - 40px);
                padding: 20px;
                background-color: white;
                box-shadow: 0 2px 5px rgba(0,0,0,0.1);
                border-radius: 10px;
                transition: transform 0.3s, box-shadow 0.3s;
                text-align: center;
            }
            .box:hover {
                transform: translateY(-10px);
                box-shadow: 0 5px 15px rgba(0,0,0,0.3);
            }
            .box h3 {
                color: #007a7a;
                margin-bottom: 10px;
            }
            .box p {
                color: #333;
            }
            h1 {
                color: #007a7a; 
                text-align: center;
            }
            .read-more {
                color: #007a7a;
                cursor: pointer;
                display: inline-block;
                margin-top: 10px;
            }
        </style>
    </head>
    <body>
        <%
            TopictypeDAO topicdao = new TopictypeDAO();
            String id = (String) request.getAttribute("topicid");
            
            String text = (String) request.getAttribute("text");
            QandaDAO dao = new QandaDAO();

            
        %>
        <h1><%= topicdao.getTopicTypeById(id).getTopic() %></h1>
        
        <br><br>
        <div class="container">
            <%
                List<Qandatopic> listquestion = dao.getQandatopicsByTopicId(id);
                for (Qandatopic qanda : listquestion) {
                    String content = qanda.getContent();
                    boolean needsTruncation = content.length() > 80;
            %>
                <div class="box">
                    <h3><%= qanda.getQuestion() %></h3>
                    <p>
                        <%= needsTruncation ? content.substring(0, 90) + "..." : content %>
                    </p>
                    <% if (needsTruncation) { %>
                        <span class="read-more" onclick="window.location.href='fullContent.jsp?id=<%= qanda.getIdContent() %>'">Read More</span>
                    <% } %>
                </div>
            <% } %>
        </div>
    </body>
</html>
