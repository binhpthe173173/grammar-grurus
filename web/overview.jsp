<%-- 
    Document   : overview
    Created on : Jun 18, 2024, 10:32:54 PM
    Author     : Admin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Overview</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="Css/style.css?v=1.0">
        <style>
            .form {
                display: flex;
                justify-content: space-around;
            }
            li {
                color: green;
            }
            .container-fluid {
                display: flex;
                flex-direction: column;
                min-height: 100vh;
            }
            .editor-container {
                display: flex;
                width: 100%;
            }
            .modal-header, .modal-footer {
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .modal-footer button {
                margin: 5px;
            }
            .upload-modal-body {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                height: 150px;
                text-align: center;
            }
            .modal-title {
                text-align: center;
                width: 100%;
            }
            .modal-content {
                border-radius: 10px;
            }
        </style>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script>
            function submitForm(action) {
                var form = document.getElementById('myForm');
                var textarea = document.querySelector('textarea[name="inputText"]');
                if (textarea.value.trim() === "") {
                    alert("Please enter text before submitting.");
                    return false;
                }
                form.action = action;
                form.submit();
            }
            function validateForm() {
                var text = document.forms["grammarForm"]["text"].value;
                if (text.trim() === "") {
                    alert("Please enter text for overview check.");
                    return false;
                }
                return true;
            }
            function showUploadModal() {
                $('#uploadModal').modal('show');
            }
            function handleFileUpload() {
                var fileInput = document.getElementById('fileInput');
                if (fileInput.files.length === 0) {
                    alert("Please select a file before uploading.");
                    return false;
                }
                var form = document.getElementById('fileUploadForm');
                form.submit();
            }
        </script>
    </head>
    <body>
        <div class="container-fluid"><br/>
            <div class="menubar">
                <div class="flexde">
                    <aside class="sidebar">
                        <jsp:include page="sideBarUser.jsp" />
                    </aside>
                    <main class="editor-container">
                        <div id="main" class="form">
                            <div><form class="form-class" id="myForm" method="post">
                                    <div class="button-group">
                                        <button class="toolbar-button" type="button" onclick="submitForm('grammarFunction')">Grammar Checking <i class="fas fa-spell-check"></i> </button>
                                        <button class="toolbar-button" type="button" onclick="submitForm('spellFunction')">Spell Checking <i class="fas fa-spell-check"></i></button>
                                        <button class="toolbar-button" type="button" onclick="submitForm('sticky')">Sticky <i class="fas fa-lock"></i></button>
                                        <button class="toolbar-button" type="button" onclick="submitForm('repeatFunction')">All Repeat <i class="fas fa-redo"></i></button>
                                        <button class="toolbar-button" type="button" onclick="submitForm('cliche')">Cliche <i class="fas fa-pen-alt"></i></button>
                                        <button class="toolbar-button" type="button" onclick="submitForm('overview')">Overview <i class="fas fa-pen-alt"></i></button>
                                        <button class="toolbar-button" type="button" onclick="submitForm('alliterationFunction')">Alliteration <i class="fas fa-music"></i></button>
                                        <button class="toolbar-button" type="button" onclick="submitForm('allsentencesFunction')">Length <i class="fas fa-ruler-horizontal"></i></button>
                                        <button class="toolbar-button" type="button" onclick="submitForm('complexWordText')">Complex Word <i class="fas fa-comment-alt"></i></button><button class="toolbar-button" type="button" onclick="submitForm('consistencyText')">Consistency Word <i class="fas fa-align-center"></i></button>
                                        <button class="toolbar-button" type="button" onclick="submitForm('Text2DVA')">DVA <i class="fas fa-chart-bar"></i></button>
                                        <button class="toolbar-button" type="button" onclick="submitForm('eadabilityTextResult')">Readability <i class="fas fa-book-open"></i></button>
                                        <button class="toolbar-button" type="button" onclick="submitForm('transitionsTextEr')">Transitions Check <i class="fas fa-arrows-alt-h"></i></button>
                                        <button class="toolbar-button" type="button" onclick="showUploadModal()">Upload File <i class="fas fa-file-upload"></i></button>
                                    </div>

                                    <input type="hidden" class="text-confix" name="text1" value="${text}"/>
                                </form>
                                <form name="grammarForm" action="overview" method="post" onsubmit="return validateForm()">
                                    <textarea name="inputText" rows="20" cols="100" >${inputText!= null ? inputText : ""}</textarea> <br/>
                                    <input type="submit" value="Check" />
                                </form></div>
                    </main>
                    <aside class="editor-notes" >
                        <h1>Overview Result</h1>
                        <ul>
                            <c:forEach items="${result}" var="i">
                                <div class="note" ><li>${i}</li></div>
                                    </c:forEach>
                        </ul></aside>
                </div>
            </div></div>
        <!-- Modal for File Upload -->
        <div id="uploadModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Upload File</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body upload-modal-body">
                        <form id="fileUploadForm" method="post" action="fileUpload" enctype="multipart/form-data">
                            <input type="file" id="fileInput" name="file" accept=".docx">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="handleFileUpload()">Upload</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
