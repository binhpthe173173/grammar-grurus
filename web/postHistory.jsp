<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Post History</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #f8f9fa;
            }
            .container-fluid {
                padding: 20px;
            }
            .post-content {
                max-width: 100%;
                overflow-wrap: break-word;
            }
            .error-table {
                margin-top: 10px;
                width: 100%;
            }
            .breadcrumb {
                background-color: #e9ecef;
            }
            .breadcrumb-item + .breadcrumb-item::before {
                content: ">";
                padding: 0 5px;
                color: #6c757d;
            }
            .breadcrumb a {
                text-decoration: none;
                color: #007bff;
            }
            .breadcrumb a:hover {
                text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <h1 class="mb-4">Post History</h1>
            <c:if test="${id == null}">
                <div class="table-responsive">
                    <div class="home-button">
                        <a href="./grammarFunction" class="btn btn-primary btn-lg">
                            <i class="fas fa-arrow-left"></i> Back
                        </a>
                    </div> <br/>
                    <table class="table table-bordered table-striped">
                        <thead class="thead-light">
                            <tr>
                                <th>Post Title</th>
                                <th>Time Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="post" items="${postHistory}">
                                <tr>
                                    <td class="post-content">
                                        <a href="postHistory?id=${post.id}">${post.title == null ? "Untitled" : post.title}</a> -
                                        <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#renameModal" data-post-id="${post.id}" data-post-title="${post.title == null ? 'Untitled' : post.title}">
                                            <i class="fas fa-pencil-alt"></i>
                                        </button>
                                    </td>
                                    <td>${post.time_created}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:if>
            <c:if test="${id != null}">
                <div class="home-button">
                        <a href="./postHistory" class="btn btn-primary btn-lg">
                            <i class="fas fa-arrow-left"></i> Back
                        </a>
                    </div> <br/>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead class="thead-light">
                            <tr>
                                <th>Post Content</th>
                                <th>Fixed Content</th>
                                <th>Errors</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="post-content" style="vertical-align: top">${post.content}</td>
                                <td>${post.fixed_content}</td>
                                <td>
                                    <c:if test="${not empty errors}">
                                        <table class="table table-sm error-table">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Error Content</th>
                                                    <th>Suggested Fix</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="error" items="${errors}">
                                                    <tr>
                                                        <td>${error.content}</td>
                                                        <td>${error.suggest}</td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </c:if>
                                    <c:if test="${empty errors}">
                                        No errors found.
                                    </c:if>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </c:if>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="renameModal" tabindex="-1" role="dialog" aria-labelledby="renameModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="renameModalLabel">Rename Post</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="postTitle" class="col-form-label">Post Title:</label>
                                <input type="text" class="form-control" id="postTitle" name="postTitle">
                                <input type="hidden" id="postId" name="postId" value="${post.id}">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script>
                    $('#renameModal').on('show.bs.modal', function (event) {
                        var button = $(event.relatedTarget);
                        var postId = button.data('post-id');
                        var postTitle = button.data('post-title');

                        var modal = $(this);
                        modal.find('.modal-title').text('Rename Post: ' + postTitle);
                        modal.find('.modal-body #postTitle').val(postTitle);
                        modal.find('.modal-body #postId').val(postId);
                    });
        </script>
    </body>
</html>
