<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="Css/menu.css">
        <link rel="stylesheet" type="text/css" href="Css/style.css?v=1.0">
        <style>
             .pwa-selector {
            position: relative;
            display: inline-block;
        }

        .title {
            color: blue;
            display: block;
        }

        .dropdown-content {
            display: none;
            text-align: center;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 350px;
            background: #9AECDB;
            z-index: 1;
            border: 1px solid;
            border-radius: 5px;
        }

        .pwa-selector:hover .dropdown-content {
            display: block;
        }
        .btn-primary{
            width: 100%;
            padding: 10px;
            border-radius: 5px;
        }
        .dropdown-menu{
            width: 250px;
            overflow-x: scroll;
        }
        
#barContainer {
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  max-width: 400px;
  padding: 10px;
  background-color: #007bff; /* Background color similar to your image */
}

.bar-container {
  display: flex;
  align-items: center;
  margin-bottom: 10px;
  width: 100%;
}

.label {
  flex: 1;
  font-weight: bold;
  margin-right: 10px;
}

.bar {
  height: 20px;
  background-color: #10b981; /* Adjust bar color as needed */
  color: #9AECDB;
  text-align: center;
  line-height: 20px;
  white-space: nowrap;
  overflow: hidden;
}
.dropdown-menu{
    position: static;
        padding: 10px;
}

        </style>
        <script>
            function submitForm(action) {
                var form = document.getElementById('myForm');
                var input = document.querySelector('input[name="inputText"]');
                if (input.value.trim() === "") {
                    alert("Please enter text before submitting.");
                    return false;
                }
                form.action = action;
                form.submit();
            }

   document.addEventListener('DOMContentLoaded', function () {
  const pwaSpans = document.querySelectorAll('.pwa');

  pwaSpans.forEach(span => {
    const titleText = span.getAttribute('title');
    if (titleText !== '') {
      span.style.cssText = 'border: none;  border-bottom: 2px solid blue;';
    }

    const dropdown = document.createElement('div');
    dropdown.classList.add('dropdown-content');
    dropdown.style.display = 'none';

    const title = document.createElement('div');
    title.textContent = titleText;
    dropdown.appendChild(title);

    span.appendChild(dropdown);

    span.addEventListener('mouseenter', function () {
      dropdown.style.display = 'block';
    });

    span.addEventListener('mouseleave', function () {
      dropdown.style.display = 'none';
    });
  });
});
$(document).ready(function () {
                $('.dropdown').on('show.bs.dropdown', function () {
                    var $dropdowns = $('.dropdown');
                    var $drodowns1 = $('.dropdown1');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown').on('hide.bs.dropdown', function () {
                    $('.dropdown1').css('margin-top', 0);
                });
            });
            $(document).ready(function () {
                $('.dropdown1').on('show.bs.dropdown1', function () {
                    var $dropdowns = $('.dropdown1');
                    var $drodowns1 = $('.dropdown2');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown1').on('hide.bs.dropdown1', function () {
                    $('.dropdown2').css('margin-top', 0);
                });
            });
            $(document).ready(function () {
                $('.dropdown2').on('show.bs.dropdown2', function () {
                    var $dropdowns = $('.dropdown2');
                    var $drodowns1 = $('.dropdown3');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown2').on('hide.bs.dropdown1', function () {
                    $('.dropdown3').css('margin-top', 0);
                });
            });
    function updateBarWidths() {
  const bars = document.querySelectorAll('.bar');
  const containerWidth = document.getElementById('barContainer').offsetWidth;
  let total = 0;

  // Calculate the total value of all bars
  bars.forEach(bar => {
    const value = parseInt(bar.getAttribute('data-value'), 10);
    total += value;
  });

  // Set each bar's width as a percentage of the container's width based on its proportion of the total
  bars.forEach(bar => {
    const value = parseInt(bar.getAttribute('data-value'), 10);
    const widthPercentage = (value / total) * containerWidth;
    bar.style.width = `${widthPercentage}px`;
  });
}

// Call update function initially
updateBarWidths();

// Example: Changing data-value dynamically after 3 seconds
setTimeout(() => {
  const bars = document.querySelectorAll('.bar');
  bars.forEach((bar, index) => {
    const newValue = Math.floor(Math.random() * 100); // Example: updating with specific values
    bar.setAttribute('data-value', newValue);
    bar.textContent = `Updated Value: ${newValue}`; // Updating the text content for example
  });
  updateBarWidths(); // Update widths after changing data-values
}, 3000);
function showUploadModal() {
            $('#uploadModal').modal('show');
        }

        function handleFileUpload() {
            var fileInput = document.getElementById('fileInput');
            if (fileInput.files.length === 0) {
                alert("Please select a file before uploading.");
                return false;
            }
            var form = document.getElementById('fileUploadForm');
            form.submit();
        }
            function handleNonSubscribe() {
                window.alert("You have to subscribe to use this function");
            }
        </script>
    </head>
    <body>
        
        <div class="container"><br/>
            <div class="menubar">
            <div class="flexde">
        <% 
        List<String> error = (List<String>) request.getAttribute("num43");
        List<Integer> error2 = (List<Integer>) request.getAttribute("num45");
        List<Integer> error3 = (List<Integer>) request.getAttribute("num55");
        %>

       <aside class="sidebar">
            <ul>
                <jsp:include page="sideBarUser.jsp" />
            </ul>
        </aside>
        <br/>
        <main class="editor-container"><form class="form-class" id="myForm" method="post">
                <div class="button-group">
                                <button class="toolbar-button" type="button" onclick="submitForm('grammarFunction')">Grammar Checking <i class="fas fa-spell-check"></i> </button>
                                <button class="toolbar-button" type="button" onclick="submitForm('spellFunction')">Spell Checking <i class="fas fa-spell-check"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('sticky')">Sticky <i class="fas fa-lock"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('repeatFunction')">All Repeat <i class="fas fa-redo"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('cliche')">Cliche <i class="fas fa-pen-alt"></i></button>
                                <c:if test="${ac.role_id==1}">
                                    <button class="toolbar-button" type="button" onclick="handleNonSubscribe()">Overview <i class="fas fa-pen-alt"></i></button>
                                </c:if>
                                <c:if test="${ac.role_id==2 || ac.role_id==0}">
                                    <button class="toolbar-button" type="button" onclick="submitForm('overview')">Overview <i class="fas fa-pen-alt"></i></button>
                                </c:if>
                                <button class="toolbar-button" type="button" onclick="submitForm('alliterationFunction')">Alliteration <i class="fas fa-music"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('allsentencesFunction')">Length <i class="fas fa-ruler-horizontal"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('complexWordText')">Complex Word <i class="fas fa-comment-alt"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('consistencyText')">Consistency Word <i class="fas fa-align-center"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('Text2DVA')">DVA <i class="fas fa-chart-bar"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('eadabilityTextResult')">Readability <i class="fas fa-book-open"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('transitionsTextEr')">Transitions Check <i class="fas fa-arrows-alt-h"></i></button>
                                <c:if test="${ac.role_id==1}">
                                    <button class="toolbar-button" type="button" onclick="handleNonSubscribe()">Upload File <i class="fas fa-file-upload"></i></button>
                                </c:if>
                                <c:if test="${ac.role_id==2 || ac.role_id==0}">
                                    <button class="toolbar-button" type="button" onclick="showUploadModal()">Upload File <i class="fas fa-file-upload"></i></button>
                                </c:if>
                            </div>
                <input type="hidden" class="text-confix" name="inputText" value="${inputText}"/>
            </form>
            <div class="text-decoration" >${text1}</div><br/>
        </main>
        <aside class="editor-notes" >
            <div class="dropdown"><button style="color: #ffffff "class="btn-primary dropdown-toggle" type="button" data-toggle="dropdown">${num1}<span class="caret"></span></button></div>
            <div class="dropdown"><button style="color: #ffffff "class="btn-primary dropdown-toggle" type="button" data-toggle="dropdown">${num2}<span class="caret"></span></button></div>
            <div class="dropdown"><button style="color: #f9f9f9 "class="btn-primary dropdown-toggle" type="button" data-toggle="dropdown">${num3}<span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a><br/>${num35}</a></li>
                </ul>
            </div>
            <div class="dropdown1"><button style="color: #f9f9f9 "class="btn-primary dropdown-toggle" type="button" data-toggle="dropdown">${num4}<br/>${num41}<br/><% for (int i = 0; i < error.size(); i++) { %><div id="barContainer" style="width: 100%; max-width: 400px; padding: 10px; background-color: #eaf3f4;"> <div class="bar-container" data-value="<%= error2.get(i) %>"><span class="label" style="color: red"><%= error.get(i) %>:</span><div class="bar"><%= error2.get(i) %></div></div></div><% } %><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    
    <li><a>${num48}</a></li>

                </ul>
            </div>
            <div class="dropdown2"><button style="color: #f9f9f9 "class="btn-primary dropdown-toggle" type="button" data-toggle="dropdown">${num5}<br/><% for (int i = 0; i < error3.size(); i++) { %><div id="barContainer" style="width: 100%; max-width: 400px; padding: 10px; background-color: #eaf3f4;"> <div class="bar-container" data-value="<%= error3.get(i) %>"><span class="label" style="color: red"><%= error3.get(i) %>:</span><div class="bar"><%= error3.get(i) %></div></div></div><% } %><span class="caret"></span></button></div>
            </div>
        </aside>
</div>
        </div>
             <!-- Modal for File Upload -->
    <div id="uploadModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload File</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body upload-modal-body">
                    <form id="fileUploadForm" method="post" action="fileUpload" enctype="multipart/form-data">
                        <input type="file" id="fileInput" name="file" accept=".docx" />
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="handleFileUpload()">Upload</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
