<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="vi">
    <head>
        <title>Forgot</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f4f4f4;
                margin: 0;
                padding: 0;
                display: flex;
                justify-content: center;
                align-items: center;
                height: 100vh;
            }
            .container {
                background-color: #fff;
                padding: 20px;
                border-radius: 10px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                width: 400px;
                max-width: 100%;
                text-align: center;
            }
            h3 {
                margin-top: 0;
                text-align: center;
            }
            .message {
                margin-bottom: 20px;
                padding: 10px;
                border-radius: 5px;
            }
            .message.error {
                background-color: #f8d7da;
                color: #721c24;
                border: 1px solid #f5c6cb;
            }
            .message.success {
                background-color: #d4edda;
                color: #155724;
                border: 1px solid #c3e6cb;
            }
            form {
                display: flex;
                flex-direction: column;
                margin-bottom: 15px;
            }
            label {
                margin-bottom: 5px;
                font-weight: bold;
            }
            input[type="text"] {
                padding: 10px;
                margin-bottom: 15px;
                border: 1px solid #ccc;
                border-radius: 5px;
                width: calc(100% - 22px);
            }
            input[type="submit"] {
                padding: 10px;
                border: none;
                border-radius: 5px;
                background-color: #34d399;
                color: #fff;
                font-weight: bold;
                cursor: pointer;
                transition: background-color 0.3s;
                width: 100%;
            }
            input[type="submit"]:hover {
                background-color: #0056b3;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h3 style="color: darkslateblue ">Forgot PassWord </h3>
            <% 
                String ms = (String) request.getAttribute("ms");
                if (ms != null) {
                    String messageClass = ms.contains("pass") ? "success" : "error";
            %>
            <% 
                    // Xóa thuộc tính ms sau khi hiển thị
                    request.removeAttribute("ms");
                %>
                <div class="message <%= messageClass %>">
                    <%= ms %>
                </div>
            <% 
                }
            %>
            <form action="Forgot" method="get">
                <input type="text" id="email" name="nameac"  placeholder="Enter Name Account" required>

                <input type="text" id="email" name="email"  placeholder="Enter Your Mail" required>
                <input type="submit" value="Submit">
            </form>
        </div>
    </body>
</html>
