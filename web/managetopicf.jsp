<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="models.*"%>
<%@page import="dal.*"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Q&A Topics</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #f4f4f4;
            }
            .container {
                width: calc(100% - 400px);
                margin: 20px auto;
                display: flex;
                flex-wrap: wrap;
                gap: 20px;
                justify-content: center;
            }
            .box {
                width: calc(33.333% - 40px);
                padding: 20px;
                background-color: white;
                box-shadow: 0 2px 5px rgba(0,0,0,0.1);
                border-radius: 10px;
                transition: transform 0.3s, box-shadow 0.3s;
                text-align: center;
                position: relative; /* Để định vị nút sửa và xóa */
            }
            .box:hover {
                transform: translateY(-10px);
                box-shadow: 0 5px 15px rgba(0,0,0,0.3);
            }
            .box h3 {
                color: #007a7a;
                margin-bottom: 10px;
            }
            .box p {
                color: #333;
            }
            h1 {
                color: #007a7a;
                text-align: center;
            }
            .read-more {
                color: #007a7a;
                cursor: pointer;
                display: inline-block;
                margin-top: 10px;
                text-decoration: underline;
            }
            .btn-container {
                display: flex;
                justify-content: center;
                gap: 10px;
                margin-top: 10px;
            }
            .edit-btn, .delete-btn {
                padding: 8px 16px;
                background-color: #007a7a;
                color: white;
                border: none;
                cursor: pointer;
                border-radius: 5px;
                text-decoration: none;
                transition: background-color 0.3s;
            }
            .edit-btn:hover, .delete-btn:hover {
                background-color: #005959;
            }

            /* Modal styles */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            .modal-content {
                background-color: #fefefe;
                margin: 15% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 50%; /* Could be more or less, depending on screen size */
                border-radius: 10px;
                box-shadow: 0 4px 8px rgba(0,0,0,0.2), 0 6px 20px rgba(0,0,0,0.15);
                text-align: center;
            }

            .modal h2 {
                color: #007a7a;
                margin-bottom: 20px;
            }

            .modal-buttons {
                display: flex;
                justify-content: center;
                gap: 20px;
                margin-top: 20px;
            }

            .modal-buttons a {
                padding: 8px 16px;
                background-color: #007a7a;
                color: white;
                border: none;
                cursor: pointer;
                border-radius: 5px;
                text-decoration: none;
                transition: background-color 0.3s;
            }

            .modal-buttons a:hover {
                background-color: #005959;
            }
        </style>
    </head>
    <body>
        <%
            TopictypeDAO topicdao = new TopictypeDAO();
            String id = (String) request.getAttribute("topicid");
            QandaDAO dao = new QandaDAO();
        %>
        <h1><%= topicdao.getTopicTypeById(id).getTopic() %></h1>

        <br><br>
        <div class="container">
            <%
                List<Qandatopic> listquestion = dao.getQandatopicsByTopicId(id);
                for (Qandatopic qanda : listquestion) {
                    String content = qanda.getContent();
                    boolean needsTruncation = content.length() > 80;
            %>
            <div class="box">
                <h3>ID:<%= qanda.getIdContent() %></h3>

                <h3><%= qanda.getQuestion() %></h3>
                <p>
                    <%= needsTruncation ? content.substring(0, 80) + "..." : content %>
                </p>
                <% if (needsTruncation) { %>
                <span class="read-more" onclick="window.location.href = 'fullContent.jsp?id=<%= qanda.getIdContent() %>'">Read More</span>
                <% } %>
                <div class="btn-container">
                    <a href="editQandatopic?id=<%= qanda.getIdContent() %>" class="delete-btn">Edit</a>
                    <a href="deleteQandatopic?id=<%= qanda.getIdContent() %>" class="delete-btn">Delete</a>
                </div>
            </div>
            <% } %>
        </div>
    </body>  
</html>
