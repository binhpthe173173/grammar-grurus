<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="models.*"%>
<%@page import="dal.*"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Contact Us</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }
        .contact-container {
            background-color: white;
            border-radius: 10px;
            box-shadow: 0 0 20px rgba(0,0,0,0.1);
            width: 800px;
            padding: 40px;
        }
        .contact-header {
            text-align: center;
            margin-bottom: 20px;
        }
        .contact-header h2 {
            color: #008080;
            margin: 0;
        }
        .contact-header p {
            color: #808080;
            margin: 0;
            font-size: 14px;
        }
        .contact-form {
            display: flex;
            flex-direction: column;
        }
        .contact-form label {
            margin-bottom: 5px;
            font-size: 14px;
        }
        .contact-form input,
        .contact-form textarea,
        .contact-form select {
            margin-bottom: 20px;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            font-size: 14px;
            width: 100%;
        }
        .contact-form textarea {
            resize: none;
            height: 100px;
        }
        .contact-form button {
            background-color: #f26c27;
            color: white;
            border: none;
            border-radius: 5px;
            padding: 10px;
            cursor: pointer;
            font-size: 16px;
        }
        .contact-form button:hover {
            background-color: #e65c19;
        }
        .faq-container {
            margin-left: 40px; /* Increased margin */
        }
        .faq-container h4 {
            color: #008080;
            margin-bottom: 10px;
        }
        .faq-container a {
            color: #008080;
            text-decoration: none;
            margin-bottom: 10px;
            display: block;
        }
        .faq-container a:hover {
            text-decoration: underline;
        }
        .form-section {
            display: flex;
            justify-content: space-between;
            margin-bottom: 40px; /* Increased margin */
        }
        .form-section .form-group {
            flex: 1;
            margin-right: 20px;
        }
        .form-section .form-group:last-child {
            margin-right: 0;
        }
    </style>
</head>
<body>
    
    <%
       Account x = (Account) session.getAttribute("us");
       String ms = (String)request.getAttribute("ms");
       if(x==null)
       {
    %>
    <h2 corlor="green">Please <a href="login.jsp">Login</a> To Use Many Other Interesting Features</h2>
          

    <%
    }else{
      %>
    <div class="contact-container">
        <div class="contact-header">
            <h2>Questions and Comments</h2>
            </br>
            <%if(ms!=null){%>
            <p style="color: green"><%=ms%></p>
            <%}%>
           
        </div>
        <div class="form-section">
            <form class="contact-form" method="post" action="contactus">
               
                <textarea id="message" name="message" required placeholder="Enter your question here"></textarea>

                <div class="form-section">
                    <div class="form-group">
                        <label for="name">Your Name</label>
                        <input type="text" id="name" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Your Email Address</label>
                        <input type="email" id="email" name="email" required>
                    </div>
                </div>

                <label for="query-type">What kind of query do you have?</label>
                <select id="query-type" name="query" required>
                    <option value="">Choose type of your query</option>
                    <option value="general">General</option>
                    <option value="support">Support</option>
                    <option value="feedback">Feedback</option>
                    <option value="Other">Other</option>
                </select>

                <button type="submit">Send Message</button>
            </form>
            <div class="faq-container">
                <h4>Related Questions</h4>
                <a href="#">Is GrammarGurus available in other languages?</a>
                <a href="#">GrammarGurus ribbon disappeared from Word</a>
                <a href="#">Can I upgrade my license?</a>
                <a href="#">How do I uninstall the GrammarGurus Add-In from Microsoft Word?</a>
                <a href="#">What are 'ignore' and 'disable rule' for?</a>
                <a href="#">How do I make a word cloud?</a>
            </div>
        </div>
    </div><%}%>
</body>
</html>
