<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="jakarta.servlet.http.HttpSession" %>
<%@page import="models.Account" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <title>Subscriptions</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                line-height: 1.6;
                padding: 20px;
                background-color: #f4f4f9;
            }
            h1 {
                color: #333;
            }
            .subscription-container {
                display: grid;
                flex-wrap: wrap;
                justify-content: space-around;
                grid-template-columns: repeat(5, 1fr);
                gap: 15px;
            }
            .subscription-card {
                background: #fff;
                border: 3px solid #ddd;
                border-radius: 8px;
                box-shadow: 0 2px 4px rgba(0,0,0,0.1);
                margin: 20px;
                padding: 20px;
                width: 300px;
                text-align: center;
            }
            .subscription-card h2 {
                margin-top: 0;
                color: #007bff;
            }
            .subscription-card p {
                color: #555;
            }
            .subscription-card button {
                padding: 10px 20px;
                cursor: pointer;
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 4px;
                margin-top: 10px;
            }
            .subscription-card button:hover {
                background-color: #0056b3;
            }
            p {
                text-align: left;
            }
            .home-button {
                display: flex;
                justify-content: left;
                align-items: center;
                padding-left: 22px;
            }
            body {
                font-family: Arial, sans-serif;
                margin: 20px;
            }
            .container {
                max-width: 1000px;
                margin: auto;
                border: 1px solid #ddd;
                padding: 20px;
            }
            .header, .footer {
                text-align: center;
                margin-bottom: 20px;
            }
            .content {
                display: flex;
                justify-content: space-between;
                align-items: flex-start;
            }
            .details {
                max-width: 60%;
            }
            .details p {
                margin: 5px 0;
            }
            .details p strong {
                color: red;
            }
            .details .highlight {
                color: red;
                font-weight: bold;
            }
            .qr-code {
                text-align: center;
            }
            .qr-code img {
                max-width: 200px;
                height: auto;
            }
        </style>
    </style>
    <script>
        function confirmSubscription(subscriptionId, subscriptionName) {
            if (confirm("Are you sure you want to subscribe to " + subscriptionName + "?")) {
                window.location.href = 'processSubscription?subscriptionId=' + subscriptionId;
            }
        }
    </script>
</head>
<body>
    <h1 class="text-center" >Available Subscriptions</h1><br />
    <div class="home-button">
        <a href="./grammarFunction" class="btn btn-primary btn-lg">
            <i class="fas fa-arrow-left"></i> Back
        </a>
    </div>
    <div class="subscription-container">
        <c:forEach var="sub" items="${dao.subscriptions}">
            <c:if test="${sub.status}">
                <div key="${sub.id}" class="subscription-card">
                    <h2>${sub.name}</h2><br />
                    <p><b>Price:</b> ${sub.price} VND</p>
                    <p><b>Duration:</b> ${sub.duration} ${sub.duration == 1 ? "day" : "days"}</p>
                    <p>${sub.description}</p>
                    <button onclick="confirmSubscription(${sub.id}, '${sub.name}')">Subscribe Now</button>
                </div>
            </c:if>
        </c:forEach>
    </div>
    <div class="container">
        <div class="header">
            <h2>Payment methods</h2>
            <p>You need to deposit money into your wallet to make transactions on Grammar Gurus</p>
        </div>
        <div class="content">
            <div class="details">
                <h3>Account accepting deposits</h3>
                <p><strong>CARD OWNER:</strong> VU THANH NAM</p>
                <p><strong>ACCOUNT NUMBER:</strong> 7555568699999</p>
                <p><strong>BANK:</strong> MBBANK</p>
                <% HttpSession ses = request.getSession();
                Account ac = (Account)ses.getAttribute("us");
                request.setAttribute("ac", ac);
                %>
                <p><strong>TRANSFER CONTENT:</strong> GGS${ac.username}</p>
                <p>Please send the correct remittance content to avoid not receiving the money. If the transfer is incorrect, please contact Admin for support.</p>
                <p>After successful transfer, please wait for at least 2 minute for the system to process.</p>
            </div>
            <div class="qr-code">
                <h4>OR SCAN THE FOLLOWING QR CODE TO AUTOMATICALLY FILL IN INFORMATION</h4>
                <img src="qr-code.png" alt="QR Code">
            </div>
        </div>
    </div>
    <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
