<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Footer Example</title>
    <style>
        body haha{
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            display: flex;
            flex-direction: column;
            min-height: 100vh;
        }
        .footer {
            display: flex;
            justify-content: space-around;
            background-color: #f9f9f9;
            padding: 20px;
            border-top: 1px solid #e0e0e0;
        }
        .footer-column {
            flex: 1;
            margin: 0 10px;
        }
        .footer-column h3 {
            color: #333;
        }
        .footer-column ul {
            list-style: none;
            padding: 0;
        }
        .footer-column ul li {
            margin: 5px 0;
        }
        .footer-column ul li a {
            color: #007A7A;
            text-decoration: none;
        }
        .footer-column ul li a:hover {
            text-decoration: underline;
        }
        .footer .social-icons {
            display: flex;
            gap: 10px;
        }
        .footer .social-icons a {
            color: #007A7A;
            text-decoration: none;
            font-size: 20px;
        }
    </style>
</head>
<body class="haha">
    <div class="footer">
        <div class="footer-column">
            <h3>Integrations</h3>
            <ul>
                <li><a href="#">Desktop Everywhere for Windows</a></li>
                <li><a href="#">Desktop Everywhere for Mac</a></li>
                <li><a href="#">Chrome Extension (Firefox, Edge)</a></li>
                <li><a href="#">Google Docs</a></li>
                <li><a href="#">Microsoft Office</a></li>
                <li><a href="#">Desktop Editor</a></li>
            </ul>
        </div>
        <div class="footer-column">
            <h3>Features</h3>
            <ul>
                <li><a href="#">All Features</a></li>
                <li><a href="#">Grammar Checker</a></li>
                <li><a href="#">Plagiarism Checker</a></li>
                <li><a href="#">For Teams</a></li>
            </ul>
        </div>
        <div class="footer-column">
            <h3>Learn More</h3>
            <ul>
                <li><a href="#">Pricing</a></li>

                <li><a href="showBlog.jsp">Blog</a></li>
                <li><a href="#">Grammar Guide</a></li>
                <li><a href="#">GrammarGurus API</a></li>

                <li><a href="#">Grammar Checker Component</a></li>
            </ul>
        </div>
        <div class="footer-column">
            <h3>Company</h3>
            <ul>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Careers & Culture</a></li>
                <li><a href="#">Affiliates</a></li>
                <li><a href="#">Terms of Service</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Cookie Policy</a></li>
                <li><a href="#">GDPR</a></li>
                <li><a href="#">Sitemap</a></li>
            </ul>
        </div>
        <div class="footer-column">
            <h3>Contact Us</h3>
            <p>Drop us a line</p>
            <p>or let's stay in touch via :</p>
            <div class="social-icons">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
                <a href="#"><i class="fa fa-youtube"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>
            </div>
        </div>
    </div>
</body>
</html>
