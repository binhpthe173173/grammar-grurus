<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Subscription Failure</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                line-height: 1.6;
                padding: 20px;
                background-color: #f4f4f9;
                text-align: center;
            }
            h1 {
                color: red;
            }
        </style>
    </head>
    <body>
        <h1>Subscription Failed!</h1>
        <p>There was an issue processing your subscription. Please try again later.</p>
        <a href="subscriptions">Back to Subscriptions</a>
    </body>
</html>
