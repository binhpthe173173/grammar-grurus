<%-- 
    Document   : sideBarUser
    Created on : Jul 12, 2024, 6:27:08 PM
    Author     : admin
--%>
<%@ page import="jakarta.servlet.http.HttpSession" %>
<%@ page import="models.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="./Css/styles.css?v=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    </head>
    <body>
            
       
        <aside class="sidebar">
            <div class="logo" style="font-size: 20px;"><a href="grammarFunction"><h2>GrammarGurus</h2></a></div>
            <ul>
               
                <li><a href="grammarFunction"><button class="new-page"><i class="fas fa-file-alt"></i>New Document</button></a></li>
                <li><a href="postHistory"><button class="new-page"><i class="fas fa-history"></i>My First History</button></a></li>
                <c:if test="${ac.role_id !=0}">
                <li><a href="subscriptions"><button class="new-page"><i class="fas fa-dollar-sign"></i>Subscription</button></a></li>
                </c:if>
                <li><a href="ProfileUsers"><button class="new-page"><i class="fas fa-user"></i>My Profile</button></a></li>
                <li><a href="userBlog.jsp"><button class="new-page"><i class="fas fa-blog"></i>Blog</button></a></li>
                <li><a href="review.jsp"><button class="new-page"><i class="fas fa-star-half-alt"></i>Review</button></a></li>
                <li><a href="listFAQ.jsp"><button class="new-page"><i class="fas fa-question-circle"></i>FAQ</button></a></li>
                <li><a href="grammarGuideList"><button class="new-page"><i class="fas  fa-blog"></i>Grammar Guide</button></a></li>
                <c:if test="${ac.role_id==0}">
                <li><a href="<%= request.getContextPath() %>/Admin/homePage"><button class="new-page"><i class="fas fa-user-shield"></i>Change to Admin</button></a></li>
                </c:if>
            </ul>
        </aside>
    </body>
</html>
