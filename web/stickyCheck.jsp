<%@ page import="org.json.JSONArray" %>
<%@ page import="org.json.JSONObject" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Sticky Check</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <style>
        .error {
                text-decoration: underline;
                text-decoration-color: red;
                text-decoration-style: double;
                cursor: pointer;
            }
            #main {
                background: #fff;
                color: #333;
                padding: 20px;
                margin-top: 20px;
                border-radius: 8px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
           
           
            .suggestion {
                background-color: #f9f9f9;
                border-left: 5px solid #ff6f61;
                margin-bottom: 10px;
                padding: 10px;
                border-radius: 5px;
            }
            .sticky {
                text-decoration: underline;
                cursor: pointer;
                color: #d9534f;
            }
        .container-fluid {
            display: flex;
            flex-direction: column;
            min-height: 100vh;
        }
        .editor-container {
            display: flex;
            width: 100%;
        }
        .modal-header, .modal-footer {
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .modal-footer button {
            margin: 5px;
        }
        .upload-modal-body {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            height: 150px;
            text-align: center;
        }
        .modal-title {
            text-align: center;
            width: 100%;
        }
        .modal-content {
            border-radius: 10px;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script>
         function submitForm(action) {
                var form = document.getElementById('myForm');
                var input = document.querySelector('input[name="inputText"]');
                if (input.value.trim() === "") {
                    alert("Please enter text before submitting.");
                    return false;
                }
                form.action = action;
                form.submit();
            }
            function validateForm() {
                var text = document.forms["grammarForm"]["text"].value;
                if (text.trim() === "") {
                    alert("Please enter text for grammar check.");
                    return false;
                }
                return true;
            }
            function showHint(hint) {
                alert(hint);
            }
        function showUploadModal() {
            $('#uploadModal').modal('show');
        }
        function handleFileUpload() {
            var fileInput = document.getElementById('fileInput');
            if (fileInput.files.length === 0) {
                alert("Please select a file before uploading.");
                return false;
            }
            var form = document.getElementById('fileUploadForm');
            form.submit();
        }
            function handleNonSubscribe() {
                window.alert("You have to subscribe to use this function");
            }
    </script>
</head>
    
   <body>
        
        <% 
            String inputText = (String) request.getAttribute("inputText");
            String errorsAndSuggestionsJson = (String) request.getAttribute("errorsAndSuggestions");
            String errorMessage = (String) request.getAttribute("errorMessage");
        %>
        <div class="container-fluid">
            <div class="flexde">
                <aside class="sidebar">
                     <jsp:include page="sideBarUser.jsp" />
                </aside>
                <main class="editor-container">
                    <form class="form-class" id="myForm" method="post" onsubmit="return validateForm()">
                        <div class="button-group">
                                <button class="toolbar-button" type="button" onclick="submitForm('grammarFunction')">Grammar Checking <i class="fas fa-spell-check"></i> </button>
                                <button class="toolbar-button" type="button" onclick="submitForm('spellFunction')">Spell Checking <i class="fas fa-spell-check"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('sticky')">Sticky <i class="fas fa-lock"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('repeatFunction')">All Repeat <i class="fas fa-redo"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('cliche')">Cliche <i class="fas fa-pen-alt"></i></button>
                                <c:if test="${ac.role_id==1}">
                                    <button class="toolbar-button" type="button" onclick="handleNonSubscribe()">Overview <i class="fas fa-pen-alt"></i></button>
                                </c:if>
                                <c:if test="${ac.role_id==2 || ac.role_id==0}">
                                    <button class="toolbar-button" type="button" onclick="submitForm('overview')">Overview <i class="fas fa-pen-alt"></i></button>
                                </c:if>
                                <button class="toolbar-button" type="button" onclick="submitForm('alliterationFunction')">Alliteration <i class="fas fa-music"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('allsentencesFunction')">Length <i class="fas fa-ruler-horizontal"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('complexWordText')">Complex Word <i class="fas fa-comment-alt"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('consistencyText')">Consistency Word <i class="fas fa-align-center"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('Text2DVA')">DVA <i class="fas fa-chart-bar"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('eadabilityTextResult')">Readability <i class="fas fa-book-open"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('transitionsTextEr')">Transitions Check <i class="fas fa-arrows-alt-h"></i></button>
                                <c:if test="${ac.role_id==1}">
                                    <button class="toolbar-button" type="button" onclick="handleNonSubscribe()">Upload File <i class="fas fa-file-upload"></i></button>
                                </c:if>
                                <c:if test="${ac.role_id==2 || ac.role_id==0}">
                                    <button class="toolbar-button" type="button" onclick="showUploadModal()">Upload File <i class="fas fa-file-upload"></i></button>
                                </c:if>
                            </div>
                        <input type="hidden" class="text-confix" name="inputText" value="${inputText}">
                    </form>
                    <% if (inputText != null && !inputText.isEmpty()) { %>
                        <div class="text-decoration">
                            <%= inputText %>
                        </div>
                        <% if (errorsAndSuggestionsJson != null && !errorsAndSuggestionsJson.isEmpty()) {
                            JSONArray errorAndSuggest = new JSONArray(errorsAndSuggestionsJson);
                            StringBuilder highlightedText = new StringBuilder();
                            for (int i = 0; i < errorAndSuggest.length(); i++) {
                                JSONObject suggestion = errorAndSuggest.getJSONObject(i);
                                int startPos = suggestion.getInt("startPos");
                                int endPos = suggestion.getInt("endPos");
                                String word = inputText.substring(startPos, endPos);
                                highlightedText.append("<span class='error' onclick='showHint(\"" + suggestion.getString("hint") + "\")'>" + word + "</span><br>");
                            }
                        %>
                        <div class="text-decoration"><%= highlightedText.toString() %></div>
                        <% } %>
                    <% } else if (errorMessage != null) { %>
                        <div class="suggestion"><%= errorMessage %></div>
                    <% } %>
                </main>
            </div>
        </div>
         <div id="uploadModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload File</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body upload-modal-body">
                    <form id="fileUploadForm" method="post" action="fileUpload" enctype="multipart/form-data">
                        <input type="file" id="fileInput" name="file" accept=".docx">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="handleFileUpload()">Upload</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>  
    </body>
</html>