<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add Subscription</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            padding: 20px;
        }
        h1, h2 {
            color: #333;
        }
        form {
            max-width: 600px;
            margin: 20px auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 8px;
            background-color: #f9f9f9;
        }
        form div {
            margin-bottom: 15px;
        }
        form label {
            display: block;
            margin-bottom: 5px;
            color: #333;
        }
        form input[type="text"],
        form input[type="number"],
        form textarea,
        form select {
            width: calc(100% - 12px);
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 4px;
            font-size: 16px;
        }
        form button {
            padding: 10px 20px;
            cursor: pointer;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 4px;
            font-size: 16px;
        }
        form button[type="submit"] {
            margin-right: 10px;
        }
        form button[type="reset"] {
            background-color: #6c757d;
            margin-right: 10px;
        }
        form button[type="button"] {
            background-color: red;
        }
        .error-message {
            color: red;
            margin-top: 5px;
        }
    </style>
</head>
<body>
    <h1>Add Subscription</h1>
    <form action="" method="post">
        <div>
            <label for="subscription-name">Name:</label>
            <input type="text" id="subscription-name" name="name" required>
        </div>
        <div>
            <label for="subscription-price">Price (VND):</label>
            <input type="number" id="subscription-price" name="price" min="0" required>
        </div>
        <div>
            <label for="subscription-duration">Duration (days):</label>
            <input type="number" id="subscription-duration" name="duration" required>
        </div>
        <div>
            <label for="subscription-status">Status:</label>
            <select id="subscription-status" name="status" required>
                <option value="1">Actived</option>
                <option value="0">Inactived</option>
            </select>
        </div>
        <div>
            <label for="subscriptionDescription">Description:</label>
            <textarea id="subscriptionDescription" name="description" rows="4" required></textarea>
        </div>
        <div>
            <button type="submit">Save</button>
            <button type="reset">Reset</button>
            <button type="button" onclick="window.location.href='manageSubscription'">Cancel</button>
        </div>
    </form>
</body>
</html>
