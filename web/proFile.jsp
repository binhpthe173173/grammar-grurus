<%@page import="models.Account" %>
<%@page import="dal.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>User Profile</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>
            body {
                font-family: 'Arial', sans-serif;
                background-color: #f9f9f9;
                margin: 0;
                padding: 0;
            }
            .profile-container {
                background-color: #ffffff;
                padding: 20px;
                border-radius: 10px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                max-width: 600px;
                margin: 40px auto;
            }
            .profile-header {
                text-align: center;
                margin-bottom: 20px;
            }
            .profile-image {
                display: block;
                margin: 0 auto 20px;
                border-radius: 50%;
                width: 150px;
                height: 150px;
                object-fit: cover;
            }
            .profile-info label {
                font-weight: bold;
                margin-right: 10px;
            }
            .profile-info span {
                display: block;
                margin-bottom: 10px;
            }
            .profile-form {
                margin-top: 20px;
                text-align: center;
            }
            .profile-form input[type="submit"] {
                background-color: #007bff;
                color: #ffffff;
                border: none;
                padding: 10px 20px;
                font-size: 16px;
                border-radius: 5px;
                cursor: pointer;
                transition: background-color 0.3s;
            }
            .profile-form input[type="submit"]:hover {
                background-color: #0056b3;
            }
            .nav-link {
                font-size: 16px;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="profile-container">
            <nav class="nav justify-content-center mb-4">
                <a class="nav-link btn btn-outline-primary" href="grammarFunction">Home</a>
            </nav>
            <div class="profile-header">
                <h2>User Profile</h2>
            </div>
            <%
                Account c = (Account) request.getAttribute("account"); 
                if(c==null){
                response.sendRedirect("login.jsp");
                }else {
            %>
            <img src="<%=c.getImage()%>" alt="User Image" class="profile-image" />
            <div class="profile-info">
                <div class="mb-3">
                    <label>Full Name:</label>
                    <span><%= c.getFull_name() %></span>
                </div>
                <div class="mb-3">
                    <label>User Name:</label>
                    <span><%= c.getUsername() %></span>
                </div>
                <div class="mb-3">
                    <label>Email:</label>
                    <span><%= c.getEmail() %></span>
                </div>
                <div class="mb-3">
                    <label>Account Created:</label>
                    <span><%= c.getTime_created() %></span>
                </div>
                <div class="mb-3">
                    <label>Balance:</label>
                    <span>${wallet.balance}</span>
                </div>
                <div class="mb-3">
                    <label>Status:</label>
                    <c:choose>
                        <c:when test="${account.role_id==1}">
                            <span>Normal</span>
                        </c:when>
                        <c:when test="${account.role_id==2}">
                            <strong class="text-success">Premium</strong> </br>
                        </c:when>
                    </c:choose>
                </div>
            </div>
            <div class="profile-form">
                    <button><a href="./changesP" class="btn btn-primary">Change Profile</a></button>
                <form action="logout" method="post">
                    <input type="submit" value="Logout" class="btn btn-danger"/>
                </form>
            </div>
            <%
                }
            %>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </body>
</html>
