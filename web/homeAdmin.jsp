<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Dashboard</title>

        <style>
            body {
                font-family: Arial, sans-serif;
                line-height: 1.6;
                padding: 0px;
                display: flex;
            }
            .sidebar {
                height: 100vh;
                position: fixed;
                padding : 24px;
                background-color: #9AECDB;
            }
            .sidebar-brand {
                text-align: center;
                margin-bottom: 20px;
            }
            .sidebar-brand h2 {
                margin: 0;
                font-size: 28px;
                color: #9AECDB;

                .main {
                    margin-left: 200px;
                    padding: 20px;
                    padding-top: 24px;
                }
                .card {
                    margin-bottom: 20px;
                }
                .breadcrumb {
                    background-color: #f8f9fa;
                    padding: 8px 15px;
                    margin-bottom: 20px;
                    border-radius: 4px;
                }
                .breadcrumb-item + .breadcrumb-item::before {
                    content: ">";
                    padding: 0 5px;
                    color: #6c757d;
                }
                .breadcrumb a {
                    text-decoration: none;
                    color: #007bff;
                }
                .breadcrumb a:hover {
                    text-decoration: underline;
                }
            </style>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script>
                $(document).ready(function () {
                    // Function to fetch number of users using AJAX
                    function fetchDashboardData() {
                        $.ajax({
                            url: '${pageContext.request.contextPath}/dashboardData', // Servlet URL
                            type: 'GET',
                            dataType: 'json',
                            success: function (data) {
                                // Update HTML with fetched data
                                $('#numberOfUsers').text(data.numberOfUsers);
                                $('#totalTransactions').text(data.totalTransactions);
                                $('#totalAmount').text(data.totalAmount);
                            },
                            error: function (xhr, status, error) {
                                console.error('Error fetching data:', error);
                            }
                        });
                    }
                    // Call fetchDashboardData on page load
                    fetchDashboardData();
                });
            </script>
        </head>
        <body>

            <div class="container-fluid">
                <div class="row">
                    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                        <%@ include file="sidebar.jsp" %>
                    </nav>

                    <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4 bg-light main" style="padding-top: 24px;">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<%= request.getContextPath() %>/Admin/homePage">Home</a></li>

                            </ol>
                        </nav>
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                            <h1 class="h2" id="dashboardTitle">Home Admin</h1>
                        </div>

                        <div id="adminContent">
                            <div class="row mb-3">
                                <div class="col-md-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Number of Users</h5>
                                            <h6 class="card-subtitle mb-2 text-muted">Current users</h6>
                                            <p class="card-text" id="numberOfUsers">Loading...</p>
                                            <p class="card-text">Updated now</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Total Transactions</h5>
                                            <h6 class="card-subtitle mb-2 text-muted">Today</h6>
                                            <p class="card-text" id="totalTransactions">Loading...</p>
                                            <p class="card-text">Updated now</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Total Amount</h5>
                                            <h6 class="card-subtitle mb-2 text-muted">Today</h6>
                                            <p class="card-text" id="totalAmount">Loading...</p>
                                            <p class="card-text">Updated now</p>
                                        </div>
                                    </div>
                                </div>
<!--                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Followers</h5>
                                            <h6 class="card-subtitle mb-2 text-muted">+45</h6>
                                            <p class="card-text">Updated now</p>
                                        </div>
                                    </div>
                                </div>
                            </div>-->

                            
                        </div>
                    </main>
                </div>
            </div>

            <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
            <script>
                document.addEventListener("DOMContentLoaded", function () {

                    var currentUrl = window.location.pathname;
                    document.querySelectorAll('.nav-link').forEach(function (link) {
                        if (link.getAttribute('href') === currentUrl) {
                            link.classList.add('active');
                        }
                    });
                });

                
                document.querySelectorAll('.nav-link').forEach(function (link) {
                    link.addEventListener('click', function () {
                        document.querySelectorAll('.nav-link').forEach(function (link) {
                            link.classList.remove('active');
                        });
                        this.classList.add('active');
                    });
                });
            </script>

        </body>
    </html>
