<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Grammar and Spelling check</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link rel="stylesheet" type="text/css" href="Css/menu.css">
        <link rel="stylesheet" type="text/css" href="Css/style.css?v=1.0">
    </head>
    <body>
        <style>
            .dropdown, .dropdown1,.dropdown2,.dropdown3 {
                margin-bottom: 10px;
            }
            .dropdown-menu {
                position: static;
                box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
                border-radius: 5px;
                width: 100%;
            }
            .dropdown.open .dropdown-menu {
                display: block;
                position: absolute;
                top: 100%;
                left: 0;
                z-index: 1000;

            }
            .btn-primary {
                background-color: #f5f5f5;
                color: #333;
                border: 1px solid #ddd;
            }
            .btn-primary:hover, .btn-primary:focus, .btn-primary:active {
                background-color: #e6e6e6;
                color: #333;
            }
            .caret {
                margin-left: 10px;
            }
            .dropdown-menu > li > a {
                padding: 10px 20px;
                color: #333;
            }
            .dropdown-menu > li > a:hover {
                background-color: #e6e6e6;
                color: #333;
            }
            .btn-primary {
                background-color: #00BFA6;
                padding: 3px 5px;
                color: #fff;
                text-transform: uppercase;
                letter-spacing: 2px;
                cursor: pointer;
                border-radius: 5px;
                border: 2px dashed #00BFA6;
                transition: .4s;
            }



            .btn-primary:hover {
                transition: .4s;
                border: 2px dashed #00BFA6;
                background-color: #fff;
                color: #00BFA6;

            }

            .btn-primary:active {
                background-color: #87dbd0;
            }
            .btnLink{
                border-radius: 5px;
                width: 100%;
            }
        </style>
        <script>
            function submitForm(action) {
                var form = document.getElementById('myForm');
                var textarea = document.querySelector('textarea[name="inputText"]');
                if (textarea.value.trim() === "") {
                    alert("Please enter text before submitting.");
                    return false;
                }
                form.action = action;
                form.submit();
            }

            $(document).ready(function () {
                $('.dropdown').on('show.bs.dropdown', function () {
                    var $dropdowns = $('.dropdown');
                    var $drodowns1 = $('.dropdown1');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown').on('hide.bs.dropdown', function () {
                    $('.dropdown1').css('margin-top', 0);
                });
            });
            $(document).ready(function () {
                $('.dropdown1').on('show.bs.dropdown1', function () {
                    var $dropdowns = $('.dropdown1');
                    var $drodowns1 = $('.dropdown2');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown1').on('hide.bs.dropdown1', function () {
                    $('.dropdown2').css('margin-top', 0);
                });
            });
            $(document).ready(function () {
                $('.dropdown2').on('show.bs.dropdown2', function () {
                    var $dropdowns = $('.dropdown2');
                    var $drodowns1 = $('.dropdown3');
                    var index = $dropdowns.index(this);
                    $drodowns1.css('margin-top', $(this).find('.dropdown-menu').outerHeight());
                });

                $('.dropdown2').on('hide.bs.dropdown1', function () {
                    $('.dropdown3').css('margin-top', 0);
                });
            });
function showUploadModal() {
            $('#uploadModal').modal('show');
        }

        function handleFileUpload() {
            var fileInput = document.getElementById('fileInput');
            if (fileInput.files.length === 0) {
                alert("Please select a file before uploading.");
                return false;
            }
            var form = document.getElementById('fileUploadForm');
            form.submit();
        }
        function handleNonSubscribe() {
                window.alert("You have to subscribe to use this function");
            }
        </script>

        <div class="container"><br/>
            <div class="menubar"></div>
            <div class="flexde">
                <% 
            List<String> errorG = (ArrayList<String>) request.getAttribute("ErrorA1");
            List<String> errorG2 = (ArrayList<String>) request.getAttribute("ErrorA2");
            List<String> errorG3 = (ArrayList<String>) request.getAttribute("ErrorA3");
            List<String> errorG4 = (ArrayList<String>) request.getAttribute("ErrorA4");
                %>

                <aside class="sidebar">
                      <jsp:include page="sideBarUser.jsp" />
                </aside>
                <main class="editor-container">
                    <form class="form-class" id="myForm" method="post">
                        <div class="button-group">
                                <button class="toolbar-button" type="button" onclick="submitForm('grammarFunction')">Grammar Checking <i class="fas fa-spell-check"></i> </button>
                                <button class="toolbar-button" type="button" onclick="submitForm('spellFunction')">Spell Checking <i class="fas fa-spell-check"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('sticky')">Sticky <i class="fas fa-lock"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('repeatFunction')">All Repeat <i class="fas fa-redo"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('cliche')">Cliche <i class="fas fa-pen-alt"></i></button>
                                <c:if test="${ac.role_id==1}">
                                    <button class="toolbar-button" type="button" onclick="handleNonSubscribe()">Overview <i class="fas fa-pen-alt"></i></button>
                                </c:if>
                                <c:if test="${ac.role_id==2 || ac.role_id==0}">
                                    <button class="toolbar-button" type="button" onclick="submitForm('overview')">Overview <i class="fas fa-pen-alt"></i></button>
                                </c:if>
                                <button class="toolbar-button" type="button" onclick="submitForm('alliterationFunction')">Alliteration <i class="fas fa-music"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('allsentencesFunction')">Length <i class="fas fa-ruler-horizontal"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('complexWordText')">Complex Word <i class="fas fa-comment-alt"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('consistencyText')">Consistency Word <i class="fas fa-align-center"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('Text2DVA')">DVA <i class="fas fa-chart-bar"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('eadabilityTextResult')">Readability <i class="fas fa-book-open"></i></button>
                                <button class="toolbar-button" type="button" onclick="submitForm('transitionsTextEr')">Transitions Check <i class="fas fa-arrows-alt-h"></i></button>
                                <c:if test="${ac.role_id==1}">
                                    <button class="toolbar-button" type="button" onclick="handleNonSubscribe()">Upload File <i class="fas fa-file-upload"></i></button>
                                </c:if>
                                <c:if test="${ac.role_id==2 || ac.role_id==0}">
                                    <button class="toolbar-button" type="button" onclick="showUploadModal()">Upload File <i class="fas fa-file-upload"></i></button>
                                </c:if>
                            </div>
                        <textarea placeholder="Start writing..."name="inputText" readonly>${inputText}</textarea>
                    </form>
                </main>
                <aside class="editor-notes" >
                    <%if(!errorG.isEmpty()){%>
                    <div class="dropdown">
                        <button class="btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Frequent 1-Word Phrases
                            <span class="caret"></span></button>

                        <ul class="dropdown-menu"><%for(String a9:errorG){%>
                            <li><a href="showErrorRepeatword?error=<%=a9%>"><%=a9%></a></li><br/><%}%>
                             <form action="showErrorRepeatword" method="post"><input type="hidden" name="inputText" value="${inputText}" /> <input type="hidden" name="idText" value="1"/> <input class="btn-primary btnLink" type="submit" value="Show all Frequent 1-Word"/></form>
                        </ul>
                           
                    </div>
                    <%}else{//
}%>
                    <%if(!errorG2.isEmpty()){%>
                    <div class="dropdown1">
                        <button class="btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Frequent 2-Word Phrases
                            <span class="caret"></span></button>

                        <ul class="dropdown-menu"><%for(String a9:errorG2){%>
                            <li><a href="showErrorRepeatword?error=<%=a9%>"><%=a9%></a></li><%}%>
                            <form action="showErrorRepeatword" method="post"><input type="hidden" name="inputText" value="${inputText}" /> <input type="hidden" name="idText" value="2"/> <input class="btn-primary btnLink" type="submit" value="Show all Frequent 2-Word"/></form>
                        </ul>
                    </div>
                    <%}else{

}%>
                    <%if(!errorG3.isEmpty()){%>
                    <div class="dropdown2">
                        <button class="btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Frequent 3-Word Phrases
                            <span class="caret"></span></button>

                        <ul class="dropdown-menu"><%for(String a9:errorG3){%>
                            <li><a href="showErrorRepeatword?error=<%=a9%>"><%=a9%></a></li><%}%>
                            <form action="showErrorRepeatword" method="post"><input type="hidden" name="inputText" value="${inputText}" /> <input type="hidden" name="idText" value="3"/> <input class="btn-primary btnLink" type="submit" value="Show all Frequent 3-Word"/></form>
                        </ul>
                    </div>
                    <%}else{

}%>
                    <%if(!errorG4.isEmpty()){%>
                    <div class="dropdown3">
                        <button class="btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Frequent 4-Word Phrases
                            <span class="caret"></span></button>

                        <ul class="dropdown-menu"><%for(String a9:errorG4){%>
                            <li><a href="showErrorRepeatword?error=<%=a9%>"><%=a9%></a></li><%}%>
                            <form action="showErrorRepeatword" method="post"><input type="hidden" name="inputText" value="${inputText}" /> <input type="hidden" name="idText" value="4"/> <input class="btn-primary btnLink" type="submit" value="Show all Frequent 4-Word"/></form>
                        </ul>
                    </div>
                    <%}else{
%><%
}%>
                </aside>

            </div>
        </div>

 <!-- Modal for File Upload -->
    <div id="uploadModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload File</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body upload-modal-body">
                    <form id="fileUploadForm" method="post" action="fileUpload" enctype="multipart/form-data">
                        <input type="file" id="fileInput" name="file" accept=".docx" />
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="handleFileUpload()">Upload</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>