<%-- 
    Document   : editFeedback
    Created on : Jul 25, 2024, 4:31:08 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="models.*" %>
<%@ page import="dal.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
                 .rating {
            display: flex;
            flex-direction: row-reverse;
            gap: 0.3rem;
        }

        .rating input {
            display: none;
        }

        .rating label {
            cursor: pointer;
        }

        .rating svg {
            width: 2rem;
            height: 2rem;
            overflow: visible;
            fill: transparent;
            stroke: #666;
            stroke-linejoin: bevel;
            stroke-dash: 0.2s;
        }

        .rating label:hover svg {
            stroke: #ffc73a;
        }

        .rating input:checked ~ label svg {
            transition: 0s;
            fill: #ffc73a;
            stroke: #ffc73a;
            stroke-opacity: 0;
            stroke-dasharray: 0;
            stroke-linejoin: miter;
            stroke-width: 8px;
        }

        form {
            max-width: 600px;
            margin: 20px auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 8px;
        }

        textarea {
            width: 100%;
            height: 100px;
            margin-bottom: 10px;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
            resize: vertical;
        }
       
        .cancel-btn{
              background-color: #007A7A;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            
            
        }
        button {
            background-color: #007A7A;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        button:hover {
            background-color: #45a049;
        }
            
            
            
        </style>
    </head>
    
    
    
    
    
    <body>
        <% Account x = (Account) session.getAttribute("us");
                       String ms = (String) request.getAttribute("ms");

     String id  =  (String)  request.getAttribute("id");
      String feedback =(String) request.getAttribute("feedback");
     int   rating = (int)request.getAttribute("rating"); 
        
        
        
        
        
        
        
        
     if(x!=null)
       {   %>


        <%
        if(feedback!=null&& id!=null&&rating!=0)
        %>

        <form class="rate-feed" action="editFeedbackUser" method="post">
            <h2>You want to edit your feedback and rating as follows 
</h2>
            <div class="rating">
              
                <input type="radio" id="star-5" name="rating" value="5" <%= (rating == 5) ? "checked" : "" %>>
                <label for="star-5">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                    </svg>
                </label>
                <input type="radio" id="star-4" name="rating" value="4" <%= (rating == 4) ? "checked" : "" %>>
                <label for="star-4">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                    </svg>
                </label>
                <input type="radio" id="star-3" name="rating" value="3" <%= (rating == 3) ? "checked" : "" %>>
                <label for="star-3">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                    </svg>
                </label>
                <input type="radio" id="star-2" name="rating" value="2" <%= (rating == 2) ? "checked" : "" %>>
                <label for="star-2">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                    </svg>
                </label>
                <input type="radio" id="star-1" name="rating" value="1" <%= (rating == 1) ? "checked" : "" %>>
                <label for="star-1">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                    </svg>
                </label>
            </div>
                <% if(ms!=null) {%>
         <p style="color: red"><%=ms%></p>
         <%ms=null;%>
         <%}%>
         <input type="hidden" name="id" value="<%= id%>" >

            <textarea name="feedback"><%= feedback %></textarea>
            <button type="submit" name="action" value="edit">Edit</button>
             <a href="javascript:history.back()" class="cancel-btn">Cancel</a>
        </form>

        <%}%>
    </body>
</html>
