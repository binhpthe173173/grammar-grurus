<%-- 
    Document   : voteAndFeedback
    Created on : Jul 14, 2024, 3:39:27 PM
    Author     : Haỉ Hoàn
--%>
<%@ page import="models.*"%>
<%@ page import="dal.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ReviewofUser</title>
    <style>
        .rating {
            display: flex;
            flex-direction: row-reverse;
            gap: 0.3rem;
        }

        .rating input {
            display: none;
        }

        .rating label {
            cursor: pointer;
        }

        .rating svg {
            width: 2rem;
            height: 2rem;
            overflow: visible;
            fill: transparent;
            stroke: #666;
            stroke-linejoin: bevel;
            stroke-dash: 0.2s;
        }

        .rating label:hover svg {
            stroke: #ffc73a;
        }

        .rating input:checked ~ label svg {
            transition: 0s;
            fill: #ffc73a;
            stroke: #ffc73a;
            stroke-opacity: 0;
            stroke-dasharray: 0;
            stroke-linejoin: miter;
            stroke-width: 8px;
        }

        form {
            max-width: 600px;
            margin: 20px auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 8px;
        }

        textarea {
            width: 100%;
            height: 100px;
            margin-bottom: 10px;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
            resize: vertical;
        }

        button {
            background-color: #007A7A;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        button:hover {
            background-color: #45a049;
        }
    </style>
</head>

<body>
    <%
       
       if(x!=null)
       {
       
      %>
    <h2>Give us your review</h2>
    <form action="sendFeedBack" method="post">
        <div class="rating">
            <input type="radio" id="star-5" name="rating" value="5" checked >
            <label for="star-5">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                </svg>
            </label>
            <input type="radio" id="star-4" name="rating" value="4">
            <label for="star-4">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                </svg>
            </label>
            <input type="radio" id="star-3" name="rating" value="3">
            <label for="star-3">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                </svg>
            </label>
            <input type="radio" id="star-2" name="rating" value="2">
            <label for="star-2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                </svg>
            </label>
            <input type="radio" id="star-1" name="rating" value="1">
            <label for="star-1">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z"></path>
                </svg>
            </label>
        </div>
        <%  String ms =(String) request.getAttribute("ms");%>
         <% if(ms!=null) {%>
         <p style="color: red"><%=ms%></p>
         <%ms=null;%>
         <%}%>
        <input  type="hidden" name="name" value="<%=x.getUsername()%>"> 
        <input  type="hidden" name="idacc" value="<%=x.getId()%>">
        <textarea name="review" placeholder="Enter your review"></textarea>
        <br>
        <button type="submit">Send Review</button>
    </form>
<%}else{%>


<h2 corlor="green">Please <a href="login.jsp">Login</a> To Use Many Other Interesting Features</h2>
<%}%>
</body>
</html>

