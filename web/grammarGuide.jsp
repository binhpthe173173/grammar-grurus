<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="models.*"%>
<%@page import="dal.*"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Example Page</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #F5F5F5;
            }
            .header {
                background-color: #00BFA6;
                padding: 20px;
                text-align: center;
            }
            .header h1 {
                margin: 0;
                font-size: 24px;
            }
            .search-container {
                margin: 20px 0;
                text-align: center;
            }
            .search-container input[type="text"] {
                width: 60%;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }
            .search-container button {
                padding: 10px 15px;
                border: none;
                border-radius: 4px;
                background-color: #333;
                color: white;
                cursor: pointer;
            }
            .content {
                display: flex;
                justify-content: center;
                margin: 20px;
                color: #00BFA6
            }
            .column {
                flex: 1;
                padding: 0 20px;
            }
            .column h1 {
                font-size: 40px;
                border-bottom: 2px solid #333;
                padding-bottom: 5px;
            }
            .column ul {
                list-style: none;
                padding: 0;
            }
            .column li {
                margin: 10px 0;
            }
            a {
                text-decoration: none;
                color: black;
            }

            a:hover {
                color: #5191B6; /* Màu bạn muốn khi hover */
                text-decoration: underline;
            }
            header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 10px 20px;
    background-color: #fff;
    border-bottom: 1px solid #ddd;
}

header .logo {
    font-size: 24px;
    font-weight: bold;
    color: #000;
    text-decoration: none;
}

header a {
    margin: 0 15px;
    color: #000;
    text-decoration: none;
    transition: color 0.3s;
}

header a:hover {
    color: #007A7A;
}
.custom-breadcrumb {
            background-color: #e9ecef;
            padding: 10px 15px;
            border-radius: 5px;
            margin: 20px 0;
        }
        
        .custom-breadcrumb .breadcrumb {
            background-color: transparent;
            padding: 0;
            margin: 0;
            list-style: none;
            display: flex;
        }
        
        .custom-breadcrumb .breadcrumb-item + .breadcrumb-item::before {
            content: ">";
            color: #6c757d;
            padding: 0 5px;
        }
        
        .custom-breadcrumb .breadcrumb-item a {
            color: #007A7A;
            text-decoration: none;
        }
        
        .custom-breadcrumb .breadcrumb-item a:hover {
            color: #005a5a;
            text-decoration: underline;
        }
        
        .custom-breadcrumb .breadcrumb-item.active {
            color: #6c757d;
        }

        </style>
    </head>
    <body>
        <%
       Account x = (Account) session.getAttribute("us");
       if(x!=null&& x.getRole_id()==0)
       {
        %>
        <nav aria-label="breadcrumb" class="custom-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<%= request.getContextPath() %>/Admin/homePage">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Manager Blogs</li>
        </ol>
    </nav>
        <%}%>
        
       <%if(x==null|| x.getRole_id()!=0){%>
         <header>
        <div>
            <a href="#" class="logo">Grammar Gurus</a>
        </div>
        <div>
            
           <%        if(x!=null){%>
            <a href="checkFunction.jsp">Home</a>
            <%}%>
          
          <%        if(x==null){%>
           <a href="userBlog.jsp">Blog</a>
            <a href="listFAQ.jsp">FAQ</a> 
           
             <a href="grammarGuideList">Grammar Guide</a>
             <a href="login.jsp">Use App</a>
            <a href="register.jsp">Register</a>
            <%}%>
        </div>
        </div>
    </header>
        <%}%>
        
        
        
        <div class="header">
            <h1>Check for answers to the most common grammar and writing style questions, or try our grammar checker software.</h1>
        </div>

   
        
        
        
        
        
        
        <form action="searchGG" method="post"> <div class="search-container">
            <input type="text" placeholder="What are you looking for?"name="keyword">
            <input type="submit"  value="Search" >
            </div>  </form>
       
       <%  if(x!=null&& x.getRole_id()==0)
       {
        %>
        <div class="add-faq-container">
            <a href="insertGG.jsp" class="add-faq-button"><h2>Add Grammar Guide</h2></a>
        </div><%}if(x==null|| x.getRole_id()!=0) {%>
        <%}%>
        
        
        <div class="content">
            <div class="column">
                <h1>Grammar</h1>
                <ul>
                    <c:forEach var="item" items="${grammarList}">
                        <li><a href="detailGG?idContent=${item.idContent}">●${item.question}</a></li>
                        </c:forEach>
                </ul>
            </div>
            <div class="column">
                <h1>Punctuation</h1>
                <ul>
                    <c:forEach var="item" items="${punctuationList}">
                        <li><a href="detailGG?idContent=${item.idContent}">●${item.question}</a></li>
                        </c:forEach>
                </ul>
            </div>
            <div class="column">
                <h1>Mechanics</h1>
                <ul>
                    <c:forEach var="item" items="${mechanicsList}">
                        <li><a href="detailGG?idContent=${item.idContent}">●${item.question}</a></li>
                        </c:forEach>
                </ul>
            </div>
        </div>
    </body>
</html>
