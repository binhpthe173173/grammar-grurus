<%@ page import="java.util.List" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="models.*" %>
<%@ page import="dal.*" %>

<%  Account x2 = (Account) session.getAttribute("us");
       if(x2!=null&&x2.getRole_id()==0){
       
       
    BlogDAO dao = new BlogDAO();
    List<Blog> blogs = dao.getAllBlogsByState("choduyet");
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Blog Approval</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }
        table, th, td {
            border: 1px solid #007A7A;
        }
        th, td {
            padding: 10px;
            text-align: left;
        }
        th {
            background-color: #007A7A;
            color: white;
        }
        tr:nth-child(even) {
            background-color: #E6F0FB;
        }
        .approve-btn {
            background-color: #007A7A;
            color: white;
            padding: 5px 10px;
            text-decoration: none;
            border-radius: 5px;
            cursor: pointer;
        }
        .approve-btn:hover {
            background-color: #005a5a;
        }
    </style>
</head>
<body>
    <h1>Blog Approval</h1>
    <table>
        <tr>
            <th>ID Account</th>
            <th>Title</th>
            <th>Type</th>
            <th>Time Up</th>
            <th>ID Blog</th>
            <th>Content of Blog</th>
            <th>Action</th>
        </tr>
        <% for (Blog blog : blogs) { %>
        <tr>
            <td><%= blog.getIdacount() %></td>
            <td><%= blog.getTitle() %></td>
            <td><%= blog.getType() %></td>
            <td><%= blog.getTime_up() %></td>
            <td><%= blog.getIdBlog() %></td>
            <td><span class="read-more" onclick="window.location.href='fullContentblog.jsp?id=<%= blog.getIdBlog() %>'">Read </span></td>  
            <td>
                <form action="approveBlog" method="post">
                    <input type="hidden" name="idBlog" value="<%= blog.getIdBlog() %>">
                    <button type="submit" class="approve-btn">Approve</button>
                </form>
                     <form action="deleteBlog" method="post">
                    <input type="hidden" name="idBlog" value="<%= blog.getIdBlog() %>">
                    <button type="submit" class="approve-btn">Delete</button>
                </form>
            </td>
        </tr>
        <% } %>
    </table>
</body>
</html>
<%}%>