<%@ page import="models.*" %>
<%@ page import="dal.*" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<% 
    String idBlog = request.getParameter("id");
    BlogDAO dao = new BlogDAO();
    Blog blog = dao.getBlogById(idBlog);
%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Confirm Delete</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 20px;
            text-align: center;
        }
        .confirmation-box {
            background-color: #ffffff;
            padding: 20px;
            border: 1px solid #007A7A;
            border-radius: 10px;
            display: inline-block;
            margin-top: 50px;
        }
        .confirmation-box h2 {
            color: #007A7A;
        }
        .confirmation-box p {
            color: #666;
        }
        .confirmation-box .btn {
            display: inline-block;
            padding: 10px 20px;
            margin: 10px;
            text-decoration: none;
            color: white;
            border-radius: 5px;
            cursor: pointer;
            transition: background-color 0.3s;
        }
        .btn-delete {
            background-color: #ff4c4c;
        }
        .btn-delete:hover {
            background-color: #cc0000;
        }
        .btn-cancel {
            background-color: #007A7A;
        }
        .btn-cancel:hover {
            background-color: #005a5a;
        }
    </style>
</head>
<body>
    <div class="confirmation-box">
        <h2>Confirm Delete</h2>
           
        <p>Are you sure you want to delete the following blog?</p>
        <p><strong>Id Blog:</strong> <%= blog.getIdBlog() %></p>
        <p><strong>Title:</strong> <%= blog.getTitle() %></p>
        <p><strong>Type:</strong> <%= blog.getType() %></p>
        <p><strong>Time Up:</strong> <%= blog.getTime_up() %></p>
        
        <form action="deleteBlog" method="post" style="display: inline;">
            <input type="hidden" name="idBlog" value="<%= blog.getIdBlog() %>">
            <button type="submit" class="btn btn-delete">Delete</button>
        </form>
        <a href="javascript:history.back()" class="btn btn-cancel">Cancel</a>
    </div>
</body>
</html>
