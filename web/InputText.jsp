<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="menujsp.jsp" />
        <% 
    List<String> errorGrammar = (List<String>) request.getAttribute("error");
    List<String> suggestGrammar=(List<String>) request.getAttribute("suggestion");
    String texted = (String) request.getAttribute("text");
%>
        <form action="testBox" method="post">
             <input type="text" name="text1" 
       <%if(texted!=null &&!texted.equals("")){%>
             value="<%=texted%>"
       <%}else{%>
             value=""
       <%}%>/>
            <br/>
            <input type="submit" value="Save"/>
        </form>
            <table style="border: 1px solid black; border-collapse: collapse;">
                <% if (errorGrammar != null && suggestGrammar != null) { %>
                    <% for (int i = 0; i < errorGrammar.size(); i++) { if(suggestGrammar.get(i).equalsIgnoreCase(".")){
                    suggestGrammar.set(i, "space");
                        }%>
                        <tr style="border: 1px solid black; border-collapse: collapse;">
                            <td style="border: 1px solid black; border-collapse: collapse;"><p><%= errorGrammar.get(i) %></p></td>
                            <td style="border: 1px solid black; border-collapse: collapse;"><p><%= suggestGrammar.get(i) %></p></td>
                        </tr>
                    <% } %>
                <% } else if (errorGrammar != null) { %>
                    <% for (int i = 0; i < errorGrammar.size(); i++) { %>
                        <tr style="border: 1px solid black; border-collapse: collapse;">
                            <td><p><%= errorGrammar.get(i) %></p></td>
                            <td><p></p></td>
                        </tr>
                    <% } %>
                <% } else if (suggestGrammar != null) { %>
                    <% for (int i = 0; i < suggestGrammar.size(); i++) { %>
                        <tr>
                            <td><p></p></td>
                            <td><p><%= suggestGrammar.get(i) %></p></td>
                        </tr>
                    <% } %>
                <% } %>
            </table>
            <% 
    List<String> errorSpelling = (List<String>) request.getAttribute("error");
    List<String> suggestSpelling=(List<String>) request.getAttribute("suggestion");
    String score = (String) request.getAttribute("score");
%>
        <form action="testBox" method="get">
             <input type="text" name="text1" 
       <%if(texted!=null &&!texted.equals("")){%>
             value="<%=texted%>"
       <%}else{%>
             value=""
       <%}%>/>
            <br/>
            <input type="submit" value="Save"/>
        </form>
            <%if(score!=null){
            %><p><%= score
                %></p><%}%>
    </body>
</html>