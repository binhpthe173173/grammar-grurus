<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add Funds to Wallet</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                padding: 20px;
            }
            form {
                max-width: 500px;
            }
            form div {
                margin-bottom: 15px;
            }
            form label {
                display: block;
                margin-bottom: 5px;
            }
            form input[type="number"] {
                width: 100%;
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }
            form button {
                padding: 10px 15px;
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            form button:hover {
                background-color: #0056b3;
            }
            button {
                padding: 8px 12px;
                cursor: pointer;
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 4px;
                margin-top: 10px;
                width: 100px;
                margin-right: 10px;
                margin-bottom: 10px;
            }
            button:hover {
                background-color: #0056b3;
            }
        </style>
    </head>
    <body>
        <h1>Add Funds to ${wallet.account_id}</h1>
        <form action="" method="post">
            <div>
                <input type="number" id="accountId" name="accountId" value="${wallet.account_id}" hidden>
            </div>
            <div>
                <label for="amount">Amount (VND):</label>
                <input type="number" id="amount" name="amount" required>
            </div>
            <div>
                <button type="submit">Add Funds</button>
                <button type="button" onclick="window.location.href='manageWallet'">Cancel</button>
            </div>
        </form>
    </body>
</html>
