<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="models.*"%>
<%@ page import="dal.*"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Insert Blog</title>
    
    
    
    
    <style>
       
       
       
       
        body .insert {
            font-family: Arial, sans-serif;
            background-color: #f4f4f9;
            margin: 0;
            padding: 0;
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        h2 {
            color: #333;
            margin-top: 20px;
        }
        form {
            margin: 20px;
            padding: 20px;
            border: 1px solid #007A7A;
            border-radius: 5px;
            background-color: #fff;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            width: 100%;
            max-width: 800px;
        }
        label {
            display: block;
            margin-top: 10px;
            color: #007A7A;
        }
        input[type="text"], textarea {
            width: calc(100% - 22px);
            padding: 10px;
            margin-top: 5px;
            border: 1px solid #ccc;
            border-radius: 5px;
            box-sizing: border-box;
        }
        input[type="submit"] {
            margin-top: 20px;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            background-color: #007A7A;
            color: #fff;
            cursor: pointer;
        }
        textarea {
            height: 600px; /* Chiều cao của hộp nhập nội dung */
        }
    </style>
</head>
<body><% Account x2 = (Account) session.getAttribute("us");
       if(x2.getRole_id()==0){
       
       %>
    <div class="insert">
        <h2>Write Your Blog And Share Your Knowledge With Our Community </h2>
        <form class="high-weight" action="checkBlog" method="post">
            <input type="hidden" name="idacount" value="<%=x2.getId()%>"> <br>
            <label>Title:</label> 
            <input type="text" name="title" required><br>
            <label>Type:</label> 
             <select name="type">

                <option value="Grammar">Grammar</option>
                <option value="Business Writing">Business Writing</option>
                <option value="Creative Writing">Creative Writing</option>
                <option value="Student Writing">Student Writing</option>
                <option value="Inspiration">Inspiration</option>
                 <option value="GrammarGurus Help">GrammarGurus Help</option>
                                  <option value="Other">Other</option>


                <!-- Add more options as needed -->
            </select><br>
            <label>Content:</label><br>
            <textarea name="content" rows="10" cols="50"></textarea><br>
            
            <input type="hidden" name="state" value="choduyet"><br>
            <input type="submit" class="sub" value="Post Blog">
        </form>
    </div><%}%>
</body>
</html>

