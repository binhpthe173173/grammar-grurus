<%-- 
    Document   : sideGuest
    Created on : Jul 12, 2024, 6:27:08 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" type="text/css" href="Css/style.css?v=1.0">
    </head>
    <body>
        <aside class="sidebar">
            <div class="logo" style="font-size: 20px;"><a href="#">GrammarGurus</a></div>
            <ul>
                <li><a href="login.jsp"><button class="new-page">Use App</button></a></li>
                <li><button class="page-item"><a href="userBlog.jsp">FAQ</a></button></li>
                <li><a href="includeBlog.jsp"><button class="new-page">Blogs</button></a></li>
                <li><a href="review.jsp"><button class="new-page">Review</button></a></li>
                <li><a href="login.jsp"><button class="new-page">Login</button></a></li>
            </ul>
        </aside>
    </body>
</html>
