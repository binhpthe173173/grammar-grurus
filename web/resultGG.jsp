<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="models.*"%>
<%@page import="dal.*"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Example Page</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #F5F5F5;
            }
            .header {
                background-color: #00BFA6;
                padding: 20px;
                text-align: center;
            }
            .header h1 {
                margin: 0;
                font-size: 24px;
            }
            .search-container {
                margin: 20px 0;
                text-align: center;
            }
            .search-container input[type="text"] {
                width: 60%;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }
            .search-container button {
                padding: 10px 15px;
                border: none;
                border-radius: 4px;
                background-color: #333;
                color: white;
                cursor: pointer;
            }
            .content {
                display: flex;
                justify-content: center;
                margin: 20px;
                color: #00BFA6
            }
            .column {
                flex: 1;
                padding: 0 20px;
            }
            .column h1 {
                font-size: 40px;
                border-bottom: 2px solid #333;
                padding-bottom: 5px;
            }
            .column ul {
                list-style: none;
                padding: 0;
            }
            .column li {
                margin: 10px 0;
            }
            a {
                text-decoration: none;
                color: black;
            }

            a:hover {
                color: #5191B6; /* Màu bạn muốn khi hover */
                text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <div class="header">
            <h1>Check for answers to the most common grammar and writing style questions, or try our grammar checker software.</h1>
        </div>

 <%
       Account x = (Account) session.getAttribute("us");
       if(x!=null&& x.getRole_id()==0)
       {
        %>
        <div class="add-faq-container">
            <a href="insertGG.jsp" class="add-faq-button"><h2>Add Grammar Guide</h2></a>
        </div><%}%>
        <form action="searchGG" method="post"> <div class="search-container">
            <input type="text" placeholder="What are you looking for?"name="keyword">
            <input type="submit"  value="Search" >
            </div>  </form>
       
        <div class="content">
            <div class="column">
                <h1>Common questions for editors about "${keyword}"</h1>
                <ul>
                    <c:forEach var="item" items="${results}">
                        <li><a href="detailGG?idContent=${item.idContent}">●${item.question}</a></li>
                        </c:forEach>
                </ul>
            </div>