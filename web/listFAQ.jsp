<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="models.*"%>
<%@page import="dal.*"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>List of Topictypes</title>
    <style>
        body {
            margin: 0;
            padding: 0;
        }
        .container {
            display: flex;
            flex-wrap: wrap;
            gap: 10px;
            justify-content: space-between;
            padding: 0 200px;
            margin-bottom: 40px;
        }
        .box h2 {
            color: #007a7a;
            text-align: center;
        }
        .box {
            border: 1px solid #007a7a;
            padding: 10px;
            width: calc(30% - 20px);
            box-sizing: border-box;
            transition: transform 0.2s ease-in-out;
            cursor: pointer;
            border-radius: 8px;
            text-decoration: none;
            color: inherit;
            margin-bottom: 20px;
        }
        .box:hover {
            transform: scale(1.03);
            box-shadow: 0 0 10px rgba(0, 122, 122, 0.3);
        }
        .box strong, .box p {
            color: black;
            text-align: center;
        }
        h1 {
            color: #007A7A;
            text-align: center;
        }
        .contact-container {
            margin: 20px 0;
            display: flex;
            justify-content: center;
            align-items: center;
            gap: 10px;
        }
        .contact-question {
            color: #808080; /* Màu nhạt hơn */
            font-weight: normal;
        }
        .contact-link {
            color: #007a7a; /* Màu xanh đậm hơn */
            font-weight: bold;
            text-decoration: none;
        }
        .contact-link:hover {
            text-decoration: underline;
        }
          .add-faq-container {
            display: flex;
            justify-content: center;
            margin-bottom: 20px;
            color: #007a7a;
        }
        header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 10px 20px;
    background-color: #fff;
    border-bottom: 1px solid #ddd;
    margin-bottom: 30px;
}

header .logo {
    font-size: 24px;
    font-weight: bold;
    color: #000;
    text-decoration: none;
}

header a {
    margin: 0 15px;
    color: #000;
    text-decoration: none;
    transition: color 0.3s;
}

header a:hover {
    color: #007A7A;
}

.main-content {
    text-align: center;
    padding: 50px 20px;
    background-color: #E6F0FB;
}

.main-content h1 {
    font-size: 36px;
    margin-bottom: 20px;
    color: #333;
}

.main-content p {
    font-size: 18px;
    margin-bottom: 30px;
    color: #666;
}

.main-content .cta-button {
    padding: 15px 30px;
    background-color: #007A7A;
    color: white;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    font-size: 16px;
    text-decoration: none;
    transition: background-color 0.3s;
}

.main-content .cta-button:hover {
    background-color: #005a5a;
}

.contact-section {
    text-align: center;
    padding: 50px 20px;
    background-color: #fff;
}

.contact-section h2 {
    font-size: 28px;
    margin-bottom: 20px;
    color: #333;
}

.contact-section p {
    font-size: 16px;
    color: #666;
}

.contact-section .contact-links a {
    margin: 0 10px;
    color: #007A7A;
    text-decoration: none;
    font-size: 16px;
    transition: color 0.3s;
}

.contact-section .contact-links a:hover {
    color: #005a5a;
}
    </style>
</head>
<body>
    <%
       Account x = (Account) session.getAttribute("us");
     
      %>
    <header>
        <div>
            <a href="#" class="logo">Grammar Gurus</a>
        </div>
        <div>
            
           <%        if(x!=null){%>
            <a href="checkFunction.jsp">Home</a>
            <%}%>
          
          <%        if(x==null){%>
          <a href="userBlog.jsp">Blog</a>
            <a href="listFAQ.jsp">FAQ</a> 
           
             <a href="grammarGuideList">Grammar Guide</a>
             <a href="login.jsp">Use App</a>
            <a href="register.jsp">Register</a>
            <%}%>
    </header>
    
       <%if(x==null)
       {
      %>
      <div class="main-content">
        <h1>Knowledge is power, use it wisely.</h1>
        <p>Grammar Gurus helps you craft, polish, and elevate your writing.</p>
        
    </div>
    
    <%}%>
    <div class="grammar-image">
                <img src="https://assets.prowritingaid.com/f/145420/1096x800/1bb672344d/hero_teachers_featurespage.png/m/684x500/" alt="Grammar Checking">
            </div>
    
    
    <h1>Grammar Gurus Frequently Asked Questions</h1>
    <br><br>
    
    <div class="contact-container">
        <h2 class="contact-question">Can’t find an answer for your question?</h2>
        <h2><a href="contactus.jsp" class="contact-link">Contact Us ></a></h2>
    </div>
    
    
    
    <%
        TopictypeDAO dao = new TopictypeDAO();
        QandaDAO qadao = new QandaDAO();

       
        List<Topictype> topictypeList = dao.getTopicTypesByType("f");
    %>
    <div class="container">
        <% for (Topictype topictype : topictypeList) { %>
        <a href="Topicf?idtopic=<%= topictype.getIdtopic() %>" class="box">
            <h2><%= topictype.getTopic() %></h2>
            <p><%= qadao.countQandatopicsByTopicId(topictype.getIdtopic()) %> <strong>Articles</strong></p>
        </a>
        <% } %>
    </div>
    <%@ include file="footer.jsp" %>

</body>
</html>
