<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Add Account</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <style>

            .sidebar {
                height: 100vh;
                position: fixed;
                padding: 24px;
                background-color: #9AECDB;
            }
            .sidebar-brand {
                text-align: center;
                margin-bottom: 20px;
            }
            .sidebar-brand h2 {
                margin: 0;
                font-size: 28px;
                color: #9AECDB;
            }
            .main {
                margin-left: 200px;
                padding: 24px;
            }
        </style>
        
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar" style="padding-left: 24px;padding-right: 24px;">
                    <%@ include file="sidebar.jsp" %>
                </nav>

                <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4 bg-light main">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<%= request.getContextPath() %>/Admin/homePage">Home</a></li>
                            <li class="breadcrumb-item"><a href="<%= request.getContextPath() %>/Admin/listUser">List Account</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Add Account</li>
                        </ol>
                    </nav>
                            
                    <div class="container " style="margin-left: 0px; margin-right: 0px;padding:0px;">
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <h1 class="h2">Add Account</h1>
                        </div>
                        <!-- Error Message Display -->
                        <c:if test="${not empty err}">
                            <div class="alert alert-danger" role="alert">
                                ${err}
                            </div>
                        </c:if>

                        <form action="addAccount" method="post">
                            <div class="form-group">
                                <label for="username">Username:</label>
                                <input type="text" class="form-control" id="username" name="username" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Password:</label>
                                <input type="password" class="form-control" id="password" name="password" required>
                            </div>
                            <div class="form-group">
                                <label for="confirm_password">Confirm Password:</label>
                                <input type="password" class="form-control" id="confirm_password" name="confirm_password" required>
                            </div>
                            <div class="form-group">
                                <label for="full_name">Full Name:</label>
                                <input type="text" class="form-control" id="full_name" name="full_name" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" id="email" name="email" required>
                            </div>
                            <div class="form-group">
                                <label for="image">Image:</label>
                                <div>
                                    <label>
                                        <input type="radio" name="image" value="\Image\user.jpg" required>
                                        <img src="..\Image\user.jpg" alt="User Image 1" width="100">
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="image" value="\Image\userman.png" required>
                                        <img src="..\Image\userman.png" alt="User Image 2" width="100">
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="image" value="\Image\Usergirl.jpg" required>
                                        <img src="..\Image\Usergirl.jpg" alt="User Image 3" width="100">
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="active_status">Active Status:</label>
                                <select class="form-control" id="active_status" name="active_status">
                                    <option value="true">Active</option>
                                    <option value="false">Inactive</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="role_id">Role ID:</label>
                                <input type="number" class="form-control" id="role_id" name="role_id" required min="0" max="1">
                            </div>
                            <button type="submit" class="btn btn-primary">Add Account</button>
                        </form>
                    </div>
                </main>
            </div>
    </body>
</html>
