<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="models.Subscription" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Manage Subscription</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>
            body {
                font-family: Arial, sans-serif;
                line-height: 1.6;
                padding: 0px;
                display: flex;
            }
            h1, h2 {
                color: #333;
            }
            table {
                width: 100%;
                border-collapse: collapse;
                margin-top: 20px;
            }
            table, th, td {
                border: 1px solid #ccc;
            }
            th, td {
                padding: 10px;
                text-align: left;
            }
            button {
                padding: 8px 12px;
                cursor: pointer;
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 4px;
                margin-top: 10px;
            }
            button:hover {
                background-color: #0056b3;
            }
            form {
                max-width: 600px;
                margin-top: 20px;
            }
            form div {
                margin-bottom: 10px;
            }
            form label {
                display: block;
                margin-bottom: 5px;
                color: #333;
            }
            form input[type="text"],
            form input[type="number"],
            form textarea,
            form select {
                width: 100%;
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 4px;
                font-size: 16px;
            }
            td button {
                width: 70px;
                margin-right: 5px;
                margin-bottom: 5px;
            }
            a {
                text-decoration: none;
                color: #fff;
            }
            .sidebar {
                height: 100vh;
                position: fixed;
                padding-top:24px;
               
                background-color: #9AECDB;
            }
            .sidebar-brand {
                text-align: center;
                margin-bottom: 20px;
            }
            .sidebar-brand h2 {
                margin: 0;
                font-size: 28px;
                color: #9AECDB;
            }
            .main {
                margin-left: 200px;
                padding: 24px;
            }

            
            .breadcrumb a {
                text-decoration: none;
                color: #007bff;
            }
            

            /* Bootstrap 4 Table Styling */
            .table {
                margin-top: 20px;
            }
            .table th, .table td {
                border: 1px solid #dee2e6;
                padding: 8px;
                vertical-align: middle;
            }
            .table th {
                background-color: #007bff;
                color: #fff;
            }
            .table-striped tbody tr:nth-of-type(odd) {
                background-color: rgba(0, 123, 255, 0.1);
            }
        </style>

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar" style="padding-left: 24px;padding-right: 24px;">
                    <%@ include file="sidebar.jsp" %>
                </nav>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 bg-light main">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<%= request.getContextPath() %>/Admin/homePage">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Subscription</li>
                        </ol>
                    </nav>
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">      
                        <h1>Manage Subscription</h1></div>
                    <button onclick="window.location.href = 'addSubscription'" class="btn btn-primary mb-3">Add Subscription</button>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Duration</th>
                                    <th>Status</th>
                                    <th>Description</th>
                                    <th>Created on</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="subscr" items="${dao.subscriptions}">
                                    <tr>
                                        <td>${subscr.id}</td>
                                        <td>${subscr.name}</td>
                                        <td>${subscr.price}</td>
                                        <td>${subscr.duration} ${subscr.duration == 1 ? 'day' : 'days'}</td>
                                        <td>${subscr.status ? 'Active' : 'Inactive'}</td>
                                        <td>${subscr.description}</td>
                                        <td>${subscr.time_created}</td>
                                        <td>
                                            <button class="btn btn-info btn-sm"><a href="editSubscription?id=${subscr.id}" style="color: #fff">Edit</a></button>
                                            <button class="btn btn-danger btn-sm"><a href="deleteSubscription?id=${subscr.id}" style="color: #fff" onclick="return confirm('Are you sure you want to delete this Subscription?')">Delete</a></button>
                                        </td>
                                    </tr>
                                </c:forEach>
                                <c:if test="${empty dao.subscriptions}">
                                    <tr>
                                        <td colspan="8">No Subscription existed.</td>
                                    </tr>
                                </c:if>
                            </tbody>
                        </table>
                    </div>
                </main>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </body>
</html>
